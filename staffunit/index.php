<!doctype html>
<html><head>
<meta charset="UTF-8">
<meta content="IE=edge" http-equiv=X-UA-Compatible> <meta content="width=device-width,initial-scale=1" name=viewport>
<title>AcceleDent Staff Unit Promotion Portal</title>
<!-- CSS -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" id="font-awesome-css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css" media="all" />
<link rel="stylesheet" href="assets/css/unit.css" type="text/css">
<link rel="stylesheet" href="assets/css/animate.css" type="text/css">
<link rel="shortcut icon" type="image/x-icon" href="http://acceledent.com/wp-content/themes/acceledent/img/favicon.ico">

<!-- JS -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body class="su_login">


 <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <img src="assets/img/logo_oati.png" class="logo">
                            <h1><strong>AcceleDent</strong> Staff Member Promotion</h1>
                            <div class="description">
                            	<p>
	                            	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eu erat dictum velit volutpat consectetur. Sed in volutpat libero. Nulla malesuada tincidunt nunc id lobortis. 
                            	</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box animated bounceIn">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Log In</h3>
                            		<p>Please enter your username and password.</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-lock"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" name="login" method="post" action="validate.php" class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="username">Username</label>
			                        	<input type="text" name="username" placeholder="Username..." class="form-username form-control" id="username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="password">Password</label>
			                        	<input type="password" name="password" placeholder="Password..." class="password form-control" id="password">
			                        </div>
			                        <input class="btn btn-lg btn-ortho btn-block" name="submit" value="LOGIN" type="submit" >

			                    </form>
		                    </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>

</body>
</html>