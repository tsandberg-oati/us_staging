<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta content="IE=edge" http-equiv=X-UA-Compatible> <meta content="width=device-width,initial-scale=1" name=viewport>
<title>AcceleDent Staff Unit Program</title>

<!-- CSS -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" id="font-awesome-css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css" media="all" />
<link rel="stylesheet" href="assets/css/unit.css" type="text/css">
<link rel="shortcut icon" type="image/x-icon" href="http://acceledent.com/wp-content/themes/acceledent/img/favicon.ico">

<!-- JS -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

<!-- DatePicker -->
<script>
  $( function() {
    $( "#datepicker" ).datepicker({ minDate: "+10M", maxDate: "+14M" });
  } );
  </script>

</head>
<body class="su_page">

<?php
include $_SERVER['DOCUMENT_ROOT']."/oati_includes/db.php";
$connect = mysql_connect($db_host, $db_user, $db_pw);
mysql_select_db('ortho_wp',$connect);
	
$linkid = mysql_real_escape_string(isset($_POST['linkid']) ? $_POST['linkid'] : null);
$action = mysql_real_escape_string(isset($_POST['action']) ? $_POST['action'] : null);
$nameack = mysql_real_escape_string(isset($_POST['nameack']) ? $_POST['nameack'] : null);
$doctor = mysql_real_escape_string(isset($_POST['doctor']) ? $_POST['doctor'] : null);
$practice = mysql_real_escape_string(isset($_POST['practice']) ? $_POST['practice'] : null);
$enddate =  mysql_real_escape_string(isset($_POST['enddate']) ? $_POST['enddate'] : date("m/d/Y",time()));

if($action == "process") {
	list($month,$day,$year) = explode("/",$enddate);
	$enddate = $year . "-" . $month . "-" . $day;
	$sql = "UPDARE staffunit_registrations SET nameack = '" . $nameack . "',doctor = '" . $doctor . "',practice = '" . $practice . "',enddate = '" . $enddate . "',regcompleted = NOW() WHERE linkid = '" . $linkid . "'";
	$result = mysql_query($sql);
	if(!$result) {
		?>

        <div class="container thankyou">
        <div class="row">
        <div class="col-md-12">
        <div class="well">
        	<h2>You have successfully registered. Thank you!</h2>
        </div>
        </div>
        </div>
        </div>

    <?php
	} else {
		$action = "";
	}
}

if(!$linkid && $action == "") {
	$sql= "SELECT fname,lname,email FROM staffunit_registrations WHERE linkid = '" . $linkid . "'";
	$result = mysql_query($sql);
	while($row = mysql_fetch_array($result)) {
			$fname = $row['fname'];
			$lname = $row['lname'];
			$email = $row['email'];
	}
?>

<div class="container agreement">
<div class="row">
<div class="col-md-12">
	<img src="assets/img/logo_oati.png" class="half center-block">
	<h2 class="text-center">AcceleDent Staff Member Program Entry Form and Authorization and Release of Case Records for Orthodontic Staff</h2>
	<p>I <mark><span id="customer"><?php echo $fname . " " . $lname ?></span></mark>, a full-time staff member at an orthodontic practice hereby enroll in OrthoAccel Technologies’ Staff Member Program at a cost of $99.00. By enrolling in this program I confirm that I am a current orthodontic staff member presently in or about to start orthodontic treatment. I agree to use this device for my own personal benefit as the AcceleDent Aura is a single-user prescription device and this promotional offer is not transferrable. I further agree to document the full case records at every office visit throughout the treatment and share records with OrthoAccel for use in marketing materials upon treatment completion as well as taking a patient completion survey.</p>

	<p>I agree that if I fail to provide a patient completion survey and full case clinical records, I will pay the Price list for a single unit of $649 ($649-$99 = $550) or an incremental $550 to OrthoAccel Technologies.</p>

	<p>I authorize the release of my orthodontic records, electronic or other form, to OrthoAccel Technologies, Inc. ("OATI"), including x-rays and photographs taken of me (including my face, mouth, and/or teeth) used by my employer/orthodontist (“orthodontist”) for my treatment.</p>

	<p>I understand that such photographs shall become the property of OATI, and as long as OATI’s does NOT disclose my identity, I give my expressed consent for OATI to retain, use, release and distribute such x-rays or photographs for publication or republication in any print, visual, electronic (internet) or broadcast media for any purpose which OATI deems appropriate to inform the medical profession or the general public about orthodontic methods and/or to advertise OATI's orthodontic products. The media may include, but are not limited to, the following: medical journals and textbooks, pamphlets, newspapers, magazines, videotapes, websites, television or motion pictures. However, I understand that in some circumstances the photographs may portray features that shall make my identity recognizable.</p>

	<p>In consideration for the benefits I receive in participating in OATI’s staff  program, I release and discharge OATI and all parties acting under its license and authority from all rights – including rights of publicity and privacy under any state or country statute or case law – that I may have in the photographs and from any claim that I (or my heirs) may have relating to such use and publication.  I further agree that my orthodontic case record may be used by OATI to develop and publish a case report with my Orthodontist’s help. I understand a case report is a summary of my orthodontic treatment and results prepared by my Orthodontist and potentially published in journal articles, marketing pieces, slide presentations or on the OATI website.</p>

	<p>I understand that if the use of my records and other protected health information and data from the case report is used in orthodontic publications or presentations, I will not be personally identified. OATI will code my records and other health information to protect my anonymity.</p>

	<p>I understand that I will not be paid for my participation, but that my participation in OATI’s staff program is conditioned upon the signing of this authorization.</p>

	<p>I acknowledge that I have had the opportunity to discuss and ask questions concerning the use of my treatment information by OATI with my Orthodontist including, but not limited to x-rays, photographs, treatment data, and my information in case reports. I further understand and acknowledge that my treatment information may be shared anonymously by OATI in its marketing efforts through various media on an ongoing basis throughout and after my orthodontic treatment.</p>

</div> <!-- End col-md-12 -->
</div> <!-- End row -->
</div> <!-- End container -->

<div class="container">
<div class="row">
	<div class="panel panel-default">
	  <div class="panel-body">
	  <form class="form-horizontal" name="auth" method="post" action="complete_registration.php">
<div class="form-group">
	<div class="col-sm-4"><p class="authorize lead">By typing my name in this box,</p></div>
	<div class="col-sm-8"><input name="nameack" type="text" class="form-control"></p></div>
	<div class="col-sm-12"><p class="authorize lead">I hereby authorize the use of my orthodontic treatment records by OATI for the purposes outlined above.</p></div>
</div>
<div class="form-group">
	<label for="inputDate" class="col-sm-3 control-label">Date</label>
		<div class="col-sm-9">
			<p class="currentdate"><?php echo date("m/d/y",time()); ?></p>
		</div>
</div>
<div class="form-group">
	<label for="inputEndDate" class="col-sm-3 control-label">Projected Treatment End Date</label>
		<div class="col-sm-9">
			<input type="text" class="form-control" id="datepicker" name="enddate" readonly>
		</div>
</div>
<div class="form-group">
	<label for="inputEndDate" class="col-sm-3 control-label">Doctor Name</label>
		<div class="col-sm-9">
			<input type="text" class="form-control" id="doctor" name="doctor">
		</div>
</div>
<div class="form-group">
	<label for="inputEndDate" class="col-sm-3 control-label">Practice Name</label>
		<div class="col-sm-9">
			<input type="text" class="form-control" id="practice" name="practice">
		</div>
</div>

<div class="form-group">
	<div class="col-md-offset-3 col-md-9">
		<input name="action" type="hidden" value="process"><input name="linkid" type="hidden" value="<?php echo $linkid; ?>"><input name="submit" type="submit" value="SUBMIT" class="btn btn-lg btn-ortho btn-block" >
	</div>
</div>
</form>
</div>
</div>

</div> <!-- End row -->
</div> <!-- End container -->

</body>
</html>
<?php
} else {
	header("http://acceledent.com");
}
?>
