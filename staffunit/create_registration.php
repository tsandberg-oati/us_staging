<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta content="IE=edge" http-equiv=X-UA-Compatible> <meta content="width=device-width,initial-scale=1" name=viewport>
<title>AcceleDent Staff Unit Program</title>

<!-- CSS -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" id="font-awesome-css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css" media="all" />
<link rel="stylesheet" href="assets/css/unit.css" type="text/css">
<link rel="shortcut icon" type="image/x-icon" href="http://acceledent.com/wp-content/themes/acceledent/img/favicon.ico">

<!-- JS -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

</head>

<body class="su_page">
<?php
include $_SERVER['DOCUMENT_ROOT']."/oati_includes/db.php";
include $_SERVER['DOCUMENT_ROOT']."/oati_includes/phpfunctions.php";
$connect = mysql_connect($db_host, $db_user, $db_pw);
mysql_select_db('ortho_wp',$connect);

$fname = mysql_real_escape_string(isset($_POST['fname']) ? $_POST['fname'] : null);
$lname = mysql_real_escape_string(isset($_POST['lname']) ? $_POST['lname'] : null);
$email = mysql_real_escape_string(isset($_POST['email']) ? $_POST['email'] : null);

if($lname != "" && $email != "") {
	$linkid = create_guid();
	$sql = "INSERT INTO staffunit_registrations (linkid,fname,lname,email,submitted) VALUES ('" . $linkid . "','" . $fname . "','" . $lname . "',NOW())";
	$result = mysql_query($sql);
	if($result) {
		$from = "sales@orthoaccel.com";
		$subject = "Your AcceleDent Staff Unit Registration";
		$body = "<html><body><p>Thank you for your interest in AcceleDent! In order to process your order for the requested Staff Unit, we'll need some additional information.</p><p>Please <a href=\"http://acceledent.com/staffunit/complete_registration.php?linkid=" . $linkid . "\">click this link</a> to review the terms and conditions of the program and complete your registration.</p><p>If the link is disabled, copy and paste the following into your browser's address bar: http://acceledent.com/staffunit/complete_registration.php?linkid=" . $linkid . "</p><p>Thank you!<br>OrthoAccel Technologies Inc.</p>";
		$sendemail = send_email($email, $from,$subject, $body); 
		if($sendemail) {
			echo "Patient successfully registered";
		}
	} else {
		header("create_registration.php");
	}
}

?>
<div class="container registration">
	<div class="row">
		<div class="panel panel-default">
	  <div class="panel-body">
		<div class="col-md-12">
		<h2>Submit the name and e-mail address of the staff member that is to receive a unit.</h2>
		<form name="createreg" method="post" action="create_registration.php" class="form-horizontal">
		  <div class="form-group">
			<label for="inputFName" class="col-sm-2 control-label">First Name</label>
			<div class="col-sm-10">
			  <input type="text" class="form-control" id="fname" name="fname" >
			</div>
		  </div>
		  <div class="form-group">
			<label for="inputLName" class="col-sm-2 control-label">Last Name</label>
			<div class="col-sm-10">
			  <input type="text" class="form-control" id="lname" name="lname" >
			</div>
		  </div>
		  <div class="form-group">
			<label for="inputEmail" class="col-sm-2 control-label">Email</label>
			<div class="col-sm-10">
			  <input type="text" class="form-control" id="email" name="email" >
			</div>
		  </div>
			<div class="col-md-offset-2 col-md-10">
			  <input name="submit" type="submit" value="SUBMIT" class="btn btn-lg btn-ortho btn-block" >
			</div>
		  </div>
			</form>
			</div>
		</div>
		</div>
	</div>

</body>
</html>