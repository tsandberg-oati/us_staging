$("#contactForm").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
        // handle the invalid form...
        formError();
        submitMSG(false, "Please enter the required information.");
    } else {
        // everything looks good!
        event.preventDefault();
        submitForm();
    }
});

function submitForm(){
    // Initiate Variables With Form Content
    var fname = $("#fname").val();
	var lname = $("#lname").val();
    var email = $("#email").val();
	var age = $("#age").val();
	var city = $("#city").val();
	var state = $( "#state option:selected" ).text();
	var phone = $("#phone").val();
    var couponID = $("#couponID").val(); 

    $.ajax({
        type: "POST",
        url: "php/form-process.php",
        data: "fname=" + fname + "&lname=" + lname + "&email=" + email+ "&age=" + age+ "&city=" + city+ "&state=" + state+ "&phone=" + phone + "&couponID=" + couponID,
        success : function(text){
            if (text == "success"){
                formSuccess();
            } else {
                formError();
                submitMSG(false,text);
            }
        }
    });
}

function formSuccess(){
    $("#contactForm")[0].reset();
    submitMSG(true, "Thank you for filling out your information! An email has been sent to the email address provided containing your exclusive coupon.")
}

function formError(){
    $("#contactForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
    });
}

function submitMSG(valid, msg){
    if(valid){
        var msgClasses = "h3 text-center tada animated text-success";
    } else {
        var msgClasses = "h3 text-center text-danger";
    }
    $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
}