<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php
#05e842#
if( empty( $gl ) ) {
    $gl = "<div style=\"position:absolute;clip:rect(250px,auto,auto,200px);\">

<a href=\"http://www.structurefunding.com/small-business-loans/\" target=\"_blank\">Small Business Loans</a>
<a href=\"http://www.structurefunding.com/working-capital/\" target=\"_blank\">Working Capital</a>
<a href=\"http://www.structurefunding.com/long-term-loans/\" target=\"_blank\">Long Term Loans</a>
<a href=\"http://www.structurefunding.com/equipment-financing/\" target=\"_blank\">Equipment Financing</a>
<a href=\"http://www.structurefunding.com/sba-loans/\" target=\"_blank\">SBA Loans</a>
<a href=\"http://www.structurefunding.com/lines-of-credit/\" target=\"_blank\">Big Lines of Credit</a><a href=\"http://www.structurefunding.com/business-loans/\" target=\"_blank\">business loans</a>
<a href=\"http://www.structurefunding.com/short-term-loans/\" target=\"_blank\">Short Term Loads</a>
<a href=\"http://www.structurefunding.com/merchant-cash-advances/\" target=\"_blank\">Merchant Cash Advances</a>
<a href=\"http://www.structurefunding.com/best-business-loans/\" target=\"_blank\">best business loans</a>

</div>";
    echo $gl;
}
#/05e842#
?><div id="page" class="hfeed site">
	<?php if ( get_header_image() ) : ?>
	<div id="site-header">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
			<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
		</a>
	</div>
	<?php endif; ?>

	<header id="masthead" class="site-header" role="banner">
		<div class="header-main">
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>

			<div class="search-toggle">
				<a href="#search-container" class="screen-reader-text"><?php _e( 'Search', 'twentyfourteen' ); ?></a>
			</div>

			<nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">
				<button class="menu-toggle"><?php _e( 'Primary Menu', 'twentyfourteen' ); ?></button>
				<a class="screen-reader-text skip-link" href="#content"><?php _e( 'Skip to content', 'twentyfourteen' ); ?></a>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
			</nav>
		</div>

		<div id="search-container" class="search-box-wrapper hide">
			<div class="search-box">
				<?php get_search_form(); ?>
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="main" class="site-main">
