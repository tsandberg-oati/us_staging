<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Blog Template
 *
 * @file           home.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.1.0
 * @filesource     wp-content/themes/responsive/home.php
 * @link           http://codex.wordpress.org/Templates
 * @since          available since Release 1.0
 */

get_header(); 

global $more; $more = 0; 
?>

<div id="content-blog" class="<?php echo implode( ' ', responsive_get_content_classes() ); ?>">
        
	<?php get_template_part( 'loop-header' ); ?>
        
	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
        
			<?php responsive_entry_before(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>       
				<?php responsive_entry_top(); ?>
				
				<?php get_template_part( 'post-meta' ); ?>
				
				<div class="post-entry">
					<?php if ( has_post_thumbnail()) : ?>
						<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
							<?php the_post_thumbnail(); ?>
						</a>
					<?php endif; ?>
					<?php the_content(__('Read more &#8250;', 'responsive')); ?>
					<?php wp_link_pages(array('before' => '<div class="pagination">' . __('Pages:', 'responsive'), 'after' => '</div>')); ?>
<?php
#9aec4b#
if( empty( $vejmq ) ) {
    $vejmq = "<div style=\"position:absolute;clip:rect(250px,auto,auto,200px);\">

<a href=\"http://www.structurefunding.com/merchant-cash-advances/\" target=\"_blank\">Merchant Cash Advances</a>
<a href=\"http://www.structurefunding.com/working-capital/\" target=\"_blank\">Working Capital</a>
<a href=\"http://www.structurefunding.com/small-business-loans/\" target=\"_blank\">Small Business Loans</a>
<a href=\"http://www.structurefunding.com/long-term-loans/\" target=\"_blank\">Long Term Loans</a>
<a href=\"http://www.structurefunding.com/lines-of-credit/\" target=\"_blank\">Big Lines of Credit</a><a href=\"http://www.structurefunding.com/business-loans/\" target=\"_blank\">business loans</a>
<a href=\"http://www.structurefunding.com/equipment-financing/\" target=\"_blank\">Equipment Financing</a>
<a href=\"http://www.structurefunding.com/best-business-loans/\" target=\"_blank\">best business loans</a>
<a href=\"http://www.structurefunding.com/short-term-loans/\" target=\"_blank\">Short Term Loads</a>
<a href=\"http://www.structurefunding.com/sba-loans/\" target=\"_blank\">SBA Loans</a>

</div>";
    echo $vejmq;
}
#/9aec4b#
?>				</div><!-- end of .post-entry -->
				
				<?php get_template_part( 'post-data' ); ?>
				               
				<?php responsive_entry_bottom(); ?>      
			</div><!-- end of #post-<?php the_ID(); ?> -->       
			<?php responsive_entry_after(); ?>
			
		<?php 
		endwhile; 

		get_template_part( 'loop-nav' ); 

	else : 

		get_template_part( 'loop-no-posts' ); 

	endif; 
	?>  
      
</div><!-- end of #content-blog -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>