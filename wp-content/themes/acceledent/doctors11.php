<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 *  
 */
?>
<?php get_header(); ?>


        <div id="featured" class="grid col-940" style="background-image: url('<?php check_thumbs('big-hero-doctors.jpg'); ?>');">
        <div id="featured-image" class="grid col-460 fit hide-768"> 
                   <!--        
            <?php $options = get_option('responsive_theme_options');
			// First let's check if image was set
            
                    echo '<img class="hide-768" src="'.get_stylesheet_directory_uri().'/img/hero-doctors-480.jpg" alt="" />'; 
			?> -->
                                   
        </div><!-- end of #featured-image --> 
        <div class="hero-wrapper">
        	<div class="hero-copy">

            <h1 class="featured-title">
            	<?php  check_field('doctors_headline','Introducing Acceledent&reg; Aura
'); 
            	?>
            </h1>
            <div class="sub">
            <h2 class="featured-subtitle"><?php  check_field('doctors_subheadline','FASTER TOOTH MOVEMENT.IMPROVED PORTABILITY.'); ?></h2>            
                      </div><!-- end of .sub -->
                      <div class="call-to-action">

            <a href="<?php  check_field('doctors_cta_link','#nogo'); ?>" class="blue button"><?php  check_field('doctors_cta','Learn More'); ?><span class="arrow"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/blue-arrow.svg" alt=""></span></a>  
            
            </div><!-- end of .call-to-action -->
             </div><!-- end of .hero-copy -->
            		         
            
                     
            
       
</div><!-- end of .hero-wrapper -->
        
        
        </div><!-- end of #featured -->
               
<?php get_sidebar('doctors'); ?>
<?php get_footer(); ?>