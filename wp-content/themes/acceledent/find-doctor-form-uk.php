<div class="get-started"><div class="form-header"><h2>Get Started!</h2>
<h3>Talk to a doctor today!</h3>
</div>
<div class="form-wrapper">
<form action="" class="find-doctor">
	<input type="hidden" name="lat_lon" id="lat_lon" value="" />
	<div class="text-input city"><label for="city">City</label><input type="text"  name="city" id="city" maxlength="50" class="searchtextfield" /></div>
	<div class="text-input country"><label for="state">Country</label><select class="searchselectfield searchtextfield" name="state_province" id="state_province">
							<option value=",">-- Select --</option>
							<option value=",AU">Australia</option>
							<option value=",CA">Canada</option>
							<option value=",FR">France</option>
							<option value=",HU">Hungary</option>
							<option value=",IE">Ireland</option>
							<option value=",IT">Italy</option>
							<option value=",KE">Kenya</option>
							<option value=",LU">Luxembourg</option>
							<option value=",NZ">New Zealand</option>
							<option value=",ES">Spain</option>
							<option value=",CH">Switzerland</option>
							<option value=",UK">United Kingdom</option>
						</select>
	</div> <span class="or">or</span>
	<div class="text-input postal"><label for="zip">Postal</label><input type="text" class="searchtextfieldsmall searchtextfield" name="zip_code" id="zip_code" maxlength="10" /></div>
	<div class="call-to-action"><a href="javascript:void(0);" id="homesearchbutton" class="red button">Find a Doctor <span class="arrow"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/red-arrow.svg" alt=""></span>
</a>
</div>
</form></div>