<?php
//Making jQuery Google API
function modify_jquery() {
	if (!is_admin()) {
		// comment out the next two lines to load the local copy of jQuery
		wp_deregister_script('jquery');
		wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', false, '1.9.1');
	}
}
add_action('init', 'modify_jquery');
wp_enqueue_script("jquery");


if (!is_admin()) add_action( 'wp_print_scripts', 'acceledent_add_javascript' );
if (!function_exists('acceledent_add_javascript')) {
	function acceledent_add_javascript( ) {
		wp_enqueue_script( 'fastclick', get_bloginfo('template_directory').'/js/fastclick.js', array( 'jquery' ), '0.6.1', true );
		wp_deregister_script('modernizr');
		wp_register_script( 'modernizr', get_bloginfo('template_directory').'/js/modernizr.custom.44099.js', array( 'jquery' ), '2.6.2', false );
		wp_enqueue_script('modernizr');
		wp_enqueue_script( 'migrate', get_bloginfo('template_directory').'/js/jquery-migrate-1.0.0.min.js', array( 'jquery' ), '1.0.0', false );
		wp_enqueue_script( 'customselect', get_bloginfo('template_directory').'/js/jquery.customSelect.js', array( 'jquery' ), '1.0.0', true );
	}
}



/**
 * Enable Shortcodes on widgets
 */
add_filter('widget_text', 'do_shortcode');

/**
 * Searchform Shortcode
 */
function searchform_func( $atts ){
 return get_search_form();
}
add_shortcode( 'searchform', 'searchform_func' );


/**
 * Homepage Shortcode
 */
function bloginfo_func( $atts ){
 return get_bloginfo('url');
}
add_shortcode( 'myblogurl', 'bloginfo_func' );

/**
 * Template Shortcode
 */
function stylesheetdir_func( $atts ){
 return get_template_directory_uri();
}
add_shortcode( 'mystyleurl', 'stylesheetdir_func' );


/**
 * Find Doctor Shortcode
 */
function finddoctor_shortcode( $attr, $content = null ) {
	extract(shortcode_atts(array(
                "type" => 'ppc',
                "content" => 'Get 5 (mumbo jumbo free) emails <br />to armor you for the PPC battlefields'
    ), $attr));
	$shortcode = "";
	include (TEMPLATEPATH . '/find-doctor-form.php');
    return $shortcode;
}
add_shortcode('finddoctor', 'finddoctor_shortcode');


function register_custom_menu() {
  register_nav_menu('footer-menu-secondary',__( 'Secondary Footer Menu' ));
}
add_action( 'init', 'register_custom_menu' );

add_filter( 'post_thumbnail_html', 'my_post_image_html', 10, 3 );

function my_post_image_html( $html, $post_id, $post_image_id ) {

  global $post;

  $args = array(
	'post_type' => 'attachment',
	'post_status' => null,
	'post_parent' => $post->ID,
	'include'  => $thumb_id
	); 
	
$thumbnail_image = get_posts($args);
$thumbnail_link = '<a href="' . get_permalink( $post_id ) . '" title="' . esc_attr( get_post_field( 'post_title', $post_id ) ) . '">' . $html . '</a>';

if($thumbnail_image[0]->post_excerpt) {
	$html = '<div class="wp-caption alignnone">' . $thumbnail_link . '<p class="wp-caption-text">' . $thumbnail_image[0]->post_excerpt . '</p></div>';
} else {
	$html = $thumbnail_link;
}
  return $html;
}