<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Full Content Template
 *
   Template Name:  News Page (no sidebar)
 *
 * @file           full-width-page.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2011 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/full-width-page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */
?>
<?php get_header(); ?>
<?php $options = get_option('responsive_theme_options'); ?>
		<?php if ($options['breadcrumb'] == 0): ?>
		<?php 
		echo responsive_breadcrumb_lists(); 
		?>
        <?php endif; ?>
       <?php
$my_post = get_post($post->ID); 
$my_title = $my_post->post_title;
$my_excerpt = $my_post->post_excerpt;
$my_link = get_permalink($post->ID);
?> 

        <div class="title-wrapper"><div class="title-inner"><h1 class="post-title"><?php echo $my_title; ?></h1></div></div>
        <div class="content-wrapper">

        <div id="content-full" class="grid col-940">
        
<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
        
        
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
 
                
                <div class="post-entry">
                    <?php the_content(__('Read more &#8250;', 'responsive')); ?>

                
                

                <div class="recent-posts">
                <ul>
					    <?php
					$new_query = new WP_Query();
					$new_query->query('post_type=acceledent_news&showposts=10'.'&paged='.$paged);
					?>
					
					<?php while ($new_query->have_posts()) : $new_query->the_post(); ?>
					<li><span class="date"><?php the_time('F j, Y') ?></span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
					
					<?php endwhile; ?>
					                </ul>
					<a href="<?php echo get_post_type_archive_link( 'acceledent_news' ); ?>">Older Entries &raquo;</a>
				</div><!-- .recent-posts -->
					
<?php wp_reset_query(); ?>

                
            
            <div class="post-edit"><?php edit_post_link(__('Edit', 'responsive')); ?></div> 
            </div><!-- end of #post-<?php the_ID(); ?> -->
            

                </div><!-- end of .post-entry -->
                
                            
        <?php endwhile; ?> 
        

	    <?php else : ?>

        <h1 class="title-404"><?php _e('404 &#8212; Fancy meeting you here!', 'responsive'); ?></h1>
                    
        <p><?php _e('Don&#39;t panic, we&#39;ll get through this together. Let&#39;s explore our options here.', 'responsive'); ?></p>
                    
        <h6><?php printf( __('You can return %s or search for the page you were looking for.', 'responsive'),
	            sprintf( '<a href="%1$s" title="%2$s">%3$s</a>',
		            esc_url( get_home_url() ),
		            esc_attr__('Home', 'responsive'),
		            esc_attr__('&larr; Home', 'responsive')
	                )); 
			 ?></h6>
                    
        <?php get_search_form(); ?>

<?php endif; ?>  
      
        </div><!-- end of #content-full -->
        </div><!-- end of .content-wrapper -->

<?php get_footer(); ?>
