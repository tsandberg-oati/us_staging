<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 *
 *
 * load the theme function files
 * These functions override the functions.php in the parent theme, Responsive
*/
//Making jQuery Google API
function modify_jquery() {
	if (!is_admin()) {
		// comment out the next two lines to load the local copy of jQuery
		wp_deregister_script('jquery');
		wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', false, '1.9.1');
		wp_enqueue_script("jquery");
	}
}
add_action('init', 'modify_jquery');

if (!is_admin())
    add_action('wp_enqueue_scripts', 'responsive_js');

if (!function_exists('responsive_js')) {

    function responsive_js() {
		// JS at the bottom for fast page loading.
		// except for Modernizr which enables HTML5 elements & feature detects.
		/*wp_enqueue_script('modernizr', get_template_directory_uri() . '/js/responsive-modernizr.js', array('jquery'), '2.6.1', false);*/
        wp_deregister_script('responsive-scripts');
        wp_deregister_script('responsive-plugins');
/*wp_enqueue_script('responsive-scripts', get_template_directory_uri() . '/js/responsive-scripts.js', array('jquery'), '1.2.3', true);*/
		/*wp_enqueue_script('responsive-plugins', get_template_directory_uri() . '/js/responsive-plugins.js', array('jquery'), '1.2.2', true);*/
		wp_enqueue_script( 'general', get_stylesheet_directory_uri().'/js/general.js', array( 'jquery' ), '0.0.1', false );
		wp_enqueue_script( 'fastclick', get_stylesheet_directory_uri().'/js/fastclick.js', array( 'jquery' ), '0.6.1', true );
		wp_deregister_script('modernizr');
		wp_register_script( 'modernizr', get_stylesheet_directory_uri().'/js/modernizr.custom.44099.js', array( 'jquery' ), '2.6.2', false );
		wp_enqueue_script('modernizr');
		wp_enqueue_script( 'svgeezy', get_stylesheet_directory_uri().'/js/svgeezy.js', array( 'jquery' ), '1.0.0', false );
		wp_enqueue_script( 'migrate', get_stylesheet_directory_uri().'/js/jquery-migrate-1.0.0.min.js', array( 'jquery' ), '1.0.0', false );
		wp_enqueue_script( 'customselect', get_stylesheet_directory_uri().'/js/jquery.customSelect.js', array( 'jquery' ), '1.0.0', false );
		if(is_page('doctor-locator') || is_page('sales-rep-locator') || is_page('orthodontists') || is_front_page()) :
			wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB8_8H4J_Ka5YhONms6L-Co2K4sR8EpGBU', array('jquery'), '3', false);
		endif;
		if(is_page('doctor-locator')) :
			wp_enqueue_script('map-info-box', get_stylesheet_directory_uri().'/js/map_infobox.js', array('jquery'), '1.0.0', false);
			wp_enqueue_script('doctor-locator', get_stylesheet_directory_uri().'/js/map_doctor_locator.js', array('jquery'), '1.0.0', false);
		endif;
		if(is_page('sales-rep-locator')) :
			wp_enqueue_script('sales-rep', get_stylesheet_directory_uri().'/js/sales_rep_locator.js', array('jquery'), '1.0.0', false);
		endif;
		if(is_page('doctor-locator') || is_page('sales-rep-locator') || is_page('orthodontists') || is_front_page()) :
			wp_enqueue_script('locators', get_stylesheet_directory_uri().'/js/locators.js', array('jquery'), '1.0.0', false);
		endif;
		if(is_page('case-studies')) :
			wp_enqueue_style( 'purecss', 'http://yui.yahooapis.com/pure/0.6.0/pure-min.css', array( ), '0.6.0', false );
			wp_enqueue_style( 'purecss-responsive', 'http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css', array('purecss'), '0.6.0', false );
		endif;
    }
}


/**
 * Custom Taxonomy for Events
 *
 */

add_action( 'init', 'create_event_taxonomy' );
function create_event_taxonomy()
{
   register_taxonomy(
      'events_cat',
      'events',
      array(
         'label' => __( 'Categories' ),
         'rewrite' => array( 'slug' => 'events/categories' ),
         'hierarchical' => true
      )
   );
}

/**
 * Custom Post type for Press Releases
 *
 */

add_action( 'init', 'create_post_type' );
function create_post_type() {

	register_post_type( 'acceledent_news',
		array(
			'labels' => array(
				'name' => __( 'News Archive' ),
				'singular_name' => __( 'News Archive' )
			),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'orthodontists/news-archive'),
			'template' => 'full-width-page'
		)
	);

	register_post_type( 'case_studies',
		array(
			'labels' => array(
				'name' => __( 'Case Studies' ),
				'singular_name' => __( 'Case Study' )
			),
			'taxonomies' => array( 'category', 'post_tag' ),
			'public' => true,
			'has_archive' => false,
			'rewrite' => array('slug' => 'orthodontists/case-studies'),
			'supports' => array('title', 'editor', 'thumbnail', 'excerpt')
		)
	);

	register_post_type( 'acceledent_webinars',
		array(
			'labels' => array(
				'name' => __( "Webinars" ),
				'singular_name' => __( "Webinar" ),
				'menu_name' => __( "Webinars" )
			),
			'public' => true,
			'show_ui' => true,
			'revisions' => true,
			'menu_position' => 30,
			'rewrite' => array('slug' => 'webinars'),
			'supports' => array('title', 'editor', 'author', 'excerpt', 'thumbnail', 'revisions'),
			'public' => true,
			'has_archive' => true
		)
	);
}


/*
function my_recent_news()
 {
      global $post;
      $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

      $html = '';
      $html .='<ul class="recent-news">';
      $my_query = new WP_Query( array(
           'post_type' => 'acceledent_news',
           'posts_per_page' => 5,
           'paged' => $paged
      ));

      if( $my_query->have_posts() ) : while( $my_query->have_posts() ) : $my_query->the_post();
      	   $html .='<li>';
           $html .= '<span class="date">' . get_the_date() . '</span>';
           $html .= '<a href=\'' . get_permalink() . '\'>' . get_the_title() . '</a>';
           $html .='</li>';
      endwhile; endif;
      	$html .='</ul>';

      return $html;
 }
 add_shortcode( 'recent_news', 'my_recent_news' );
*/

/**
 * This function prints post meta data.
 *
 */
if (!function_exists('responsive_post_meta_data')) :

function responsive_post_meta_data() {

	/** ORIGINAL RETURN **/
	/**
		printf( __( '<span class="%1$s">by </span>%4$s on </span>%2$s<span class="%3$s">', 'responsive' ),
		'meta-prep meta-prep-author posted',
		sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="timestamp">%3$s</span></a>',
			esc_url( get_permalink() ),
			esc_attr( get_the_time() ),
			esc_html( get_the_date() )
		),
		'byline',
		sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>',
			get_author_posts_url( get_the_author_meta( 'ID' ) ),
			sprintf( esc_attr__( 'View all posts by %s', 'responsive' ), get_the_author() ),
			esc_attr( get_the_author() )
			)
		);
	**/
	/** END ORIGINAL CODE **/

	if( get_post_type() == 'acceledent_news' ):
		return;
	else:
		printf( __( '<span class="%1$s">posted</span> on </span>%2$s<span class="%3$s">', 'responsive' ),
		'meta-prep meta-prep-author posted',
		sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="timestamp">%3$s</span></a>',
			esc_url( get_permalink() ),
			esc_attr( get_the_time() ),
			esc_html( get_the_date() )
		),
		'byline'
		);
	endif;
}
endif;

/**
 * This function overrides the responsive breadcrumb function
 *
 */

if (!function_exists('responsive_breadcrumb_lists')) :

function responsive_breadcrumb_lists() {

	/* === OPTIONS === */
	$text['home']     = __('Home','responsive'); // text for the 'Home' link
	$text['category'] = __('%s','responsive'); // text for a category page
	$text['search']   = __('Search results for: %s','responsive'); // text for a search results page
	$text['tag']      = __('Posts tagged %s','responsive'); // text for a tag page
	$text['author']   = __('View all posts by %s','responsive'); // text for an author page
	$text['404']      = __('Error 404','responsive'); // text for the 404 page

	$showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
	$showOnHome  = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
	$delimiter   = ' <span class="chevron">&#8250;</span> '; // delimiter between crumbs
	$before      = '<span class="breadcrumb-current">'; // tag before the current crumb
	$after       = '</span>'; // tag after the current crumb
	/* === END OF OPTIONS === */

	global $post, $paged, $page;
	$homeLink = home_url('/');
	$linkBefore = '<span typeof="v:Breadcrumb">';
	$linkAfter = '</span>';
	$linkAttr = ' rel="v:url" property="v:title"';
	$link = $linkBefore . '<a' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;

	if ( is_front_page()) {

		if ($showOnHome == 1) echo '<div class="breadcrumb-wrapper"><div class="breadcrumb-list"><a href="' . $homeLink . '">' . $text['home'] . '</a></div></div>';

	} else {

		echo '<div class="breadcrumb-wrapper"><div class="breadcrumb-list" xmlns:v="http://rdf.data-vocabulary.org/#">' . sprintf($link, $homeLink, $text['home']) . $delimiter;

		if ( is_home() ) {
			if ($showCurrent == 1) echo $before . get_the_title( get_option('page_for_posts', true) ) . $after;

		} elseif ( is_category() ) {
			$thisCat = get_category(get_query_var('cat'), false);
			if ($thisCat->parent != 0) {
				$cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
				$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
				$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
				echo $cats;
			}
			echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;

		} elseif ( is_search() ) {
			echo $before . sprintf($text['search'], get_search_query()) . $after;

		} elseif ( is_day() ) {
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
			echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
			echo $before . get_the_time('d') . $after;

		} elseif ( is_month() ) {
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
			echo $before . get_the_time('F') . $after;

		} elseif ( is_year() ) {
			echo $before . get_the_time('Y') . $after;

		} elseif ( is_single() && !is_attachment() ) {
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				if ( get_post_type() == 'acceledent_news' ){
					printf($link, $homeLink . 'orthodontists/', 'Orthodontists');
					echo $delimiter;
					printf($link, $homeLink . $slug['slug'] . '/', $post_type->labels->singular_name);
				}else{
					printf($link, $homeLink . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
				}
				if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
			} else {
				$cat = get_the_category(); $cat = $cat[0];
				$cats = get_category_parents($cat, TRUE, $delimiter);
				if ($showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
				$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
				$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
				echo $cats;
				if ($showCurrent == 1) echo $before . get_the_title() . $after;
			}

		} elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
			$post_type = get_post_type_object(get_post_type());
			if ( is_archive() && is_post_type_archive('acceledent_news') ) {
				printf($link, $homeLink . 'orthodontists/', 'Orthodontists');
					echo $delimiter;
			}
			echo $before . $post_type->labels->singular_name . $after;

		} elseif ( is_attachment() ) {
			$parent = get_post($post->post_parent);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			$cats = get_category_parents($cat, TRUE, $delimiter);
			$cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
			$cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
			echo $cats;
			printf($link, get_permalink($parent), $parent->post_title);
			if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;

		} elseif ( is_page() && !$post->post_parent ) {
			if ($showCurrent == 1) echo $before . get_the_title() . $after;

		} elseif ( is_page() && $post->post_parent ) {
			$parent_id  = $post->post_parent;
			$breadcrumbs = array();
			while ($parent_id) {
				$page = get_page($parent_id);
				$breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
				$parent_id  = $page->post_parent;
			}
			$breadcrumbs = array_reverse($breadcrumbs);
			for ($i = 0; $i < count($breadcrumbs); $i++) {
				echo $breadcrumbs[$i];
				if ($i != count($breadcrumbs)-1) echo $delimiter;
			}
			if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;

		} elseif ( is_tag() ) {
			echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;

		} elseif ( is_author() ) {
	 		global $author;
			$userdata = get_userdata($author);
			echo $before . sprintf($text['author'], $userdata->display_name) . $after;

		} elseif ( is_404() ) {
			echo $before . $text['404'] . $after;

		}if ( get_query_var('paged') ) {
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
			echo $delimiter . sprintf( __( 'Page %s', 'responsive' ), max( $paged, $page ) );
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
		}

		echo '</div></div>';

	}
} // end responsive_breadcrumb_lists

endif;

/**
 * Enable Shortcodes on widgets
 */
add_filter('widget_text', 'do_shortcode');

/**
 * Searchform Shortcode
 */
function searchform_func( $atts ){
 return get_search_form();
}
add_shortcode( 'searchform', 'searchform_func' );


/**
 * Homepage Shortcode
 */
function bloginfo_func( $atts ){
 return get_bloginfo('url');
}
add_shortcode( 'myblogurl', 'bloginfo_func' );

/**
 * Template Shortcode
 */
function stylesheetdir_func( $atts ){
 return get_stylesheet_directory_uri();
}
add_shortcode( 'mystyleurl', 'stylesheetdir_func' );


/**
 * Find Doctor Shortcode
 */
function finddoctor_shortcode( $attr ) {
	include (STYLESHEETPATH . '/find-doctor-form.php');
}
add_shortcode('finddoctor', 'finddoctor_shortcode');

function finddoctor_uk_shortcode( $attr ) {
	include (STYLESHEETPATH . '/find-doctor-form-uk.php');
}
add_shortcode('finddoctor-uk', 'finddoctor_uk_shortcode');

function finddoctor_full_uk_shortcode( $attr ) {
	include (STYLESHEETPATH . '/find-doctor-form-full-uk.php');
}
add_shortcode('finddoctor-uk-full', 'finddoctor_full_uk_shortcode');

function finddoctor_full_shortcode( $attr ) {
	include (STYLESHEETPATH . '/find-doctor-form-full.php');
}
add_shortcode('finddoctor-full', 'finddoctor_full_shortcode');

/**
 * Register Secondary Footer Menu
 */

function register_custom_menu() {
  register_nav_menu('footer-menu-secondary',__( 'Secondary Footer Menu' ));
}
add_action( 'init', 'register_custom_menu' );

add_filter( 'post_thumbnail_html', 'my_post_image_html', 10, 3 );

function my_post_image_html( $html, $post_id, $post_image_id ) {

  global $post;

  $args = array(
	'post_type' => 'attachment',
	'post_status' => null,
	'post_parent' => $post->ID,
	'include'  => $thumb_id
	);

$thumbnail_image = get_posts($args);
$thumbnail_link = '<a href="' . get_permalink( $post_id ) . '" title="' . esc_attr( get_post_field( 'post_title', $post_id ) ) . '">' . $html . '</a>';

if($thumbnail_image[0]->post_excerpt) {
	$html = '<div class="wp-caption alignnone">' . $thumbnail_link . '<p class="wp-caption-text">' . $thumbnail_image[0]->post_excerpt . '</p></div>';
} else {
	$html = $thumbnail_link;
}
  return $html;
}


/**
 * Get top page ancestor
 */
function get_top_parent_page_id() {
    global $post;
    // Check if page is a child page (any level)
    if ($post->ancestors) {
        //  Grab the ID of top-level page from the tree
        return end( get_post_ancestors($post) );
    } else {
        // Page is the top level, so use  it's own id
        return $post->ID;
    }
}
/**
 * Check if child of a certain page
 */
function is_child($pageID) {
	global $post;
	if( is_page() && (get_top_parent_page_id()==$pageID) ) {
               return true;
	} else {
               return false;
	}
}
/**
 * Doctors Pages add class to body tag (Requires get_top_parent_page_id function ^)
 */
if (function_exists('get_top_parent_page_id')) :

add_filter('body_class','my_class_names');

function my_class_names($classes) {

get_top_parent_page_id();
$parent_page = get_top_parent_page_id($post->ID);

if(($parent_page == '75') || is_page('75')) :
	// add 'class-name' to the $classes array
	$classes[] = 'doctors-pages';
endif;
	// return the $classes array
	return $classes;
}

endif;
/**
 * Add has_children class to navs with subpages
 */
class My_Walker_Nav_Menu extends Walker_Nav_Menu{
  public function display_element($el, &$children, $max_depth, $depth = 0, $args, &$output){
    $id = $this->db_fields['id'];

    if(isset($children[$el->$id]))
      $el->classes[] = 'has_children';

    parent::display_element($el, $children, $max_depth, $depth, $args, $output);
  }
}
/**
 * This function will check if Advanced Custom Fields Plugin is installed. If it is it will output $my_field. If not it will echo $my_field_fallback.
 *
 */
function check_field($my_field, $my_field_fallback) {
	if (function_exists('the_field') && get_field($my_field)) {
		return the_field($my_field);
	} else {
		echo $my_field_fallback;
	}
}

/**
 * This function will check if there is a featured image and if not will output the fallback image.
 *
 */
function check_thumbs($fallback_hero) {
$hero_image;
if (has_post_thumbnail( $post->ID ) ) {
	$hero_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	echo $hero_image[0];
} else {
	echo get_stylesheet_directory_uri() .'/img/'.$fallback_hero;
}
}
/*** EMAIL ENCODE SHORTCODE  (usage:  [email]your@email.com[/email] ) */

function email_encode_function( $atts, $content ){
return '<a href="mailto:'.antispambot($content).'">'.antispambot($content).'</a>';
}
add_shortcode( 'email', 'email_encode_function' );

function phone_encode_function($atts, $content){
	return antispambot($content);
}
add_shortcode( 'phone-number', 'phone_encode_function' );
/*
*  Show selected image if value exists
*  Return value = URL
*/


/*
*  Show selected image cropped to a specific size
*  Return value = ID ( allows us to get more data about the image )
*  This example uses the WP function: wp_get_attachment_image - http://codex.wordpress.org/Function_Reference/wp_get_attachment_image
*/
add_image_size( 'asset_hero', 260, 180, true );

function check_field_image($my_field, $my_field_fallback) {
	if (function_exists('the_field') && get_field($my_field)) {
		$attachment_id = get_field($my_field);
		$size = "asset_hero"; // (thumbnail, medium, large, full or custom size)

		echo wp_get_attachment_image( $attachment_id, $size );

		}
}
/*Remove br tags from galleries */

add_filter('the_content', 'remove_gallery_break', 11, 2);
function remove_gallery_break($output) {
    return preg_replace('/<br[^>]*>/', '<br>', $output);
}

// Register 'Recent Custom Posts' widget
add_action( 'widgets_init', 'init_rcp_recent_posts' );
function init_rcp_recent_posts() { return register_widget('rcp_recent_posts'); }

class rcp_recent_posts extends WP_Widget {
	/** constructor */
	function rcp_recent_posts() {
		parent::WP_Widget( 'rcp_recent_custom_posts', $name = 'Recent Custom Posts' );
	}

	/**
	* This is our Widget
	**/
	function widget( $args, $instance ) {
		global $post;
		extract($args);

		// Widget options
		$title 	 = apply_filters('widget_title', $instance['title'] ); // Title
		$cpt 	 = $instance['types']; // Post type(s)
	    $types   = explode(',', $cpt); // Let's turn this into an array we can work with.
		$number	 = $instance['number']; // Number of posts to show

        // Output
		echo $before_widget;

	    if ( $title ) echo $before_title . $title . $after_title;

		$mlq = new WP_Query(array( 'post_type' => $types, 'showposts' => $number ));
		if( $mlq->have_posts() ) :
		?>
		<ul>
		<?php while($mlq->have_posts()) : $mlq->the_post(); ?>
		<li><a href="<?php the_permalink() ?>" title="<?php echo esc_attr(get_the_title() ? get_the_title() : get_the_ID()); ?>"><?php if ( get_the_title() ) the_title(); else the_ID(); ?></a></li>
		<?php wp_reset_query();
		endwhile; ?>
		</ul>

		<?php endif; ?>
		<?php
		// echo widget closing tag
		echo $after_widget;
	}

	/** Widget control update */
	function update( $new_instance, $old_instance ) {
		$instance    = $old_instance;

		//Let's turn that array into something the Wordpress database can store
		$types       = implode(',', (array)$new_instance['types']);

		$instance['title']  = strip_tags( $new_instance['title'] );
		$instance['types']  = $types;
		$instance['number'] = strip_tags( $new_instance['number'] );
		return $instance;
	}

	/**
	* Widget settings
	**/
	function form( $instance ) {

		    // instance exist? if not set defaults
		    if ( $instance ) {
				$title  = $instance['title'];
		        $types  = $instance['types'];
		        $number = $instance['number'];
		    } else {
			    //These are our defaults
				$title  = '';
		        $types  = 'post';
		        $number = '5';
		    }

			//Let's turn $types into an array
			$types = explode(',', $types);

			//Count number of post types for select box sizing
			$cpt_types = get_post_types( array( 'public' => true ), 'names' );
			foreach ($cpt_types as $cpt ) {
			   $cpt_ar[] = $cpt;
			}
			$n = count($cpt_ar);
			if($n > 10) { $n = 10;}

			// The widget form
			?>
			<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __( 'Title:' ); ?></label>
			<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" class="widefat" />
			</p>
			<p>
			<label for="<?php echo $this->get_field_id('types'); ?>"><?php echo __( 'Select post type(s):' ); ?></label>
			<select name="<?php echo $this->get_field_name('types'); ?>[]" id="<?php echo $this->get_field_id('types'); ?>" class="widefat" style="height: auto;" size="<?php echo $n ?>" multiple>
				<?php
				$args = array( 'public' => true );
				$post_types = get_post_types( $args, 'names' );
				foreach ($post_types as $post_type ) { ?>
					<option value="<?php echo $post_type; ?>" <?php if( in_array($post_type, $types)) { echo 'selected="selected"'; } ?>><?php echo $post_type;?></option>
				<?php }	?>
			</select>
			</p>
			<p>
			<label for="<?php echo $this->get_field_id('number'); ?>"><?php echo __( 'Number of posts to show:' ); ?></label>
			<input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" size="3" />
			</p>
	<?php
	}

} // class rcp_recent_posts


function find_tld($url){

$purl  = parse_url($url);
$host  = strtolower($purl['host']);

$valid_tlds = ".com .co.uk";

    $tld_regex = '#(.*?)([^.]+)('.str_replace(array('.',' '),array('\\.','|'),$valid_tlds).')$#';

    //remove the extension
    preg_match($tld_regex,$host,$matches);

    if(!empty($matches) && sizeof($matches) > 2){
        $extension = array_pop($matches);
        $tld = array_pop($matches);
        return $tld.$extension;

    }else{ //change to "false" if you prefer
        return $host;
    }

}

add_filter( 'gform_form_tag', 'form_tag', 10, 2 );
function form_tag( $form_tag, $form ) {
    if ( $form['id'] != 3 ) {
        return $form_tag;
    }
    $form_tag = preg_replace( "|action='(.*?)'|", "action='http://go.acceledent.com/l/138751/2016-04-04/5blg5'", $form_tag );
    return $form_tag;
}

add_filter( 'gform_field_input', 'tracker', 10, 5 );
function tracker( $input, $field, $value, $lead_id, $form_id ) {
    if ( $form_id == 3 && $field->id == 12 ) {
        $input = '<input type="hidden" id="Sales_Rep" name="Sales_Rep"';
    }
    return $input;
}
