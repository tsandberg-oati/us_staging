<?php 
/******************************
**** Display Press Details ****
******************************/		
?> 

<div class="press-postcont">
	<p class="meta-date"><?php echo the_date('F d, Y'); ?></p>
    <h2><a class="heading" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <p><?php the_excerpt(); ?></p>   
</div>