<div class="get-started locator">
			
			<div class="form-header">
			<h2>Doctor Locator</h2>
			
			<p class="hide-980">Please enter your city / state or zip code to contact a doctor in your area.</p>
			</div><!-- end of .form-header -->
			<div class="form-wrapper">

		<form name="searchform" id="searchform" action="" method="POST" class="find-doctor" >
			<input type="hidden" name="search_bounds" id="search_bounds" value="" />
			<input type="hidden" name="extra_bounds" id="extra_bounds" value="" />
			<input type="hidden" name="lat_lon" id="lat_lon" value="" />
			<div class="left-col">
			<div class="text-input"><label for="city">City</label><input type="text"  name="city" id="city" maxlength="50" tabindex="1" class="searchtextfield" /></div>
					<div class="text-input text_state"><label for="search_radius">Country</label><select class="searchselectfield searchtextfield" name="state_province" id="state_province" tabindex="2">
							<option value=",">-- Select --</option>
							<option value=",AU">Australia</option>
							<option value=",CA">Canada</option>
							<option value=",FR">France</option>
							<option value=",HU">Hungary</option>
							<option value=",IE">Ireland</option>
							<option value=",IT">Italy</option>
							<option value=",KE">Kenya</option>
							<option value=",LU">Luxembourg</option>
							<option value=",NZ">New Zealand</option>
							<option value=",ES">Spain</option>
							<option value=",CH">Switzerland</option>
							<option value=",UK">United Kingdom</option>
						</select>
					</div>
					<span class="or">or</span>
					<div class="text-input">
					<label for="zip_code">Postal</label><input type="text" class="searchtextfield searchtextfieldsmall" name="zip_code" id="zip_code" maxlength="10" tabindex="3" />
					</div>
</div><div class="right-col"><div class="text-input text_radius"><label for="search_radius">Radius</label><select class="searchselectfield searchtextfield" name="search_radius" id="search_radius" tabindex="4">
				<option value="5">5 kilometers</option>
				<option value="10">10 kilometers</option>
				<option value="25" selected>25 kilometers</option>
				<option value="50">50 kilometers</option>
				<option value="100">100 kilometers</option>
				<option value="250">250 kilometers</option>
			</select></div>
					<div class="buttons">
					<a href="javascript:void(0);" id="locator" class="location" style="display:none;" onclick="setLocation();"><span class="arrow"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/get-my-location.svg" alt=""></span> Get my Location</a>
					<a href="javascript:void(0);" id="searchbutton" class="red button searchbutton">Search <span class="arrow"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/red-arrow.svg" alt=""></span></a></div>
					</div>
		</form><!-- end of .left-col --></div> <!-- end of .form-wrapper -->
</div> <!-- end of .get-started -->
		<div class="resultsdiv" id="resultsdiv" style="display:none;">
			<div class="map-wrapper"><div id="map_canvas" style=""></div></div><div id="results_listing" style=""></div>
		</div><!-- end of #resultsdiv -->