<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

?>
<?php get_header(); ?>



<?php $options = get_option('responsive_theme_options'); ?>
<?php if ($options['breadcrumb'] == 0) echo responsive_breadcrumb_lists(); ?>

<?php
$my_post = get_post($post->ID); 
$my_title = $my_post->post_title;
$my_excerpt = $my_post->post_excerpt;
$my_link = get_permalink($post->ID);
?> 
        
    <div class="title-wrapper">
		<div class="title-inner">
			<h1 class="post-title"><?php echo $my_title; ?></h1>
		</div>
	</div>
    <div class="content-wrapper">
    <?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
      <div id="content-full" class="grid col-940">
        <div id="post-24" class="post-24 page type-page status-publish hentry">
          <div class="post-entry news-events">
              <div id="main_content" class="news-events">
              
              	<div class="event-row">                            	
	              	<?php the_content(); ?>	              						  	 

<div class="eventleft-cont"> 

<?php 
	$args = array(
		'post_type'      => 'events',
		'order'          => 'DESC',
		'orderby'        => 'meta_value',
		'meta_key'       => 'event_date_and_year',
		'posts_per_page' => '-1',
		'meta_query'     => array(
			array(
				'key' => 'event_date_and_year',
	            'value' => date('Y-m-d'),
	            'compare' => '<'
			)        
	    ),
		'tax_query' 	 => array(
					            array(
						            'taxonomy' => 'events_cat',
						            'field' => 'id',
						            'terms' => 17,
						            'operator' => 'IN',
					            )
        )
	);
	query_posts( $args ); 
	
	if ( have_posts() ) :  
		 while ( have_posts() ) : the_post(); 
?>


<div class="event-left"> 
	<?php get_template_part( 'excerpt-event' ); ?>
</div>
<?php endwhile; endif; wp_reset_query();?>                                    
                    
                   

<div style="clear:both"></div>
         
</div> <!-- .eventleft-cont -->


              </div>
			  </div> <!-- .main_content -->
			  
			  <?php get_sidebar( 'news-events' ); ?>
          
          
        </div><!-- end of .post-entry --> 
        
      </div><!-- end of #content-full --> 
    </div><!-- end of .content-wrapper --> 
    
  </div>
  <?php endwhile; ?> 
        
        <?php if (  $wp_query->max_num_pages > 1 ) : ?>
        <div class="navigation">
			<div class="previous"><?php next_posts_link( __( '&#8249; Older posts', 'responsive' ) ); ?></div>
            <div class="next"><?php previous_posts_link( __( 'Newer posts &#8250;', 'responsive' ) ); ?></div>
		</div><!-- end of .navigation -->
        <?php endif; ?>
  <?php else : ?>

        <h1 class="title-404"><?php _e('404 &#8212; Fancy meeting you here!', 'responsive'); ?></h1>
                    
        <p><?php _e('Don&#39;t panic, we&#39;ll get through this together. Let&#39;s explore our options here.', 'responsive'); ?></p>
                    
        <h6><?php printf( __('You can return %s or search for the page you were looking for.', 'responsive'),
	            sprintf( '<a href="%1$s" title="%2$s">%3$s</a>',
		            esc_url( get_home_url() ),
		            esc_attr__('Home', 'responsive'),
		            esc_attr__('&larr; Home', 'responsive')
	                )); 
			 ?></h6>
                    
        <?php get_search_form(); ?>

<?php endif; ?> 
  <!-- end of #wrapper --> 
</div>
       
        
        
        
<?php get_footer(); ?>
