<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Search Template
 *
 *
 * @file           search.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/search.php
 * @link           http://codex.wordpress.org/Theme_Development#Search_Results_.28search.php.29
 * @since          available since Release 1.0
 */
?>
<?php get_header(); ?>
<?php $options = get_option('responsive_theme_options'); ?>
		<?php if ($options['breadcrumb'] == 0): ?>
		<?php echo responsive_breadcrumb_lists(); ?>
        <?php endif; ?>

        <div class="title-wrapper"><div class="title-inner"><h1 class="post-title">Search Results</h1></div></div>
        <div class="content-wrapper">
                <div id="content-archive" class="grid col-776">

<?php if (have_posts()) : ?>

    <h6><?php printf(__('Search results for: %s', 'responsive' ), '<span>' . get_search_query() . '</span>'); ?></h6>

		<?php while (have_posts()) : the_post(); ?>
        <div class="post-wrapper">
        	 <?php if ( has_post_thumbnail()) : ?>
        	         	<div class="thumbnail-wrapper">

                        <?php the_post_thumbnail('thumbnail', array('')); ?>
                        
                    
                    <?php if ( comments_open() ) : ?>
                        <span class="comments-link">
                    <?php comments_popup_link(__('No Comments', 'responsive'), __('1 Comment &darr;', 'responsive'), __('% Comments', 'responsive')); ?>
                        </span>
                    <?php endif; ?>
                    </div>
        	<?php endif; ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <div class="post-title-wrapper"><h2 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__( 'Permanent Link to %s', 'responsive' ), the_title_attribute( 'echo=0' )); ?>"><?php the_title(); ?></a></h2>
                
                <div class="post-meta">
                <?php responsive_post_meta_data(); ?> 
                </div><!-- end of .post-meta -->
                </div><!-- end of .post-title-wrapper -->
                <div class="post-entry">
                                       <div class="excerpt">
                    <?php the_excerpt(); ?>
                    </div>
                    <?php wp_link_pages(array('before' => '<div class="pagination">' . __('Pages:', 'responsive'), 'after' => '</div>')); ?>
                </div><!-- end of .post-entry -->
                

            <div class="post-edit"><?php edit_post_link(__('Edit', 'responsive')); ?></div>             
            </div><!-- end of #post-<?php the_ID(); ?> -->
            
            <?php comments_template( '', true ); ?>
        </div>    
        <?php endwhile; ?> 
        
        <?php if (  $wp_query->max_num_pages > 1 ) : ?>
        <div class="navigation">
			<div class="previous"><?php next_posts_link( __( '&#8249; Older posts', 'responsive' ) ); ?></div>
            <div class="next"><?php previous_posts_link( __( 'Newer posts &#8250;', 'responsive' ) ); ?></div>
		</div><!-- end of .navigation -->
        <?php endif; ?>

	    <?php else : ?>

        <h3 class="title-404"><?php printf(__('Your search for %s did not match any entries.', 'responsive' ), get_search_query() ); ?></h3>
                    
        <p><?php _e('Don&#39;t panic, we&#39;ll get through this together. Let&#39;s explore our options here.', 'responsive'); ?></p>
                    
        <h6><?php printf( __('You can return %s or search for the page you were looking for.', 'responsive'),
	            sprintf( '<a href="%1$s" title="%2$s">%3$s</a>',
		            esc_url( get_home_url() ),
		            esc_attr__('Home', 'responsive'),
		            esc_attr__('&larr; Home', 'responsive')
	                )); 
			 ?></h6>
                    
        <?php get_search_form(); ?>

<?php endif; ?>  
      
        </div><!-- end of #content-archive -->
<?php get_sidebar('blog'); ?>
</div><!-- end of .content-wrapper -->  
<?php get_footer(); ?>
