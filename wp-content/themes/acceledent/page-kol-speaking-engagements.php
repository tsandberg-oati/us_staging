<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

?>
<?php get_header(); ?>



<?php $options = get_option('responsive_theme_options'); ?>
		<?php if ($options['breadcrumb'] == 0): ?>
		<?php 
		echo responsive_breadcrumb_lists(); 
		?>
        <?php endif; ?>
        
         <?php
$my_post = get_post($post->ID); 
$my_title = $my_post->post_title;
$my_excerpt = $my_post->post_excerpt;
$my_link = get_permalink($post->ID);
?> 
        
        <div class="title-wrapper">
        <div class="title-inner"><h1 class="post-title"><?php echo $my_title; ?></h1></div>
		</div>
    <div class="content-wrapper">
    <?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
      <div id="content-full" class="grid col-940">
        <div id="post-24" class="post-24 page type-page status-publish hentry">
          <div class="post-entry news-events">
            <div>
              <div id="main_content" class="news-events">
              
              	<div class="event-row">           		
              		<?php the_content(); ?>             		        
			  	<div class="eventleft-cont kol">

<?php 
		$args = array(
			'post_type'      => 'events',
			'order'          => 'ASC',			
			'posts_per_page' => '1',
			'orderby'        => 'meta_value',
			'meta_key'       => 'event_date_and_year',						
		   
	        // Query only spotlight features
			'meta_query' => array(
				array(
					'key' => 'event_date_and_year',
		            'value' => date('Y-m-d'),
		            'compare' => '>'
				),
		        array(
		            'key' => 'event_spotlight_feature',
		            'value' => '"spotlight"',
		            'compare' => 'LIKE'
		        )		        
		    ),
		    
		    // Query all non KOL speaking events
			'tax_query' 	 => array(
						            array(
							            'taxonomy' => 'events_cat',
							            'field' => 'id',
							            'terms' => 17,
							            'operator' => 'IN',
						            )
			)	        
		);
		query_posts( $args );
?> 				

		<?php if ( have_posts() ) : the_post(); ?>
			<div class="event-left spotlight"> 
				<?php get_template_part( 'excerpt-event' ); ?>
			</div>
		<?php endif; ?>



<div class="eventleft-heading">Upcoming Speaking Engagements</div>
<hr class="grey" />

<?php 
	$args = array(
		'post_type'      => 'events',
		'order'          => 'ASC',
		'orderby'        => 'meta_value',
		'meta_key'       => 'event_date_and_year',
		'posts_per_page' => '100',
		'meta_query' => array(
				array(
					'key' => 'event_date_and_year',
		            'value' => date('Y-m-d'),
		            'compare' => '>'
				)        
		    ),
		'tax_query' 	 => array(
            array(
	            'taxonomy' => 'events_cat',
	            'field' => 'id',
	            'terms' => 17,
	            'operator' => 'IN',
            )
        )
	);
	query_posts( $args ); 
	
	if ( have_posts() ) :  
	 while ( have_posts() ) : the_post();  
?>
          
<div class="event-left"> 
	<?php get_template_part( 'excerpt-event' ); ?>
</div>
<?php endwhile; ?> 

<p>&nbsp;</p>
<p>&nbsp;</p>

<?php endif; wp_reset_query(); ?>


<?php 
	$args = array(
		'post_type'      => 'events',
		'order'          => 'DESC',
		'orderby'        => 'meta_value',
		'meta_key'       => 'event_date_and_year',
		'posts_per_page' => '2',
		'meta_query'     => array(
			array(
				'key' => 'event_date_and_year',
	            'value' => date('Y-m-d'),
	            'compare' => '<'
			)        
	    ),
		'tax_query' 	 => array(
					            array(
						            'taxonomy' => 'events_cat',
						            'field' => 'id',
						            'terms' => 17,
						            'operator' => 'IN',
					            )
        )
	);
	query_posts( $args ); 
?>
	
<?php if ( have_posts() ) :  ?>
		
	<div class="eventleft-heading">Past Speaking Engagements</div>
<hr class="grey" />
		
<?php  while ( have_posts() ) : the_post();  ?>

	<div class="event-left"> 
		<?php get_template_part( 'excerpt-event' ); ?>
	</div>

<?php endwhile; ?>

<a href="/orthodontists/news-and-events/past-kol-speaking-engagements/" class="button-block fr">View All Past Speaking Engagements</a> 

<?endif; wp_reset_query();?>
 
                    
                    
                    
                   

 <div style="clear:both"></div>
</div>
</div>            
            
            <div class="post-edit"></div>                     
            
          </div>
          
          <?php get_sidebar( 'news-events' ); ?> 
          
        </div><!-- end of .post-entry --> 
        
      </div><!-- end of #content-full --> 
    </div><!-- end of .content-wrapper --> 
    
  </div>
  <?php endwhile; ?> 
        
        
  <?php else : ?>

        <h1 class="title-404"><?php _e('404 &#8212; Fancy meeting you here!', 'responsive'); ?></h1>
                    
        <p><?php _e('Don&#39;t panic, we&#39;ll get through this together. Let&#39;s explore our options here.', 'responsive'); ?></p>
                    
        <h6><?php printf( __('You can return %s or search for the page you were looking for.', 'responsive'),
	            sprintf( '<a href="%1$s" title="%2$s">%3$s</a>',
		            esc_url( get_home_url() ),
		            esc_attr__('Home', 'responsive'),
		            esc_attr__('&larr; Home', 'responsive')
	                )); 
			 ?></h6>
                    
        <?php get_search_form(); ?>

<?php endif; ?> 
  <!-- end of #wrapper --> 
</div>
       
        
        
        
<?php get_footer(); ?>
