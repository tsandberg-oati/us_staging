<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Home Widgets Template
 *
 *
 * @file           sidebar-home.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/sidebar-home.php
 * @link           http://codex.wordpress.org/Theme_Development#Widgets_.28sidebar.php.29
 * @since          available since Release 1.0
 */
?>  
    <div id="widgets" class="home-widgets">
    
            <div class="grid col-460 home-left">
            
            <div class="widget-wrapper">
            
                <div class="textwidget">
                		<div class="blurb">
	                	<?php  check_field('doctors_left_widget'); ?>
	                	</div>
				</div>
            
			</div><!-- end of .widget-wrapper -->

        </div><!-- end of .col-300 -->
        
        
        <div class="grid col-460 fit home-right">
                    
            <div class="widget-wrapper">
            
                <div class="textwidget">
                	<div class="blurb">
                		<?php check_field('doctors_right_widget'); ?>
                	</div>
                </div>
            
			</div><!-- end of .widget-wrapper -->

        </div><!-- end of .col-300 -->
<div class="clearfix"></div>
<div class="grid col-460 doctors-product">
            
            <div class="widget-wrapper">
            
                <div class="textwidget"><div class="product-wrapper"><div class="the-product">
                <h3><?php check_field('doctors_product_headline','The Product'); ?></h3>
                <?php check_field_image('doctors_product_image'); ?>
                <div class="doctors-widget-copy"><p><?php check_field('doctors_product_widget','Learn more about the AcceleDent&reg; Aura and see it in action!'); ?></p>
<div class="call-to-action"><a href="<?php check_field('product_cta_link','#nogo'); ?>" class="blue button">Read More <span class="arrow"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/blue-arrow.svg" alt=""></svg></span></a>  </div></div>

</div>
				
</div></div>
            
			</div><!-- end of .widget-wrapper -->

        </div><!-- end of .col-300 -->


<div class="grid col-460 doctors-product fit">
            
            <div class="widget-wrapper">
            
                <div class="textwidget"><div class="product-wrapper"><div class="the-product">
                <h3><?php check_field('doctors_results_headline','The Results'); ?></h3>
                <?php check_field_image('doctors_results_image'); ?>
                <div class="doctors-widget-copy"><p><?php check_field('doctors_results_widget','Learn more about the AcceleDent&reg; Aura and see it in action!'); ?></p>
<div class="call-to-action"><a href="<?php check_field('results_cta_link','#nogo'); ?>" class="blue button">Read More <span class="arrow"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/blue-arrow.svg" alt=""></span></a>  </div>
                </div>
</div>
				
</div></div>
            
			</div><!-- end of .widget-wrapper -->

        </div><!-- end of .col-300 -->
        


        <div id="home-right" class="grid col-300 fit">
              	<?php include (STYLESHEETPATH . '/sales-rep-form.php'); ?>
		</div><!-- end of .col-300 fit -->
    </div><!-- end of #widgets -->