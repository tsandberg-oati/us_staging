<?php
// Exit if accessed directly
if ( !defined('ABSPATH')) exit;
?>

<div id="rnav">
	
	<div class="eventleft-heading"><a href="/orthodontists/news-and-events/">NEWS AND EVENTS</a></div>

   <?php wp_nav_menu( array('menu' => 'doctors-menu', 'menu_id' => 'news-events', 'menu_class' => 'nav' )); ?>
   
   <p>&nbsp;</p>
   
   <?php if(is_page(19890)): ?>
   View Our Product Videos <a href="http://acceledent.com/what-it-is/acceledent-videos/">Here</a>  
   <?php endif; ?>


</div>
<div class="clear"></div>