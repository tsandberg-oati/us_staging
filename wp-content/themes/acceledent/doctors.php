<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Template Name: Doctors
 */
?>
<?php get_header(); ?>


        <div id="featured" class="grid col-940" >
 <div style="max-width:1250px;margin:0 auto;">
        <?php echo do_shortcode("[metaslider id=19699]"); ?>
        </div>
        </div><!-- end of #featured -->
               
<?php get_sidebar('doctors'); ?>
<?php get_footer(); ?>