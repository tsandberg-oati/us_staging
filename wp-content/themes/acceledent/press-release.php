<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Template Name: Press Release
 *
 *
 * @file           press-release.php
 * @package        Responsive 
 * @author         Dhibakar J 
 * @copyright      2003 - 2014 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @since          available since Release 1.0
 */
?>
<?php get_header(); ?>
<?php $options = get_option('responsive_theme_options'); ?>
<?php if ($options['breadcrumb'] == 0): ?>
<?php 
		echo responsive_breadcrumb_lists(); 
		?>
<?php endif; ?>
<?php
$my_post = get_post($post->ID); 
$my_title = $my_post->post_title;
$my_excerpt = $my_post->post_excerpt;
$my_link = get_permalink($post->ID);
?>

<div class="title-wrapper">
  <div class="title-inner">
    <h1 class="post-title"><?php echo $my_title; ?></h1>
  </div>
</div>
<div class="content-wrapper">
  <?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>
  <div id="content-full" class="grid col-940">
    <div id="post-24" class="post-24 page type-page status-publish hentry">
      <div class="post-entry">
        <div>
          <div id="main_content">
          
            <?php 
                        the_content();
                        $youtube = get_post_meta($post->ID, 'youtube', true);
                        $thumb1 = get_post_meta($post->ID, 'thumb1', true);
                        $thumb2 = get_post_meta($post->ID, 'thumb2', true);
                        $thumb3 = get_post_meta($post->ID, 'thumb3', true); 
                    ?>
            <div class="event-row ">
              <div class="eventleft-cont myrelease">
                <?php
                    $new_query = new WP_Query();
                    $new_query->query('post_type=acceledent_news&showposts=6'.'&paged='.$paged);
          ?>
                <div class="pressrealse-cont presscon" >
                  <h2>Press Releases</h2>
                  <?php while ($new_query->have_posts()) : $new_query->the_post(); ?>
                  <div class="press-postcont">
                    <p><a style="color:#0070B0" href="<?php the_permalink(); ?>">
                      <?php the_title(); ?>
                      </a></p>                     
                    <p>
                      <?php the_excerpt(); ?>
                    </p>
                  </div>
                  <?php endwhile; ?>
                </div>
                <div>
                  <p><a href="<?php echo get_post_type_archive_link( 'acceledent_news' ); ?>">Older Entries &raquo;</a></p>
                </div>
                <!-- end of #post-24 -->
                <?php if (  $wp_query->max_num_pages > 1 ) : ?>
                <div class="navigation">
                  <div class="previous">
                    <?php next_posts_link( __( '&#8249; Older posts', 'responsive' ) ); ?>
                  </div>
                  <div class="next">
                    <?php previous_posts_link( __( 'Newer posts &#8250;', 'responsive' ) ); ?>
                  </div>
                </div>
                <!-- end of .navigation -->
                <?php endif; ?>
              </div>
              <div class="video-cont myrelease-right">
                <div class="video-thumbcont"   >
				
				
				<div class="" id="widgets">
                    
            		<div class="widget-wrapper widget_archive" id="archives-2" style="padding-left:0px;">
					<div class="widget-title">Archives</div>		
					<ul style=" margin:0px;">
			<?php wp_get_archives('type=monthly'); ?>
		</ul>
</div>
                </div>
				 
                </div>
              </div>
            </div>
          </div>
          <div class="post-edit"></div>
        </div>
      </div>
      <!-- end of .post-entry -->
    </div>
    <!-- end of #content-full -->
  </div>
  <!-- end of .content-wrapper -->
</div>
<?php endwhile; ?>
<?php if (  $wp_query->max_num_pages > 1 ) : ?>
<div class="navigation">
  <div class="previous">
    <?php next_posts_link( __( '&#8249; Older posts', 'responsive' ) ); ?>
  </div>
  <div class="next">
    <?php previous_posts_link( __( 'Newer posts &#8250;', 'responsive' ) ); ?>
  </div>
</div>
<!-- end of .navigation -->
<?php endif; ?>
<?php else : ?>
<h1 class="title-404">
  <?php _e('404 &#8212; Fancy meeting you here!', 'responsive'); ?>
</h1>
<p>
  <?php _e('Don&#39;t panic, we&#39;ll get through this together. Let&#39;s explore our options here.', 'responsive'); ?>
</p>
<h6><?php printf( __('You can return %s or search for the page you were looking for.', 'responsive'),
	            sprintf( '<a href="%1$s" title="%2$s">%3$s</a>',
		            esc_url( get_home_url() ),
		            esc_attr__('Home', 'responsive'),
		            esc_attr__('&larr; Home', 'responsive')
	                )); 
			 ?></h6>
<?php get_search_form(); ?>
<?php endif; ?>
<!-- end of #wrapper -->
</div>
<?php get_footer(); ?>
