<a name="top"></a>
<div class="get-started locator">
			
			<div class="form-header">
			<h2>Doctor Locator</h2>
			
			<p class="hide-980">Please enter your city / state or zip code to contact a doctor in your area.</p>
			</div><!-- end of .form-header -->
			<div class="form-wrapper">


		<form name="searchform" id="searchform" action="" method="POST" class="find-doctor" >
			<input type="hidden" name="search_bounds" id="search_bounds" value="" />
			<input type="hidden" name="extra_bounds" id="extra_bounds" value="" />
			<input type="hidden" name="lat_lon" id="lat_lon" value="" />
			<div class="left-col">
			<div class="text-input"><label for="city">City</label><input type="text"  name="city" id="city" maxlength="50" tabindex="1" class="searchtextfield" /></div>
					<div class="text-input text_state"><label for="search_radius">State</label><select class="searchselectfield searchtextfield" name="state_province" id="state_province" tabindex="2">
							<option value=",US">-- Select --</option>
							<option value="AL,US">Alabama</option>
							<option value="AK,US">Alaska</option>
							<option value="AB,CA">Alberta</option>
							<option value="AZ,US">Arizona</option>
							<option value="AR,US">Arkansas</option>
							<option value="BC,CA">British Columbia</option>
							<option value="CA,US">California</option>
							<option value="CO,US">Colorado</option>
							<option value="CT,US">Connecticut</option>
							<option value="DE,US">Delaware</option>
							<option value="DC,US">District Of Columbia</option>
							<option value="FL,US">Florida</option>
							<option value="GA,US">Georgia</option>
							<option value="HI,US">Hawaii</option>
							<option value="ID,US">Idaho</option>
							<option value="IL,US">Illinois</option>
							<option value="IN,US">Indiana</option>
							<option value="IA,US">Iowa</option>
							<option value="KS,US">Kansas</option>
							<option value="KY,US">Kentucky</option>
							<option value="LA,US">Louisiana</option>
							<option value="ME,US">Maine</option>
							<option value="MB,CA">Manitoba</option>
							<option value="MD,US">Maryland</option>
							<option value="MA,US">Massachusetts</option>
							<option value="MI,US">Michigan</option>
							<option value="MN,US">Minnesota</option>
							<option value="MS,US">Mississippi</option>
							<option value="MO,US">Missouri</option>
							<option value="MT,US">Montana</option>
							<option value="NE,US">Nebraska</option>
							<option value="NV,US">Nevada</option>
							<option value="NB,CA">New Brunswick</option>
							<option value="NH,US">New Hampshire</option>
							<option value="NJ,US">New Jersey</option>
							<option value="NM,US">New Mexico</option>
							<option value="NY,US">New York</option>
							<option value="NF,CA">Newfoundland and Labrador</option>
							<option value="NC,US">North Carolina</option>
							<option value="ND,US">North Dakota</option>
							<option value="NT,CA">Northwest Territories</option>
							<option value="NS,CA">Nova Scotia</option>
							<option value="NU,CA">Nunavut</option>
							<option value="OH,US">Ohio</option>
							<option value="OK,US">Oklahoma</option>
							<option value="ON,CA">Ontario</option>
							<option value="OR,US">Oregon</option>
							<option value="PA,US">Pennsylvania</option>
							<option value="PE,CA">Prince Edward Island</option>
							<option value="PR,US">Puerto Rico</option>
							<option value="QC,CA">Québec</option>
							<option value="RI,US">Rhode Island</option>
							<option value="SK,CA">Saskatchewan</option>
							<option value="SC,US">South Carolina</option>
							<option value="SD,US">South Dakota</option>
							<option value="TN,US">Tennessee</option>
							<option value="TX,US">Texas</option>
							<option value="UT,US">Utah</option>
							<option value="VT,US">Vermont</option>
							<option value="VI,US">Virgin Islands</option>
							<option value="VA,US">Virginia</option>
							<option value="WA,US">Washington</option>
							<option value="WV,US">West Virginia</option>
							<option value="WI,US">Wisconsin</option>
							<option value="WY,US">Wyoming</option>
							<option value="YT,CA">Yukon</option>
						</select>
					</div>
					<span class="or">or</span>
					<div class="text-input">
					<label for="zip_code">Zip</label><input type="text" class="searchtextfieldsmall searchtextfield" name="zip_code" id="zip_code" maxlength="10" tabindex="3" />
					</div>
</div><div class="right-col"><div class="text-input text_radius"><label for="search_radius">Radius</label><select class="searchselectfield searchtextfield" name="search_radius" id="search_radius" tabindex="4">
				<option value="1.5">1 mile</option>
				<option value="5">5 miles</option>
				<option value="10">10 miles</option>
				<option value="25" selected>25 miles</option>
				<option value="50">50 miles</option>
				<option value="100">100 miles</option>
			</select></div>
					<div class="buttons">
					<a href="javascript:void(0);" id="locator" class="location" style="display:none;" onclick="setLocation();"><span class="arrow"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/get-my-location.svg" alt=""></span> Get my Location</a>
					<a href="javascript:void(0);" onclick="mmConversionTag(786161, this); return false;" id="searchbutton" class="red button searchbutton">Search <span class="arrow"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/red-arrow.svg" alt=""></span></a></div>
					</div>
		</form><!-- end of .left-col --></div> <!-- end of .form-wrapper -->
</div> <!-- end of .get-started -->
		<div class="resultsdiv" id="resultsdiv" style="display:none;">
			<div class="rt-wrapper">
                <div class="map-wrapper"><div id="map_canvas" style=""></div></div>
                <div class="clear"></div>
                <div id="key-wrapper">
                    <h2>Search Results Key</h2>
                    <div class="key">
                        <form name="filterform" id="filterform" action="" method="post" class="filter-providers" >
                        <div class="key-left">
                        <input type="checkbox" value="7" name="providertype7" id="providertype7" checked style="display: none;"> <img src="/wp-content/themes/acceledent/img/key-icon-7.png" alt="elite provider" style="display: none;"> <label for="providertype7" style="display: none;"><span style="display: none;">elite</span><br />
                                Elite Provider</label><br>
                        <input type="checkbox" value="6" name="providertype6" id="providertype6" checked> <img src="/wp-content/themes/acceledent/img/key-icon-6.png" alt="acceledent now provider"> <label for="providertype6"><span><img src="/wp-content/themes/acceledent/img/acceledent-now.png" alt="AcceleDent Now" class="adnow-locator"/></span><br />No-Risk, 60 Day Trial Offered</label><br>
                        <input type="checkbox" value="5" name="providertype5" id="providertype5" checked> <img src="/wp-content/themes/acceledent/img/key-icon-5.png" alt="diamond top provider"> <label for="providertype5"><span>diamond</span><br />
                                Top Provider</label><br>
                        <input type="checkbox" value="4" name="providertype4" id="providertype4" checked> <img src="/wp-content/themes/acceledent/img/key-icon-4.png" alt="platinum premier plus provider"> <label for="providertype4"><span>platinum</span><br />
                                Premier Plus Provider</label><br>
                        <input type="checkbox" value="3" name="providertype3" id="providertype3" checked> <img src="/wp-content/themes/acceledent/img/key-icon-3.png" alt="gold premier provider"> <label for="providertype3"><span>gold</span><br />
                                Premier Provider</label><br>
                        </div>
                        <div class="key-right">
                        <input type="checkbox" value="2" name="providertype2" id="providertype2" checked> <img src="/wp-content/themes/acceledent/img/key-icon-2.png" alt="silver preferred provider"> <label for="providertype2"><span>silver</span><br />
                                Preferred Provider</label><br>
                        <input type="checkbox" value="1" name="providertype1" id="providertype1" checked> <img src="/wp-content/themes/acceledent/img/key-icon-1.png" alt="provider"> <label for="providertype1">Provider</label><br>
                        </div>
                        
                        
                        
                        
                        <div class="clear"></div>
                        <div class=buttons"><a href="javascript:void(0);" id="filterbutton" class="red button searchbutton">UPDATE <span class="arrow"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/red-arrow.svg" alt=""></span></a></div>
						
						<div class="clear"></div>	
		                	
						<p class="provider-levels">
								<strong>Provider Levels</strong><br />
								<em>Our provider designations are based on the number of AcceleDent devices carried by the practice. Diamond Top Providers carry the greatest amount of devices, Platinum Premier Plus Providers carry the second-greatest amount of devices, and so on.</em>
						</p>

                        </form>
                    </div>
                </div>
                               
                
            </div>

            <div id="results_listing" style=""></div>
		</div><!-- end of #resultsdiv -->   