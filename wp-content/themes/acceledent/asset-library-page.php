<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Full Content Template
 *
   Template Name:  Asset Library Page (no sidebar)
 *
 * @file           asset-library-page.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2011 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/full-width-page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */
?>
<?php get_header(); ?>
<?php $options = get_option('responsive_theme_options'); ?>
		<?php if ($options['breadcrumb'] == 0): ?>
		<?php 
		echo responsive_breadcrumb_lists(); 
		?>
        <?php endif; ?>
       <?php
$my_post = get_post($post->ID); 
$my_title = $my_post->post_title;
$my_excerpt = $my_post->post_excerpt;
$my_link = get_permalink($post->ID);
?> 

        <div class="title-wrapper"><div class="title-inner"><h1 class="post-title"><?php echo $my_title; ?></h1></div></div>
        <div class="content-wrapper">

        <div id="content-full" class="grid col-940">
        
<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
        
        
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
 
                
                <div class="post-entry">
                    <?php the_content(__('SEARCH &#8250;', 'responsive')); ?>
                <br />
                <div id="widgets" class="home-widgets">
<div class="grid col-300 doctors-product">
            
            <div class="widget-wrapper">
            
                <div class="textwidget"><div class="product-wrapper"><div class="the-product">
                <h3><?php check_field('asset_logos_headline','LOGOS'); ?></h3>
                <?php check_field_image('asset_logos_image');
                 ?>
                <!--
    			<?php  if(function_exists('the_field') && get_field('asset_logos_image')) : 
    					the_field('asset_logos_image');
    			else : ?>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/img/acceledent-aura-asset.png" alt="The Product">
                <?php endif; ?>-->
                <div class="doctors-widget-copy"><p><?php check_field('asset_logos_widget','Download logos, corporate identity
manuals'); ?></p>
<div class="call-to-action"><a href="<?php check_field('asset_logos_cta_link','#nogo'); ?>" class="blue button">SEARCH <span class="arrow"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/blue-arrow.svg" alt=""></svg></span></a>  </div></div>

</div>
				
</div></div>
            
			</div><!-- end of .widget-wrapper -->

        </div><!-- end of .col-300 -->


<div class="grid col-300 doctors-product">
            
            <div class="widget-wrapper">
            
                <div class="textwidget"><div class="product-wrapper"><div class="the-product">
                <h3><?php check_field('asset_images_headline','IMAGES'); ?></h3>
                 <?php check_field_image('asset_images_image'); ?>
                <!--<img src="<?php bloginfo('stylesheet_directory'); ?>/img/acceledent-aura-asset2.jpg" alt="The Product">-->
                <div class="doctors-widget-copy"><p><?php check_field('asset_images_widget','Download High, Medium and low 
resolutions images'); ?></p>
<div class="call-to-action"><a href="<?php check_field('asset_images_cta_link','#nogo'); ?>" class="blue button">SEARCH <span class="arrow"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/blue-arrow.svg" alt=""></span></a>  </div>
                </div>
</div>
				
</div></div>
            
			</div><!-- end of .widget-wrapper -->

        </div><!-- end of .col-300 -->
        
<div class="grid col-300 doctors-product fit">
            
            <div class="widget-wrapper">
            
                <div class="textwidget"><div class="product-wrapper"><div class="the-product">
                <h3><?php check_field('asset_lit_headline','PRODUCT LITERATURE'); ?></h3>
                <?php check_field_image('asset_lit_image'); ?><!--<img src="<?php bloginfo('stylesheet_directory'); ?>/img/acceledent-aura-asset3.png" alt="The Product">-->
                <div class="doctors-widget-copy"><p><?php check_field('asset_lit_widget','Search for brochures & library literature'); ?></p>
<div class="call-to-action"><a href="<?php check_field('asset_lit_cta_link','#nogo'); ?>" class="blue button">SEARCH <span class="arrow"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/blue-arrow.svg" alt=""></span></a>  </div>
                </div>
</div>
				
</div></div>
            
			</div><!-- end of .widget-wrapper -->

        </div><!-- end of .col-300 -->


            
                    </div><!-- end of #post-<?php the_ID(); ?> -->
            
            		<div class="post-edit"><?php edit_post_link(__('Edit', 'responsive')); ?></div> 
        
                </div><!-- end of .post-entry -->
                
                            
        <?php endwhile; ?> 
        
        <?php if (  $wp_query->max_num_pages > 1 ) : ?>
        <div class="navigation">
			<div class="previous"><?php next_posts_link( __( '&#8249; Older posts', 'responsive' ) ); ?></div>
            <div class="next"><?php previous_posts_link( __( 'Newer posts &#8250;', 'responsive' ) ); ?></div>
		</div><!-- end of .navigation -->
        <?php endif; ?>

	    <?php else : ?>

        <h1 class="title-404"><?php _e('404 &#8212; Fancy meeting you here!', 'responsive'); ?></h1>
                    
        <p><?php _e('Don&#39;t panic, we&#39;ll get through this together. Let&#39;s explore our options here.', 'responsive'); ?></p>
                    
        <h6><?php printf( __('You can return %s or search for the page you were looking for.', 'responsive'),
	            sprintf( '<a href="%1$s" title="%2$s">%3$s</a>',
		            esc_url( get_home_url() ),
		            esc_attr__('Home', 'responsive'),
		            esc_attr__('&larr; Home', 'responsive')
	                )); 
			 ?></h6>
                    
        <?php get_search_form(); ?>

<?php endif; ?>  
      
        </div><!-- end of #content-full -->
        </div><!-- end of .content-wrapper -->

<?php get_footer(); ?>
