<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Sales Rep Locator Template
 *
   Template Name:  Sales Rep Locator (no sidebar)
 *
 * @file           sales-rep-locator.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2011 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/full-width-page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */
?>
<?php get_header(); ?>
<?php $options = get_option('responsive_theme_options'); ?>
		<?php if ($options['breadcrumb'] == 0): ?>
		<?php 
		echo responsive_breadcrumb_lists(); 
		?>
        <?php endif; ?>
       <?php
$my_post = get_post($post->ID); 
$my_title = $my_post->post_title;
$my_excerpt = $my_post->post_excerpt;
$my_link = get_permalink($post->ID);
?> 

        <div class="title-wrapper"><div class="title-inner"><h1 class="post-title"><?php echo $my_title; ?></h1></div></div>
        <div class="content-wrapper">

        <div id="content-full" class="grid col-940">
        
<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
        
        
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
             <div class="post-entry">
<?php the_content(__('Read more &#8250;', 'responsive')); ?>
 <div class="get-started locator">
			
			<div class="form-header">
			<h2>Sales Representative Locator</h2>
			<p>Please enter your city and state or ZIP Code to contact the AcceleDent Sales Representative in your area.</p>
			</div><!-- end of .form-header -->
			
			<div class="form-wrapper">
		<form name="searchform" id="searchform" action="" method="POST" class="find-doctor" >
			<input type="hidden" name="lat_lon" id="lat_lon" value="" />
			<input type="hidden" name="use_zip_code" id="use_zip_code" value="" />
			<div class="left-col"><div class="text-input"><label for="city">City</label><input type="text"  name="city" id="city" maxlength="50" tabindex="1" class="searchtextfield" /></div>
					<div class="text-input text_state"><label for="search_radius">State</label><select class="searchselectfield searchtextfield" name="state_province" id="state_province" tabindex="2">
							<option value=",">-- Select --</option>
							<option value="AL,US">Alabama</option>
							<option value="AK,US">Alaska</option>
							<option value="AB,CA">Alberta</option>
							<option value="AZ,US">Arizona</option>
							<option value="AR,US">Arkansas</option>
							<option value="BC,CA">British Columbia</option>
							<option value="CA,US">California</option>
							<option value="CO,US">Colorado</option>
							<option value="CT,US">Connecticut</option>
							<option value="DE,US">Delaware</option>
							<option value="DC,US">District Of Columbia</option>
							<option value="FL,US">Florida</option>
							<option value="GA,US">Georgia</option>
							<option value="HI,US">Hawaii</option>
							<option value="ID,US">Idaho</option>
							<option value="IL,US">Illinois</option>
							<option value="IN,US">Indiana</option>
							<option value="IA,US">Iowa</option>
							<option value="KS,US">Kansas</option>
							<option value="KY,US">Kentucky</option>
							<option value="LA,US">Louisiana</option>
							<option value="ME,US">Maine</option>
							<option value="MB,CA">Manitoba</option>
							<option value="MD,US">Maryland</option>
							<option value="MA,US">Massachusetts</option>
							<option value="MI,US">Michigan</option>
							<option value="MN,US">Minnesota</option>
							<option value="MS,US">Mississippi</option>
							<option value="MO,US">Missouri</option>
							<option value="MT,US">Montana</option>
							<option value="NE,US">Nebraska</option>
							<option value="NV,US">Nevada</option>
							<option value="NB,CA">New Brunswick</option>
							<option value="NH,US">New Hampshire</option>
							<option value="NJ,US">New Jersey</option>
							<option value="NM,US">New Mexico</option>
							<option value="NY,US">New York</option>
							<option value="NF,CA">Newfoundland and Labrador</option>
							<option value="NC,US">North Carolina</option>
							<option value="ND,US">North Dakota</option>
							<option value="NT,CA">Northwest Territories</option>
							<option value="NS,CA">Nova Scotia</option>
							<option value="NU,CA">Nunavut</option>
							<option value="OH,US">Ohio</option>
							<option value="OK,US">Oklahoma</option>
							<option value="ON,CA">Ontario</option>
							<option value="OR,US">Oregon</option>
							<option value="PA,US">Pennsylvania</option>
							<option value="PE,CA">Prince Edward Island</option>
							<option value="PR,US">Puerto Rico</option>
							<option value="QC,CA">Québec</option>
							<option value="RI,US">Rhode Island</option>
							<option value="SK,CA">Saskatchewan</option>
							<option value="SC,US">South Carolina</option>
							<option value="SD,US">South Dakota</option>
							<option value="TN,US">Tennessee</option>
							<option value="TX,US">Texas</option>
							<option value="UT,US">Utah</option>
							<option value="VT,US">Vermont</option>
							<option value="VI,US">Virgin Islands</option>
							<option value="VA,US">Virginia</option>
							<option value="WA,US">Washington</option>
							<option value="WV,US">West Virginia</option>
							<option value="WI,US">Wisconsin</option>
							<option value="WY,US">Wyoming</option>
							<option value="YT,CA">Yukon</option>
						</select>
					</div>
					<span class="or">or</span>
					<div class="text-input">
					<label for="zip_code">Zip</label><input type="text" class="searchtextfieldsmall searchtextfield" name="zip_code" id="zip_code" maxlength="10" tabindex="3" />
					</div>
					
		</div><!-- end of .left-col -->
										<div class="right-col"><div class="buttons">
					<a href="javascript:void(0);" id="locator" class="location" style="display:none;" onclick="setLocation();"><span class="arrow"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/get-my-location.svg" alt=""></span> Get my Location</a>
					<a href="javascript:void(0);" id="searchbutton" class="red button searchbutton">Search <span class="arrow"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/red-arrow.svg" alt=""></span></a></div><!-- end of .buttons -->
					</div><!-- end of .right-col -->

		</form></div> <!-- end of .form-wrapper --></
</div> <!-- end of .get-started --></div>
		<div class="resultsdiv" id="resultsdiv" style="display:none;">
                    <div id="results_listing"></div>
		</div><!-- end of #resultsdiv -->       
             <div class="post-edit"><?php edit_post_link(__('Edit', 'responsive')); ?></div> 
            </div><!-- end of #post-<?php the_ID(); ?> -->
 </div><!-- end of .post-entry -->

            
            <?php comments_template( '', true ); ?>
            
        <?php endwhile; ?> 
        
        <?php if (  $wp_query->max_num_pages > 1 ) : ?>
        <div class="navigation">
			<div class="previous"><?php next_posts_link( __( '&#8249; Older posts', 'responsive' ) ); ?></div>
            <div class="next"><?php previous_posts_link( __( 'Newer posts &#8250;', 'responsive' ) ); ?></div>
		</div><!-- end of .navigation -->
        <?php endif; ?>

	    <?php else : ?>

        <h1 class="title-404"><?php _e('404 &#8212; Fancy meeting you here!', 'responsive'); ?></h1>
                    
        <p><?php _e('Don&#39;t panic, we&#39;ll get through this together. Let&#39;s explore our options here.', 'responsive'); ?></p>
                    
        <h6><?php printf( __('You can return %s or search for the page you were looking for.', 'responsive'),
	            sprintf( '<a href="%1$s" title="%2$s">%3$s</a>',
		            esc_url( get_home_url() ),
		            esc_attr__('Home', 'responsive'),
		            esc_attr__('&larr; Home', 'responsive')
	                )); 
			 ?></h6>
                    
        <?php get_search_form(); ?>

<?php endif; ?>  
      
        </div><!-- end of #content-full -->
        </div><!-- end of .content-wrapper -->

<?php get_footer(); ?>