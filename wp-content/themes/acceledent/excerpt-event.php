<?php 
/******************************
**** Display Event Details ****
******************************/		
?> 

<div class="event-leftrow1">	
	<p class="event-date">
	
		<?php 
			$sdate = get_field('event_date_and_year', get_the_ID());
			$edate = get_field('event_end_date', get_the_ID());
		?>
		
		<?php echo date("m/d/Y", strtotime($sdate)); ?> 		
		<?php if($sdate != $edate) echo ' - ' . date("m/d/Y", strtotime($edate)); ?>
	</p>
	
	<h1> <a href="<?php the_field('event_link', get_the_ID()); ?>" target="_blank"><?php the_title(); ?></a></h1>
	<?php the_excerpt(); ?> 
	
	<?php if( get_field('event_featured_speaker') ): ?>
		<p>Speakers: <?php the_field('event_featured_speaker', get_the_ID()); ?></p>
	<? endif; ?>
	
</div>
<div class="eventlogo"> 
	<?php if (has_post_thumbnail( get_the_ID() ) ): ?>
		<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail' ); ?>
		<?php if( get_field('event_link') ): ?><a href="<?php the_field('event_link', get_the_ID()); ?>" target="_blank"><?php endif; ?>
			<img src="<?php echo $image[0]; ?>" width="121" height="37" alt="">
		<?php if( get_field('event_link') ): ?></a><?php endif; ?>
	<?php endif; ?>

	<?php if( get_field('event_image') ): ?>
		<?php if( get_field('event_link') ): ?><a href="<?php the_field('event_link', get_the_ID()); ?>" target="_blank"><?php endif; ?>
			<img src="<?php the_field('event_image'); ?>" alt="<?php the_title(); ?>" width="77" height="53"/>
		<?php if( get_field('event_link') ): ?></a><?php endif; ?>
	<?php endif; ?>

</div>
<div class="clear"></div>