<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Full Content Template
 *
   Template Name:  Case Studies
 *
 * @file           page-case-studies.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2011 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/full-width-page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */
?>
<?php
get_header();
$options = get_option('responsive_theme_options');
if ($options['breadcrumb'] == 0) {
    echo responsive_breadcrumb_lists();
}
$my_post = get_post($post->ID); 
$my_title = $my_post->post_title;
$my_excerpt = $my_post->post_excerpt;
$my_link = get_permalink($post->ID);



$category_id_cs = get_cat_ID('Case Study');


$args_cat = array(
	'type'                     => 'case_studies',
	'child_of'                 => 0,
	'parent'                   => $category_id_cs,
	'orderby'                  => 'name',
	'order'                    => 'ASC',
	'hide_empty'               => 1,
	'hierarchical'             => 1,
	'exclude'                  => '',
	'include'                  => '',
	'number'                   => '',
	'taxonomy'                 => 'category',
	'pad_counts'               => false 

); 




  



?> 




        <div class="title-wrapper"><div class="title-inner"><h1 class="post-title"><?php echo $my_title; ?></h1></div></div>
        <div class="content-wrapper">

        <div id="content-full" class="grid col-940">
			<div class="post-entry">
				<p class="cs_lead_text">Have a case you would like to share? <a href="https://www.jotform.com/abmazzi/case-studies-doctor-submission" target="_blank">Click Here.</a></p>
			</div>
        <?php
$categories_casestudy = get_categories( $args_cat ); 
  foreach ($categories_casestudy as $category) {
  
  	$case_studies = new WP_Query(array('post_type'=>'case_studies','category_name' => $category->cat_name));

?>

<div class="title-inner"><h1 class="post-title" style="color:#0070b0;"><?php echo $category->cat_name; ?></h1></div>


<?php if (have_posts()) : ?>

    <?php while (have_posts()) : the_post(); ?>
        
        
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
 
                <?php if ( comments_open() ) : ?>               
                <div class="post-meta">
                <?php responsive_post_meta_data(); ?>
                
            <?php if ( comments_open() ) : ?>
                        <span class="comments-link">
                        <span class="mdash">&mdash;</span>
                    <?php comments_popup_link(__('No Comments &darr;', 'responsive'), __('1 Comment &darr;', 'responsive'), __('% Comments &darr;', 'responsive')); ?>
                        </span>
                    <?php endif; ?> 
                </div><!-- end of .post-meta -->
                <?php endif; ?> 
                
                <div class="post-entry">
                    <?php the_content(__('Read more &#8250;', 'responsive')); ?>
                    <div class="pure-g">
                        <?php while ($case_studies->have_posts()) : $case_studies->the_post(); ?>
                            <div class="pure-u-1 pure-u-sm-1-2 pure-u-lg-1-3" style="padding: 1em; box-sizing: border-box;">
                                <a href="<?php echo get_field('case_report'); ?>" target="_blank">
                                  <img class="pure-img" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" alt="">
                                  <strong><?php the_title(); ?></strong>
                                </a>
                                <p>Projected Treatment Time: <?php echo get_field('time_projected'); ?><br>
                                <strong>Actual Treatment Time with AcceleDent: <?php echo get_field('time_actual'); ?> </strong></p>
                            </div>
                        <?php endwhile; wp_reset_postdata();  ?>
                    </div>
            
            <div class="post-edit"><?php edit_post_link(__('Edit', 'responsive')); ?></div> 
            </div><!-- end of #post-<?php the_ID(); ?> -->
            
            <?php comments_template( '', true ); ?>

                </div><!-- end of .post-entry -->
                
                            
        <?php endwhile; ?> 
        
        <?php if (  $wp_query->max_num_pages > 1 ) : ?>
        <div class="navigation">
      <div class="previous"><?php next_posts_link( __( '&#8249; Older posts', 'responsive' ) ); ?></div>
            <div class="next"><?php previous_posts_link( __( 'Newer posts &#8250;', 'responsive' ) ); ?></div>
    </div><!-- end of .navigation -->
        <?php endif; ?>

      <?php else : ?>

        <h1 class="title-404"><?php _e('404 &#8212; Fancy meeting you here!', 'responsive'); ?></h1>
                    
        <p><?php _e('Don&#39;t panic, we&#39;ll get through this together. Let&#39;s explore our options here.', 'responsive'); ?></p>
                    
        <h6><?php printf( __('You can return %s or search for the page you were looking for.', 'responsive'),
              sprintf( '<a href="%1$s" title="%2$s">%3$s</a>',
                esc_url( get_home_url() ),
                esc_attr__('Home', 'responsive'),
                esc_attr__('&larr; Home', 'responsive')
                  )); 
       ?></h6>
                    
        <?php get_search_form(); ?>

<?php endif; 
}
?>  
      
        </div><!-- end of #content-full -->
        </div><!-- end of .content-wrapper -->
        
       

<?php get_footer(); ?>