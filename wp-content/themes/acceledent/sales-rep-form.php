<div class="get-started"><div class="form-header"><h2>Get Started Today!</h2>
</div>
<div class="form-wrapper">
<form action="" class="find-doctor">
	<input type="hidden" name="lat_lon" id="lat_lon" value="" />
	<div class="text-input"><label for="city">City</label><input type="text"  name="city" id="city" maxlength="50" class="searchtextfield" /></div>
	<div class="text-input"><label for="state">State</label><select class="searchselectfield searchtextfield" name="state_province" id="state_province">
							<option value=",">-- Select --</option>
							<option value="AL,US">Alabama</option>
							<option value="AK,US">Alaska</option>
							<option value="AB,CA">Alberta</option>
							<option value="AZ,US">Arizona</option>
							<option value="AR,US">Arkansas</option>
							<option value="BC,CA">British Columbia</option>
							<option value="CA,US">California</option>
							<option value="CO,US">Colorado</option>
							<option value="CT,US">Connecticut</option>
							<option value="DE,US">Delaware</option>
							<option value="DC,US">District Of Columbia</option>
							<option value="FL,US">Florida</option>
							<option value="GA,US">Georgia</option>
							<option value="HI,US">Hawaii</option>
							<option value="ID,US">Idaho</option>
							<option value="IL,US">Illinois</option>
							<option value="IN,US">Indiana</option>
							<option value="IA,US">Iowa</option>
							<option value="KS,US">Kansas</option>
							<option value="KY,US">Kentucky</option>
							<option value="LA,US">Louisiana</option>
							<option value="ME,US">Maine</option>
							<option value="MB,CA">Manitoba</option>
							<option value="MD,US">Maryland</option>
							<option value="MA,US">Massachusetts</option>
							<option value="MI,US">Michigan</option>
							<option value="MN,US">Minnesota</option>
							<option value="MS,US">Mississippi</option>
							<option value="MO,US">Missouri</option>
							<option value="MT,US">Montana</option>
							<option value="NE,US">Nebraska</option>
							<option value="NV,US">Nevada</option>
							<option value="NB,CA">New Brunswick</option>
							<option value="NH,US">New Hampshire</option>
							<option value="NJ,US">New Jersey</option>
							<option value="NM,US">New Mexico</option>
							<option value="NY,US">New York</option>
							<option value="NF,CA">Newfoundland and Labrador</option>
							<option value="NC,US">North Carolina</option>
							<option value="ND,US">North Dakota</option>
							<option value="NT,CA">Northwest Territories</option>
							<option value="NS,CA">Nova Scotia</option>
							<option value="NU,CA">Nunavut</option>
							<option value="OH,US">Ohio</option>
							<option value="OK,US">Oklahoma</option>
							<option value="ON,CA">Ontario</option>
							<option value="OR,US">Oregon</option>
							<option value="PA,US">Pennsylvania</option>
							<option value="PE,CA">Prince Edward Island</option>
							<option value="PR,US">Puerto Rico</option>
							<option value="QC,CA">Québec</option>
							<option value="RI,US">Rhode Island</option>
							<option value="SK,CA">Saskatchewan</option>
							<option value="SC,US">South Carolina</option>
							<option value="SD,US">South Dakota</option>
							<option value="TN,US">Tennessee</option>
							<option value="TX,US">Texas</option>
							<option value="UT,US">Utah</option>
							<option value="VT,US">Vermont</option>
							<option value="VI,US">Virgin Islands</option>
							<option value="VA,US">Virginia</option>
							<option value="WA,US">Washington</option>
							<option value="WV,US">West Virginia</option>
							<option value="WI,US">Wisconsin</option>
							<option value="WY,US">Wyoming</option>
							<option value="YT,CA">Yukon</option>
						</select>
	</div> <span class="or">or</span>
	<div class="text-input"><label for="zip">Zip</label><input type="text" class="searchtextfieldsmall searchtextfield" name="zip_code" id="zip_code" maxlength="10" /></div>
	<div class="call-to-action"><a href="javascript:void(0);" onclick="mmConversionTag(746746, this); return false;"  target="_self "id="homesearchbutton" class="red button">Find a Sales Rep <span class="arrow"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/red-arrow.svg" alt=""></span>
</a>
</div>
</form></div>