<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Home Page
 *
   Template Name:  Home
 */
?>
<?php get_header(); ?>
<div id="featured" class="grid col-940" >
 <div style="max-width:1250px;margin:0 auto;">
       	<?php echo do_shortcode("[metaslider id=19017]"); ?>	
        </div>
        </div> 
        <!--<div id="featured" class="grid col-940"  >
 <div class="hero-wrapper">
        <?php // echo do_shortcode( '[responsive_slider]' ); ?>

        <?php //echo do_shortcode("[metaslider id=18174]"); ?>
        </div></div>  -->
               
<?php get_sidebar('home'); ?>
</div>
<?php get_footer(); ?>