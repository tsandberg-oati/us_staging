<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Full Content Template
 *
   Template Name:  Full Width Video Page (no sidebar)
 *
 * @file           full-width-page.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2011 ThemeID
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/full-width-page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */
?>
<?php get_header(); ?>
<?php $options = get_option('responsive_theme_options'); ?>
		<?php if ($options['breadcrumb'] == 0): ?>
		<?php 
		echo responsive_breadcrumb_lists(); 
		?>
        <?php endif; ?>
       <?php
$my_post = get_post($post->ID); 
$my_title = $my_post->post_title;
$my_excerpt = $my_post->post_excerpt;
$my_link = get_permalink($post->ID);
?> 

        <div class="title-wrapper"><div class="title-inner"><h1 class="post-title"><?php echo $my_title; ?></h1></div></div>
        <div class="content-wrapper">

        <div id="content-full" class="grid col-940">
        
<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
			
			<div id="post-16543" class="post-16543 page type-page status-publish hentry">
			<div class="post-entry">
			
			<div style="padding-bottom:15px;"> <?php the_content(__('Read more &#8250;', 'responsive')); ?></div>
			
	        <?php if (function_exists (g_ywfzvideo_show)) g_ywfzvideo_show(); ?>
    	    </div></div>
             <!-- end of .post-entry -->
                
                            
        <?php endwhile; ?> 
        
         
<?php endif; ?>  
      
        </div><!-- end of #content-full -->
        </div><!-- end of .content-wrapper -->

<?php get_footer(); ?>
