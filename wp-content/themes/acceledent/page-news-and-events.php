<?php
// Exit if accessed directly
if ( !defined('ABSPATH')) exit;
?>
<?php get_header(); ?>



<?php $options = get_option('responsive_theme_options'); ?>
		<?php if ($options['breadcrumb'] == 0): ?>
		<?php 
		echo responsive_breadcrumb_lists(); 
		?>
        <?php endif; ?>
        
         <?php
$my_post = get_post($post->ID); 
$my_title = $my_post->post_title;
$my_excerpt = $my_post->post_excerpt;
$my_link = get_permalink($post->ID);
?> 
        
        <div class="title-wrapper">
      <div class="title-inner">
        <h1 class="post-title"><?php echo $my_title; ?></h1>
      </div>
    </div>
    
    <div class="content-wrapper">
    <?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
      <div id="content-full" class="grid col-940">
        <div id="post-24" class="post-24 page type-page status-publish hentry">
          <div class="post-entry news-events">
            <div>
              <div id="main_content" class="news-events">

              		<?php the_content(); ?>
              		    		


<div class="event-row"> 

<div class="eventleft-cont"> 


<?php
/*---------------------------------------------------------------*/
/* EVENTS & EXHIBITS
/*---------------------------------------------------------------*/
?>

<?php 
		$args = array(
			'post_type'      => 'events',
			'meta_key' 		 => 'event_date_and_year',
			'meta_value' 	 =>  date('Y-m-d'),   // comment out to list all dates
			'meta_compare'   => '>',
			'order'          => 'ASC',
			'orderby'        => 'meta_value',
			'meta_key'       => 'event_date_and_year',
			'posts_per_page' => '2',
			'tax_query' 	 => array(
						            array(
							            'taxonomy' => 'events_cat',
							            'field' => 'id',
							            'terms' => 17,
							            'operator' => 'NOT IN',
						            )
	        )
		);
		query_posts( $args );
?> 
<?php if ( have_posts() ) :  ?>

	<div class="eventleft-heading fl">Events &amp; Conferences</div>
	<a href="/orthodontists/news-and-events/events-conferences/" class="button-block fr">View All Events &amp; Conferences</a>
	<hr class="grey" />

<?php  while ( have_posts() ) : the_post(); ?>        

	<div class="event-left"> 
		<?php get_template_part( 'excerpt-event' ); ?>
	</div>

<?php endwhile; endif; wp_reset_query();?>                        

<div class="clearfix"></div>
<p>&nbsp;</p>
 
<?php
/*---------------------------------------------------------------*/
/* VIDEOS
/*---------------------------------------------------------------*/
?> 
 
<?php if (function_exists (g_ywfzvideo_show)): ?>

	<div class="eventleft-heading fl">Videos</div>
	<a href="/orthodontists/news-and-events/videos/" class="button-block fr">View All Videos</a>
	<hr class="grey" />

	<?php g_ywfzvideo_show($num_videos=3); ?>
	
	<div class="clearfix"></div>
 
 <?php endif; ?>
 
 
<?php
/*---------------------------------------------------------------*/
/* PRESS ANNOUNCEMENTS
/*---------------------------------------------------------------*/
?> 

<?php 
	$args = array(
		'post_type'      => 'acceledent_news',
		'order'          => 'DESC',
		'posts_per_page' => '2'
	);
	query_posts( $args );
?> 
	
<?php if ( have_posts() ) : ?>

    <div class="eventleft-heading fl">Press Announcements</div>
	<a href="/orthodontists/news-and-events/press/" class="button-block fr">View All Press Announcements</a>   
	<hr class="grey" />
	 
<?php while ( have_posts() ) : the_post(); ?>                

		<?php get_template_part( 'excerpt-press' ); ?>
	
<?php endwhile; endif; wp_reset_query(); ?>

<div class="clearfix"></div>
<p>&nbsp;</p>

<?php
/*---------------------------------------------------------------*/
/* SPEAKING ENGAGEMENTS
/*---------------------------------------------------------------*/
?> 

<?php 
	$args = array(
		'post_type'      => 'events',
		'meta_key' 		 => 'event_date_and_year',
		'meta_value' 	 =>  date('Y-m-d'),
		'meta_compare'   => '>',
		'order'          => 'ASC',
		'orderby'        => 'meta_value',
		'meta_key'       => 'event_date_and_year',
		'posts_per_page' => '2',
		'tax_query' 	 => array(
            array(
	            'taxonomy' => 'events_cat',
	            'field' => 'id',
	            'terms' => 17,
	            'operator' => 'IN',
            )
        )
	);
	query_posts( $args );
?> 
	
<?php if ( have_posts() ) : ?>

    <div class="eventleft-heading fl">Speaking Engagements</div>
	<a href="/orthodontists/news-and-events/kol-speaking-engagements/" class="button-block fr">View All Speaking Engagements</a>
	<hr class="grey" />   
	 
<?php while ( have_posts() ) : the_post(); ?>      
          
	<div class="event-left"> 
		<?php get_template_part( 'excerpt-event' ); ?>
	</div>
	
<?php endwhile; endif; wp_reset_query();?>

<div class="clearfix"></div>
<p>&nbsp;</p>

<?php
/*---------------------------------------------------------------*/
/* WEBINARS
/*---------------------------------------------------------------*/
?> 

<?php 
		$args = array(
			'post_type'      => 'acceledent_webinars',
			'meta_key' 		 => 'webinar_start_date',
			'meta_value' 	 =>  date('Y-m-d'),   // comment out to list all dates
			'meta_compare'   => '>',
			'order'          => 'ASC',
			'orderby'        => 'meta_value',
			'meta_key'       => 'webinar_start_date',
			'posts_per_page' => '2'
		);
		query_posts( $args ); 
?>
				
<?php if ( have_posts() ) : ?>

	<div class="eventleft-heading fl">Webinars</div>
	<a href="/orthodontists/news-and-events/webinars/" class="button-block fr">View All Webinars</a> 
	<hr class="grey" />   

<?php  while ( have_posts() ) : the_post();  ?>         
         
	<div class="event-left"> 
		<?php get_template_part( 'excerpt-webinar' ); ?>
	</div>
	
<?php endwhile; endif; wp_reset_query();?>

              </div> <!-- .eventleft-cont -->
			  </div> <!-- .event-row -->
              
          </div><!-- .main_content -->
			  
			  <?php get_sidebar( 'news-events' ); ?>
          
          
        </div>
        <!-- end of .post-entry --> 
        
      </div>
      <!-- end of #content-full --> 
    </div>
    <!-- end of .content-wrapper --> 
    
  </div>
  <?php endwhile; ?> 
        
        <?php if (  $wp_query->max_num_pages > 1 ) : ?>
        <div class="navigation">
			<div class="previous"><?php next_posts_link( __( '&#8249; Older posts', 'responsive' ) ); ?></div>
            <div class="next"><?php previous_posts_link( __( 'Newer posts &#8250;', 'responsive' ) ); ?></div>
		</div><!-- end of .navigation -->
        <?php endif; ?>
  <?php else : ?>

        <h1 class="title-404"><?php _e('404 &#8212; Fancy meeting you here!', 'responsive'); ?></h1>
                    
        <p><?php _e('Don&#39;t panic, we&#39;ll get through this together. Let&#39;s explore our options here.', 'responsive'); ?></p>
                    
        <h6><?php printf( __('You can return %s or search for the page you were looking for.', 'responsive'),
	            sprintf( '<a href="%1$s" title="%2$s">%3$s</a>',
		            esc_url( get_home_url() ),
		            esc_attr__('Home', 'responsive'),
		            esc_attr__('&larr; Home', 'responsive')
	                )); 
			 ?></h6>
                    
        <?php get_search_form(); ?>

<?php endif; ?> 
  <!-- end of #wrapper --> 
</div>
       
        
        
        
<?php get_footer(); ?>
