<?php /** /orthodontists/news-and-events/videos/ **/ ?>
<?php
	// Exit if accessed directly
	if ( !defined('ABSPATH')) exit;
?>

<?php get_header(); ?>
<?php $options = get_option('responsive_theme_options'); ?>
		<?php if ($options['breadcrumb'] == 0): ?>
		<?php 
		echo responsive_breadcrumb_lists(); 
		?>
        <?php endif; ?>
       <?php
$my_post = get_post($post->ID); 
$my_title = $my_post->post_title;
$my_excerpt = $my_post->post_excerpt;
$my_link = get_permalink($post->ID);
?> 

		
        <div class="title-wrapper"><div class="title-inner"><h1 class="post-title"><?php echo $my_title; ?></h1></div></div>
        <div class="content-wrapper">

        <div id="content-full" class="grid col-940">
        
        	<div class="post-entry news-events">
	        <div id="main_content" class="news-events">
	        
				<?php if (have_posts()) : ?>				
					<?php while (have_posts()) : the_post(); ?>						
						
				        <?php if (function_exists (g_ywfzvideo_show)) g_ywfzvideo_show(); ?>			                
				                            
				    <?php endwhile; ?>			         
				<?php endif; ?>  
	        </div><!-- .main_content -->
        	
	        
	        <?php get_sidebar( 'news-events' ); ?>
	        
	        </div>
      
        </div><!-- end of #content-full -->
        </div><!-- end of .content-wrapper -->

<?php get_footer(); ?>
