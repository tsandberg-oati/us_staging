<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Header Template
 *
 *
 * @file           header.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.3
 * @filesource     wp-content/themes/responsive/header.php
 * @link           http://codex.wordpress.org/Theme_Development#Document_Head_.28header.php.29
 * @since          available since Release 1.0
 */
?>
<!doctype html>
<!--[if !IE]>      <html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
<!--[if lt IE 9 ]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>

<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">

<title>
<?php wp_title(''); ?>
</title>

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php wp_enqueue_style('responsive-style', get_stylesheet_uri(), false, '1.8.9', 'all');?>
<?php wp_enqueue_style('fonts-com', 'http://fast.fonts.com/cssapi/fbcb3429-727f-4978-95ba-0392e53b0329.css', false, 'all');?>
<?php wp_enqueue_style('font-awesome', 'http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css', '4.1.0', 'all');?>

<!--[if IE ]>
<link href="<?php echo get_stylesheet_directory_uri() ?>/ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if (lt IE 9) & (!IEMobile)]>
<link href="<?php echo get_stylesheet_directory_uri() ?>/ie8.css" rel="stylesheet" type="text/css" />
<![endif]-->

<?php wp_head(); ?>

<script type="text/javascript">
	$(function(){
	$('.find-doctor select').customSelect();
	});
</script>
			
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_stylesheet_directory_uri() ?>/img/favicon.ico">
	<?php if (is_page('67')) : ?>
		<link href="<?php echo get_stylesheet_directory_uri() ?>/stylesheets/events.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/js/amazingslider.js"></script>
		<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/js/initslider.js"></script>
	<?php endif; ?>
	
	<style>
	
		#menu-top-menu { padding-bottom: 33px !important; padding-right: 30px !important;   }
		#menu-top-menu li { float: left; }
		#menu-top-menu li a { margin: 0; padding: 0; width: 128px; height: 27px; font-size: 14px; line-height: 30px; color: #999; background: none; border: 1px solid #ccc; display: block; }
		#menu-top-menu li a:hover { color: #fff !important; background: #05b9e4; border-color: #05b9e4; }	
		#menu-top-menu li.first a { color: #fff !important; background: #05b9e4; border-color: #05b9e4; border-right: none; }
		#menu-top-menu li.last a { border-left: none; }
		
		
		.doctors-pages #menu-top-menu #menu-top-menu li.first a { color: #999; background: none; border-color: #ccc; }
		.doctors-pages #menu-top-menu #menu-top-menu li.last a { color: #fff !important; background: #05b9e4; border-color: #05b9e4; }
		
		
		#location span.customSelect { padding: 0 10px 0 10px; line-height: 30px; }
	
	</style>
	
</head>

<body <?php body_class(); ?>>
   
<?php responsive_container(); // before container hook ?>
<div id="container" class="hfeed">
         
    <?php responsive_header(); // before header hook ?>
    <div id="header">
            
    <?php responsive_in_header(); // header hook ?>
   
	<?php if ( get_header_image() != '' ) : ?>
               
        <div id="logo">
            <a href="<?php echo home_url('/'); ?>"><img src="<?php header_image(); ?>" width="<?php if(function_exists('get_custom_header')) { echo get_custom_header() -> width;} else { echo HEADER_IMAGE_WIDTH;} ?>" height="<?php if(function_exists('get_custom_header')) { echo get_custom_header() -> height;} else { echo HEADER_IMAGE_HEIGHT;} ?>" alt="<?php bloginfo('name'); ?>" /></a>
        </div><!-- end of #logo -->
        
    <?php endif; // header image was removed ?>

    <?php if ( !get_header_image() ) : ?>
                
        <div id="logo">
            <span class="site-name"><a href="<?php echo home_url('/'); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home"><?php bloginfo('name'); ?></a></span>
            <span class="site-description"><?php bloginfo('description'); ?></span>
        </div><!-- end of #logo -->  

    <?php endif; // header image was removed (again) ?>
    <a href="#" class="menu-button hide-768"><svg version="1.1" id="menu-button" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="50px" height="49px" viewBox="0 0 50 49" enable-background="new 0 0 50 49" xml:space="preserve">
<path fill="#ffffff" stroke="#0071B0" stroke-miterlimit="10" d="M3.22,1.085h43.055c1.042,0,1.885,0.844,1.885,1.886v43.212
	c0,1.041-0.844,1.885-1.885,1.885H3.22c-1.042,0-1.886-0.844-1.886-1.885V2.971C1.334,1.93,2.178,1.085,3.22,1.085z"/>
<path fill-rule="evenodd" clip-rule="evenodd" fill="#0071B0" d="M9.214,25.577v-1.562h31.781v1.562H9.214z"/>
<path fill-rule="evenodd" clip-rule="evenodd" fill="#0071B0" d="M9.214,15.577v-1.562h31.781v1.562H9.214z"/>
<path fill-rule="evenodd" clip-rule="evenodd" fill="#0071B0" d="M9.214,35.577v-1.562h31.781v1.562H9.214z"/>
</svg></a>
    <?php get_sidebar('top'); ?>
    <?php get_top_parent_page_id(); 
	    $parent_page = get_top_parent_page_id($post->ID);
    ?>
    			<?php if(($parent_page != '75') && !is_page('75')) : ?>
			    <div class="drawer"><nav id="nav">
				<?php wp_nav_menu(array(
				    'container'       => '',
					'theme_location'  => 'header-menu',
					'walker'          => new My_Walker_Nav_Menu(),
					'menu_class' => 'nav nav-tabs nav-stacked')
					); 
				?>
				</nav>
			    </div>
				<?php else : ?>
				<div class="drawer doctors-drawer"><nav id="nav">
					<?php if (has_nav_menu('sub-header-menu', 'responsive')) { ?>
	            <?php wp_nav_menu(array(
				    'container'       => '',
					'menu_class' => 'nav nav-tabs nav-stacked',
					'theme_location'  => 'sub-header-menu')
					); 
				?>
					<?php } ?>
			    </nav>
			    </div>
			    <?php endif; ?>
            
 <div class="menu-form">
        <?php if (has_nav_menu('top-menu', 'responsive')) { ?>
	        <?php wp_nav_menu(array(
				    'container'       => '',
					'fallback_cb'	  =>  false,
					'menu_class'      => 'top-menu',
					'theme_location'  => 'top-menu')
					); 
				?>
        <?php } ?>
 <form id="location" action="" class="location">
	               
       <div class="select-box"><select name="country" form="location" class="styled" onChange="document.location.href=this.value"><option value="http://acceledent.com/" selected="selected">United States &nbsp;</option><option value="http://acceledent.co.uk/" >International</option></select></div>
        </form>
        <script type="text/javascript">
			$('select.styled').customSelect();
		</script>

        <div class="social show-980 hide-320">
						<ul class="social-icons">
							<li ><a href="https://www.facebook.com/pages/AcceleDent/110270362431502" class="social-icons-facebook" target="_blank"></a></li>
							<li ><a href="https://twitter.com/acceledent" class="social-icons-twitter" target="_blank"></a></li>
							<li ><a href="https://www.linkedin.com/company/orthoaccel-technologies-inc." class="social-icons-linkedin" target="_blank"></a></li>	
							<li><a href="http://www.youtube.com/user/AcceleDentSystem" class="social-icons-youtube" target="_blank"></a></li>
							<li><a href="<?php echo get_bloginfo('url'); ?>/blog/" class="social-icons-blog"></a>&nbsp;</li>
						</ul>
					</div>

            	        </div><!-- end of .menu-form -->
    </div><!-- end of #header -->
    <?php responsive_header_end(); // after header hook ?>
    
	<?php responsive_wrapper(); // before wrapper ?>
    <div id="wrapper" class="clearfix">
    <?php responsive_in_wrapper(); // wrapper hook ?>