	var usa = (document.location.hostname.indexOf(".co.uk") > 0 ? false : true);
	var gmap;
	var defaultbounds;
	var search_ctr;
	var search_bounds;
	var markerarray = new Array();

	$(document).ready(function() {

		console.log("==============================");
		console.log("DEV 8/29/2016...");

		infowindow = new InfoBox({
			alignBottom: false,
			boxClass: "locator-infobox",
			boxStyle: {},
			closeBoxMargin: "0px 7px 0px 0px",
			closeBoxURL: "/wp-content/themes/acceledent/img/map_close.png",
			content: '...',
			disableAutoPan: false,
			enableEventPropagation: false,
			infoBoxClearance: new google.maps.Size(0, 0),
			isHidden: false,
			maxWidth: 0,
			pane: "floatPane",
			pixelOffset: new google.maps.Size(7, -50),
			zIndex: null
		});
		
		
		$("#searchbutton").on("click",function() {
			getResults();
			
        });//end searchbutton click
        
      
      
        

	});//end doc ready
	
	
		$(window).on("load",function(){
			if (typeof google === 'object' && typeof google.maps === 'object') {
				
				 if(typeof gmap === 'undefined'){
				 
				 	
				 }else{
				  	
				  	getResults();		
				 
				 }
				 	
			}
	
			
	});	
	
	
	// New function 8/10/2016
	function getResults(){
		//Dev:
			//console.log("Search Radius: " + $("#search_radius").val());
			// Dev:
			var thisMapRadius = $("#search_radius").val();

			if (($("#city").val() != "" && $("#state_province").val() != "," && $("#state_province").val() != ",US") || ($("#zip_code").val() != "") || ($("#lat_lon").val() != "")) { // valid form entry combinations
				$('#resultsdiv').show();
				//if (!gmap) { createMap(); return false; }
				// 8/29/2016 removed return false 
				if (!gmap) { createMap(); }
				infowindow.close();
				var search_addr = $.trim($("#lat_lon").val() != "" ? $("#lat_lon").val() : $("#city").val() + " " + (usa ? $("#state_province").val().split(",")[0] + " " : "") + $("#zip_code").val() + " " + $("#state_province").val().split(",")[1]);
				geo.geocode( { 'address': search_addr, region: $("#state_province").val().split(",")[1] }, function(results, status) {

					if (status == google.maps.GeocoderStatus.OK) {
						if (search_ctr)
							search_ctr.setMap(null);
						gmap.setCenter(results[0].geometry.location);
						search_ctr = new google.maps.Marker({ map: gmap, position: results[0].geometry.location, visible: false });
						var circ = new google.maps.Circle({ center: results[0].geometry.location, radius: Math.floor($("#search_radius").val() * (usa ? 1800 : 1100)) });//, map:gmap });
					
						//var radius = parseInt($("#search_radius").val(), 10)*1300;
						//var circ = new google.maps.Circle({ center: results[0].geometry.location, radius: Math.round($("#search_radius").val() * (usa ? 1700 : 1100)) });//, map:gmap });


						search_bounds = circ.getBounds();
						//Dev:
						//console.log("Search bounds:  " + circ.getBounds());


						gmap.fitBounds(search_bounds);
						if ($("#search_radius").val() >= 25){
							gmap.setZoom(gmap.getZoom() + 1);
						}

							//gmap.setZoom(gmap.getZoom());
						$("#search_bounds").val(search_bounds.toString());
                        $("#extra_bounds").val(gmap.getBounds().toString());

						//Dev:
						//console.log("Search bounds:  " + circ.getBounds());
						//console.log("Extra bounds: " + gmap.getBounds());


                        //

						$.ajax({
							type: "POST",
							url: "/search/doctor_search.php",
							data: $('#searchform').serialize(),
							error: function(msg) {
								alert(msg);//$("#errors").html(msg);
							},
							complete: function (xhr, stat) {
								results = xhr.responseText.toString();
								if (results.substring(0, 5) == "ERROR") {
									alert("There was a search error: " + results.substring(7));
								} else {
									//alert(results);
									results = $.parseJSON(results);

									//Dev:
									//console.log(results);

									for (var i = 0; i < markerarray.length; i++) {
										markerarray[i].setMap(null);
									}
									markerarray = new Array();
									var markerbounds = new google.maps.LatLngBounds();
									markerbounds.extend(search_ctr.getPosition());
									var found_in_circle = false;
									var results_html = "";
									for (var row in results) {
										data = results[row];

										//Dev: Add condition for radius
										//console.log(data["distance"]);

                                        data["display_providertype"] = data["provider_type"];
										data["display_practice_name"] = $.trim(data["practice_name"] != "" ? data["practice_name"] : data["first_name"] + ' ' + data["last_name"] + ' ' + data["degree"]);
										//data["display_name"] = $.trim(data["first_name"] + ' ' + data["last_name"] + ' ' + data["degree"]);
										data["display_address"] = data["address"] + '<br />' + data["city"] + ', ' + (usa ? data["state_province"] + ' ' + data["zip_code"] : data["postal_code"] + ' ' + data["country"]) + (data["phone"] == '' ? '' : '<br />' + data["phone"]) + '<br />';
										data["display_website"] = (data["website"] == "" ? "" : (data["website"].substring(0, 4) == "http" ? data["website"] : "http://" + data["website"]));
										data["display_link"] = (data["display_website"] == '' ? '' : '<a href="' + data["display_website"] + '" onClick="recordOutboundLink(this, \'Outbound Link\', \'' + data["display_name"] + ' - ' + data["display_website"] + '\');return false;" target="_blank">Website</a>'); data["display_directions"] = '<a href="http://maps.google.com/maps?saddr=' + search_addr + '&daddr=' + data["address"] + ', ' + data["city"] + ', ' + (usa ? data["state_province"] + ', ' + data["zip_code"] : data["postal_code"] + ', ' + data["country"]) + '&hl=en&gl=us&mra=ls&t=m" target="_blank">Directions</a>';
										var in_circle = data["in_circle"];
										found_in_circle = found_in_circle | in_circle;

                                        if (in_circle | ! found_in_circle) {


												//Dev: Add condition to display results if within radius
												//Dev: convert distance string to number for comparison
												if(Number(data["distance"]) <= thisMapRadius){
		                                            var gmarker = new google.maps.Marker({
														position: new google.maps.LatLng(data["latitude"], data["longitude"]),
														map: gmap,
														title: data["display_practice_name"],
														animation: google.maps.Animation.DROP,
														icon: "/wp-content/themes/acceledent/img/map-icon-" + data["provider_type"] + ".png",
		                                                zIndex: data["provider_type"] * 100,
		                                                markerClassName: data["provider_type"],
														shadow: { url: "/wp-content/themes/acceledent/img/map_shadow.png", anchor: new google.maps.Point(20, 22) },
														html: '<div class="ui-infobox-content"><h3 id="practice_name_span">'+data["display_practice_name"]+'</h3>'+data["display_address"]+'<div style="float:right;">'+data["display_link"]+'</div>'+'<div class="results_directions">'+data["display_directions"]+' ('+(new Number(data["distance"])).toFixed(1)+' '+(usa?'mi':'km')+')</div></div>'
													});
		                                            //console.log(gmarker);

													markerbounds.extend(gmarker.getPosition());
													markerarray.push(gmarker);
													google.maps.event.addListener(gmarker, 'click', function() {
														displayMarkerInfo(this);
													});
		                                            //alert('showing marker');

														var couponhtml;
														
														var todaysdate = new Date();
														var coupondate = new Date(data["coupon"]);
														if(coupondate >= todaysdate){
																var couponURL = "http://acceledent.com/form/dc-1606.php?couponID=" + data["couponlinkID"];
																couponhtml = '<a href=\"'+ couponURL +'\" target=\"_blank\"  class=\"couponLink\">' + data['linktext'] + '</a>';
														}else{
																couponhtml = "";
														}



														results_html += '<li class="type-' + data["provider_type"] + '">\n\t<div class="bottom">';
														results_html += '<div class="results_specialty">'+data["specialty"]+'<br />'+data["display_link"]+'<br />'+data["display_directions"]+'<br />'+(new Number(data["distance"])).toFixed(1)+' '+(usa?'miles':'kilometers')+'</div>';
														results_html += '\t<h3><a href="javascript:void(0);" onclick="displayMarkerInfo(false, '+(markerarray.length-1)+');">'+data["display_practice_name"]+'</a></h3><div class="results_address">'+data["display_address"]+ couponhtml +'</div>\n</div></li>\n';
			                                           // alert('added to results list');
                                           		}//end if
										}


									}

									$('html, body').animate({ scrollTop: $("#results_listing").offset().top - 280 });
									if (markerarray.length > 0) {
										$('#results_listing').html('<h2>Search Results</h2>\n<ul>\n' + results_html + '</ul>\n <a href="#top" class="top">Back to Top</a>');
										setTimeout(function() {
											gmap.fitBounds(markerbounds);
											if (gmap.getZoom() >= 20)
												gmap.setZoom(19);
										}, 100);
                                        setTimeout(showWhat(), 600);
									} else {
										$('#results_listing').html('<h2>No Search Results</h2>\n');
									}

								}
							}

						});

					} else {
						alert("Search was not successful for the following reason: " + status);
					}
				});

			} else {
				alert("Please enter at least one of the following search parameters:\n\n    - "+(usa ? "City and State\n    - Zip Code" :  "City and Country\n    - Postal Code"));
			}
	
	}
	// End
	

	function createMap() {
    		gmap = new google.maps.Map(document.getElementById('map_canvas'),
                {   zoom: 3,
                    center: new google.maps.LatLng(37.09024,-95.712891),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    zoomControl: true,
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.SMALL
                    },
                    mapTypeControl: false,
                    streetViewControl: false

                });
    		defaultbounds = gmap.getBounds();
    		
    		//setTimeout('$("#searchbutton").trigger("click");', 500);
    		// 8-10-2016
    		//getResults();

	}


//setTimeout('$("#searchbutton").trigger("click");', 500);


	function displayMarkerInfo(marker, index) {
		marker = (marker ? marker : markerarray[index]);
		infowindow.setContent(marker.html);
		infowindow.open(gmap, marker);
		setTimeout("checkTextFit('#practice_name_span', 17, true)", 100);
		setTimeout("checkTextFit('#display_name_span', 30, false)", 100);
		marker.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
	}

	function checkTextFit(span, height, keepbold) {
		var ns = $(span);
		if (ns[0].scrollHeight > height) {
			var fs = parseInt(ns.css('fontSize'));
			ns.css('fontSize', (fs - 1) + 'px');
			if (!keepbold) ns.css('fontWeight', 'normal');
			checkTextFit(span, height, keepbold);
		}
	}

	function recordOutboundLink(link, category, action) {
		_gat._getTrackerByName()._trackEvent(category, action);
		setTimeout('window.open("'+link.href+'","_blank");', 100);
	}

    $(document).ready(function() {
        $("#filterbutton").click(function () {
            showWhat();
        });
    });

    function showWhat(){
        //initialize all to be visible
        for ( var mark in markerarray) {
            markerarray[mark].setVisible(true);
        }
        $('.type-7').show();
		$('.type-6').show();
        $('.type-5').show();
        $('.type-4').show();
        $('.type-3').show();
        $('.type-2').show();
        $('.type-1').show();

        
		if( !$('#providertype7').is(':checked') ){
            $('.type-7').hide();
            for ( var mark in markerarray) {
                if (markerarray[mark].markerClassName == "7") { markerarray[mark].setVisible(false); }
            }
        }if( !$('#providertype6').is(':checked') ){
            // alert('6 is not CHECKED');
            $('.type-6').hide();
            for ( var mark in markerarray) {
                if (markerarray[mark].markerClassName == "6") { markerarray[mark].setVisible(false); }
            }
        }
        if( !$('#providertype5').is(':checked') ){
            $('.type-5').hide();
            for ( var mark in markerarray) {
                if (markerarray[mark].markerClassName == "5") { markerarray[mark].setVisible(false); }
            }
        }
        if( !$('#providertype4').is(':checked') ){
            $('.type-4').hide();
            for ( var mark in markerarray) {
                if (markerarray[mark].markerClassName == "4") { markerarray[mark].setVisible(false); }
            }
        }
        if( !$('#providertype3').is(':checked') ){
            $('.type-3').hide();
            for ( var mark in markerarray) {
                if (markerarray[mark].markerClassName == "3") { markerarray[mark].setVisible(false); }
            }
        }
        if( !$('#providertype2').is(':checked') ){
            $('.type-2').hide();
            for ( var mark in markerarray) {
                if (markerarray[mark].markerClassName == "2") { markerarray[mark].setVisible(false); }
            }
        }
        if( !$('#providertype1').is(':checked') ){
            //alert('1 is not CHECKED');
            $('.type-1').hide();
            for ( var mark in markerarray) {
                if (markerarray[mark].markerClassName == "1") { markerarray[mark].setVisible(false); }
            }
        }

    }
