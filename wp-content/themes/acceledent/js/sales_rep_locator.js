var sales_rep_name;
var sales_rep_email;
var loading = true;

	$(document).ready(function() {

		//Dev 6/1/2016
		console.log("Dev 6/1/2016");

		$("#searchbutton").click(function() {
			$("#use_zip_code").val("");
			if ($("#zip_code").val() != "") {
				$("#use_zip_code").val($("#zip_code").val());
				doSearch();
			} else if (($("#city").val() != "" && $("#state_province").val() != ",") || $("#lat_lon").val() != "") {
				// var search_addr = $.trim($("#lat_lon").val() + " " + ($("#lat_lon").val() == "" ? "St, " : "") + $("#city").val() + " " + $("#state_province").val().split(",")[0] + " " + $("#state_province").val().split(",")[1]);
				//Tu: Create var search_addr to hold the lat and lng for geo reverse lookup to get the postal_code
				var search_addr;

				//Tu: Get the response from the geocode api with city and state_province
				//Tu: Lat and lng will be a string stored in search_addr
				//Tu: Once response is successful then go reverse geocode to get postal_code
				$.ajax({
                	type: "GET",
                	url: "https://maps.googleapis.com/maps/api/geocode/json?address=" + $("#city").val() + "," + $("#state_province").val().split(",")[0],
                	dataType: "json",
                	error: function (response) {
                        alert('Error: There was a problem processing your request, please refresh the browser and try again');
                	},
                	success: function (response) {
						var lookUpData = response;
						search_addr = String(lookUpData.results[0].geometry.location.lat + "," + lookUpData.results[0].geometry.location.lng);
						doGeoReverse();
               		}
    		   });//end ajax

				console.log("SEARCH ADDR ===============");
				console.log(search_addr);

				// Tu: Called when response is successful
				function doGeoReverse(){
					geo.geocode( { 'address': search_addr }, function(results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							//alert(search_addr);
							//alert(status);
							var comps = results[0].address_components;
							console.log(results[0]);
							//alert(comps.length);
							var didsearch = 0;
							for (var i = 0; i < comps.length; i++) {
								var comp = comps[i];
								//alert(comp.types[0] + ' ' + comp.long_name); // problem is here, this is not returning postal_code
								if (comp.types[0] == "postal_code") {
									$("#use_zip_code").val(comp.long_name);
									didsearch = 1;
									doSearch();
								}
							}
							if (didsearch == "0") {
								alert("Your search didn't return any results. Try another City/State combination.");
							}
						} // else alert(status);
					});

				};// function doGeoReverse


			} else {
				alert("Please enter your Zip/Postal Code or City and State/Province.");
			}
		});
		$('#gform_ajax_frame_1').load(function() {
			if (! loading) {
				setTimeout(function() {
					if (sales_rep_name)
						$(".gform_title").html("Contact " + sales_rep_name);
					if (sales_rep_email)
						$("#input_1_1").val(sales_rep_email);
					$.fancybox({content:$('#formdiv').html()});
				}, 100);
			} else loading = false;
		});
	});
	function doSearch() {

		

		if ($("#use_zip_code").val() != "") {
			$('#resultsdiv').show();
			$.ajax({
				type: "POST",
				url: "/search/sales_rep_search.php",
				data: $('#searchform').serialize(),
				error: function(msg) {
					alert(msg);//$("#errors").html(msg);
				},
				complete: function (xhr, stat) {
					results = xhr.responseText.toString();
					if (results.substring(0, 5) == "ERROR") {
						alert("There was a search error: " + results.substring(7));
					} else {
						results = $.parseJSON(results);
						var results_html = "";
						for (var row in results) {
							data = results[row];
							sales_rep_email = data["email"];
							sales_rep_name = data["first_name"] + " " + data["last_name"];
							results_html += '<li class="sales-rep-li"><div class="avatar"><img class="sales-rep-image" src="/wp-content/themes/acceledent/img/'+data["first_name"]+data["last_name"]+'.jpg" onerror="this.src=\'/wp-content/themes/acceledent/img/orthodontist-default.jpg\'" /></div>\n';
							results_html += '<div class="results-info"><h3>'+data["first_name"]+' '+data["last_name"]+'</h3>\n';
							results_html += '<h4>'+data["title"]+'</h4>\n';
							results_html += '<p>Phone: '+data["phone"]+'</p>\n';
							if (data["email"] != "") {
								results_html += '<div class="call-to-action"><a href="javascript:void(0);" class="contactlink button" id="contactlink_' + row + '">Email '+data["first_name"]+' '+data["last_name"]+'<span class="arrow"><img src="/wp-content/themes/acceledent/img/blue-arrow.svg" alt=""></span></a></div>\n';
							}
							results_html += '</div></li>\n';
						}
						$('html, body').animate({ scrollTop: 320 });
						if (results_html != "") {
							$('#results_listing').html('<h2>Search Results</h2>\n<ul>\n' + results_html + '</ul>\n');
						} else {
							$('#results_listing').html('<h2>No Search Results</h2>\n');
						}
						$(".contactlink").click(function() {
							var rowid = $(this).attr("id").substr(12);
							$(".gform_title").html("Contact " + results[rowid]["first_name"] + " " + results[rowid]["last_name"]);
							$("#input_1_1").val(results[rowid]["email"]);
							$.fancybox({content:$('#formdiv').html()});
						});
					}
				}
			});
		}
	}
