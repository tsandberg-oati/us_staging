var geo = new google.maps.Geocoder();
var usa = (document.location.hostname.indexOf(".co.uk") > 0 ? false : true);

(function($) {
    $.QueryString = (function(a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i)
        {
            var p=a[i].split('=');
            if (p.length != 2) continue;
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }screen
        return b;
    })(window.location.search.substr(1).split('&'))
})(jQuery);

$(document).ready(function() {
	if (navigator.geolocation)
		$('#locator').show();
	$('input.searchtextfield, select.searchtextfield').each(function() {
		$(this).change(function() {
			$(this).val($.trim($(this).val()));
			if ($(this).attr('id') != "search_radius" && $(this).val() != "") {
				$('#lat_lon').val("");
			}
		});
		$(this).keyup(function(e) {
			if(e.keyCode == 13) {
				setTimeout(function() {
					if ($("#searchbutton").length) { $("#searchbutton").click(); $("#searchbutton").focus(); }
					if ($("#homesearchbutton").length) { $("#homesearchbutton").click(); $("#homesearchbutton").focus(); }
					//$(this).blur();
				}, 100);
			}
		});
	});
	if ($("#homesearchbutton").length) {
		$("#homesearchbutton").click(function() {
			if (($("#city").val() != "" && $("#state_province").val() != "," && $("#state_province").val() != ",US") || ($("#zip_code").val() != "") || ($("#lat_lon").val() != "")) { // valid form entry combinations
				document.location = "/" + (document.location.pathname== "/orthodontists/" ? "about-us/sales-rep-locator" : "doctor-locator") + "/?" + ($("#lat_lon").val() != "" ? "ll=" + $("#lat_lon").val() : ($("#zip_code").val() != "" ? "zp=" + $("#zip_code").val() : "ci=" + $("#city").val() + "&st=" + $("#state_province").val()));
			} else {
				alert("Please enter at least one of the following search parameters:\n\n    - " + (usa ? "City and State\n    - Zip Code" : "City and Country\n    - Postal Code"));
			}
		});
	} else {
		var initted = false;
		if ($.QueryString["ci"]) { $("#city").val($.QueryString["ci"]); initted = true; }
		if ($.QueryString["st"]) { $("#state_province").val($.QueryString["st"]); initted = true; }
		if ($.QueryString["zp"]) { $("#zip_code").val($.QueryString["zp"]); initted = true; }
		if ($.QueryString["ll"]) { $("#lat_lon").val($.QueryString["ll"]); initted = true; }
		if (initted && (($("#city").val() != "" && $("#state_province").val() != "," && $("#state_province").val() != ",US") || ($("#zip_code").val() != "") || ($("#lat_lon").val() != ""))) {
			$("#searchbutton").click();
		}
	}
});

function setLocation() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(pos){
			$('#lat_lon').val("(" + pos.coords.latitude + "," + pos.coords.longitude + ")");
			// $('#address').val("");
			$('#city').val("");
			$('#state_province').val((usa ? ",US" : ","));
			$(".text_state > .customSelect > .customSelectInner").html("-- Select --");
			$('#zip_code').val("");
			if ($("#searchbutton").length) $("#searchbutton").click();
			if ($("#homesearchbutton").length) $("#homesearchbutton").click();
		}, function(err) {
			if (err.code == err.PERMISSION_DENIED) {
				alert("Location Services have been denied for this browser. Please check your Settings.");
			} else if (err.code == err.POSITION_UNAVAILABLE) {
				alert("Location Services are unable to determine your position at this time. You may need to check your Settings.");
			} else if (err.code == err.TIMEOUT) {
				alert("Location Services have timed out.");
			} else {
				alert("Location Services error message: " + err.message);
			}
		});
	} else {
		alert("Geolocation API is not supported in your browser.");
	}
}