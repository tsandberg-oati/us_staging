$(document).ready(function() {
  

if( !$("html").hasClass("lt-ie9") ) {
  /* Move buttons around when browser resizes, necessary for design */
	$(window).resize(function() {
	    clearTimeout(this.id);
	    this.id = setTimeout(doneResizing, 500);
	});
	function doneResizing(){
	      if(window.innerWidth >= 768) {
			$('.buttons').appendTo('.form-header');
		}
		if(window.innerWidth >= 768 && window.innerWidth < 980) {
			$('.get-started .call-to-action').appendTo('.form-header');
		}
		if(window.innerWidth < 768) {
			$('.buttons').appendTo('.find-doctor');
			$('.get-started .call-to-action').appendTo('.find-doctor');
		}
		if(window.innerWidth > 980) {
			$('.text_radius').appendTo('.right-col');
			$('.buttons').appendTo('.right-col');
			$('.get-started .call-to-action').appendTo('.find-doctor');
		}   
	}
  doneResizing();
  
  
  /* Wrap video iframes to control with css */
  $("iframe").wrap("<div class='video-wrapper'/>");
  
  /*Do Fastclick on Menu Button, prevent mobile button lag */
  window.addEventListener('load', function() {
    new FastClick(document.body);
    }, false);
	$('.menu-button').click(function(e){
	  $('.drawer').slideToggle();
	  e.preventDefault();
	});
	    // do stuff
} else {
		/*Replaces svgs with pngs in IE */
  
	  svgeezy.init(false, 'png');

}


/** Fancy Stuff **/
$(function(){
	$('a.fancyserial').attr('href','/wp-content/themes/acceledent/images/serial-number.jpg').fancybox({
		autoSize: true
	});			
});


});
