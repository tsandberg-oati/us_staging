<?php 
/******************************
**** Display Webinar Details ****
******************************/		
?> 

<div class="event-leftrow1">	
	<p class="event-date">	
		<?php 
			$sdate = get_field('webinar_start_date', get_the_ID());
			echo date("m/d/Y", strtotime($sdate));
		?>
	</p>

	
	<h1> <a href="<?php the_field('event_link', get_the_ID()); ?>" target="_blank"><?php the_title(); ?></a></h1>
	
	<?php if( get_field('webinar_featured_speaker') ): ?>
		<p class="txt-blue">Presenter: <?php the_field('webinar_featured_speaker', get_the_ID()); ?>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		   CE Credits: <?php the_field('webinar_ce_credits', get_the_ID()); ?>		
		</p>
	<? endif; ?>
	
	<?php the_content(); ?> 
	
	
	<?php // Display subscribe link only for FUTURE EVENTS  ?>
	<?php if( get_field('webinar_subscribe_link') && (strtotime($sdate) > strtotime('now')) ): ?>
		<a class="button-block" href="<?php the_field('webinar_subscribe_link'); ?>" target="_blank">Enter Class</a>
	<?php endif; ?>
	
	<?php // Display watch link only for PAST EVENTS  ?>
	<?php if( get_field('webinar_video_link') && (strtotime($sdate) < strtotime('now')) ): ?>
		<a class="button-block" href="<?php the_field('webinar_video_link'); ?>" target="_blank">Watch Video</a>
	<?php endif; ?>
	
	
</div>
<div class="eventlogo"> 
	<?php if (has_post_thumbnail( get_the_ID() ) ): ?>
		<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'thumbnail' ); ?>
		<img src="<?php echo $image[0]; ?>">
	<?php endif; ?>
</div>
<div class="clear"></div>