<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * Footer Template
 *
 *
 * @file           footer.php
 * @package        Responsive 
 * @author         Emil Uzelac 
 * @copyright      2003 - 2013 ThemeID
 * @license        license.txt
 * @version        Release: 1.2
 * @filesource     wp-content/themes/responsive/footer.php
 * @link           http://codex.wordpress.org/Theme_Development#Footer_.28footer.php.29
 * @since          available since Release 1.0
 */
?>
    </div><!-- end of #wrapper -->
    <?php responsive_wrapper_end(); // after wrapper hook ?>
</div><!-- end of #container -->

<?php responsive_container_end(); // after container hook ?>

<div id="footer" class="clearfix">

    <div id="footer-wrapper">
    <div class="grid col-220 scroll-top hide-980" >
              <div class="social">
                <div class="find-us hide-320 show-980"> </div>
            <ul class="social-icons">
              <li ><a href="https://www.facebook.com/pages/AcceleDent/110270362431502" class="social-icons-facebook" target="_blank"></a></li>
              <li ><a href="https://twitter.com/acceledent" class="social-icons-twitter" target="_blank"></a></li>
              <li ><a href="https://www.linkedin.com/company/orthoaccel-technologies-inc." class="social-icons-linkedin" target="_blank"></a></li>  
              <li><a href="http://www.youtube.com/user/AcceleDentSystem" class="social-icons-youtube" target="_blank"></a></li>
              <li><a href="<?php echo get_bloginfo('url'); ?>/blog/" class="social-icons-blog"></a>&nbsp;</li>
            </ul>
          </div>
        <!--<div class="contact-us show-320 hide-768">
          Contact Us
        </div>
        <div class="about-us show-320 hide-768">
          <a href="about-us/">About Us</a> | <a href="privacy/">Privacy & Terms</a>
        </div>-->
        </div>
        
        <div class="grid col-220 fit hide-320 show-768 hide-980">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/img/orthoaccel.png" alt="Ortho Accel" class="ortho-accel-logo">
        </div><!-- end .powered -->
        <div style="clear:both;"></div>
                   <div class="hide-320 show-768 hide-980"><span class="list-title">About Us</span></div>
            <div class="grid col-220">
                                <div class="hide-768 show-980"><span class="list-title">About Us</span></div>

          <div class="padding">
          
          <?php if (has_nav_menu('footer-menu', 'responsive')) { ?>
            <?php wp_nav_menu(array(
              'container'       => '',
              'fallback_cb'   =>  false,
              'menu_class'      => 'footer-list',
              'theme_location'  => 'footer-menu')
              ); 
            ?>
              <?php } ?>
          
          </div>
          
          
            </div><!-- end of .copyright -->
            
            <div class="grid col-220">
              <div class="padding">
              <?php if (has_nav_menu('footer-menu-secondary', 'responsive')) { ?>
            <?php wp_nav_menu(array(
              'container'       => '',
              'fallback_cb'   =>  false,
              'menu_class'      => 'footer-list secondary',
              'theme_location'  => 'footer-menu-secondary')
              ); 
            ?>
              <?php } ?></div>
            </div>
            <div class="grid col-220 scroll-top hide-320 hide-768 show-980" >
              <div class="social">
                <div class="find-us hide-320 show-980">Find us</div>
            <ul class="social-icons">
              <li ><a href="https://www.facebook.com/pages/AcceleDent/110270362431502" class="social-icons-facebook" target="_blank"></a></li>
              <li ><a href="https://twitter.com/acceledent" class="social-icons-twitter" target="_blank"></a></li>
              <li ><a href="https://www.linkedin.com/company/orthoaccel-technologies-inc." class="social-icons-linkedin" target="_blank"></a></li>  
              <li><a href="http://www.youtube.com/user/AcceleDentSystem" class="social-icons-youtube" target="_blank"></a></li>
              <li><a href="<?php echo get_bloginfo('url'); ?>/blog/" class="social-icons-blog"></a>&nbsp;</li>
            </ul>
          </div>
        <!--<div class="contact-us show-320 hide-768">
          Contact Us
        </div>
        <div class="about-us show-320 hide-768">
          <a href="about-us/">About Us</a> | <a href="privacy/">Privacy & Terms</a>
        </div>-->
        </div>
       <div class="grid col-220 fit show-320 hide-768 show-980">
            <img src="http://acceledent.com/wp-content/uploads/2015/07/LOGO_OrthoAccel-RGB.jpg" alt="Ortho Accel" class="ortho-accel-logo">
        </div><!-- end .powered -->
	  
    </div><!-- end #footer-wrapper -->
</div><!-- end #footer -->

<div class="copyright grid col-940">
  LEADER IN ACCELERATED ORTHODONTICS<sup>&reg;</sup>
<ul><li>OrthoAccel Technologies, Inc.</li><li>6575 West Loop South, Suite 200</li><li class="city">Bellaire, TX 77401</li><span class="numbers"><li class="phone"><?php echo phone_encode_function('','866-866-4919'); ?></li><li>Email: <?php echo email_encode_function('','info@orthoaccel.com'); ?></li><li>Copyright AcceleDent 2014</li></span></ul>
<a style="text-align:center;" href="http://acceledent.com/sitemap/">Sitemap</a>
</div>
<?php wp_footer(); ?>

<!-- Google Tracking for click event -->
<script type="text/javascript">
    $(document).ready(function(){
        if ( $( "a.trackThisClick" ).length ) {
            $("a.trackThisClick").each(function () {
                var href = $(this).attr("href");
                var target = $(this).attr("target");
                var text = $(this).text();
                $(this).click(function (event) { // when someone clicks these links
                    event.preventDefault(); // don't open the link yet
                    ga('send', 'event', 'Links', 'Click', href);
                    setTimeout(function () { // now wait 300 milliseconds...
                        window.open(href, (!target ? "_self" : target)); // ...and open the link as usual
                    }, 300);
                });
            });
        }//end if
    });
</script>

</body>
</html>