<?php
/*
Plugin Name: OrthoAccel Locators Administration
Description: Import and export tab-delineated Doctor and Sales Rep spreadsheets for the site locator tools
Version: 0.9
Author: Portent, Inc.
Author URI: http://www.portent.com
------------------------------------------------------------------------
*/

$is_dev = true; // change to false
$doc_filetype = (strpos($_SERVER["HTTP_HOST"], ".co.uk") ? "uk" : "usa");
$doc_table = "docs_".$doc_filetype;
$eol = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' ? "\r\n" : "\n");

add_action('init', 'permissions_check');
add_action('init', 'handle_export');
add_action('init', 'handle_import');
add_action('admin_menu', 'orthoaccel_plugin_menu');

function orthoaccel_plugin_menu() {
  add_menu_page('Locators Admin', 'Locators Admin', 'manage_options', 'doctors', 'doctors_manage');
  //add_submenu_page('locators', 'Doctors Import/Export', 'Doctors Import/Export', 'manage_options', 'doctors', 'doctors_manage');
  add_submenu_page('doctors', 'Sales Reps Import/Export', 'Sales Reps Import/Export', 'manage_options', 'sales-reps', 'sales_rep_manage');
  global $submenu;
  if ( isset( $submenu['doctors'] ) )
  	$submenu['doctors'][0][0] = 'Doctors Import/Export';
  
}

function handle_export() {
	if (isset($_POST['_wpnonce-orthoaccel-export-doctors'])) {
		check_admin_referer('orthoaccel-export-doctors', '_wpnonce-orthoaccel-export-doctors');
		doctors_export();
	} else if (isset($_POST['_wpnonce-orthoaccel-export-sales_rep'])) {
		check_admin_referer('orthoaccel-export-sales_rep', '_wpnonce-orthoaccel-export-sales_rep');
		sales_rep_export();
	}
}

function handle_import() {
	if (isset($_POST['_wpnonce-orthoaccel-import-doctors'])) {
		check_admin_referer('orthoaccel-import-doctors', '_wpnonce-orthoaccel-import-doctors');
		doctors_import();
	} else if (isset($_POST['_wpnonce-orthoaccel-import-sales_rep'])) {
		check_admin_referer('orthoaccel-import-sales_rep', '_wpnonce-orthoaccel-import-sales_rep');
		sales_rep_import();
	}
}

function sales_rep_manage() {
	manager_output($doc=false);
}

function doctors_manage() {
	manager_output($doc=true);
}

function manager_output($for_docs) {
	global $is_dev, $doc_filetype, $doc_table;
?>
	<div class="wrap">
		<div id="icon-options-general" class="icon32"><br /></div>
		<h2><?= ($for_docs ? "Doctor" : "Sales Rep") ?> Locator data import/export tool</h2>
		<p>This functionality is in development by Portent, Inc. Do not use at this time. Thank you.</p>
		<div>
			<form name="importform" id="importform" action="admin.php?page=<?= ($for_docs ? "doctors" : "sales_rep") ?>" target="submitframe" method="POST" onsubmit="return checkimport(this);" enctype="multipart/form-data" style="padding:10px;background-color:#eeeeee;text-align:right;display:inline-block;vertical-align:top;">
				<?php wp_nonce_field('orthoaccel-import-'.($for_docs ? "doctors" : "sales_rep"), '_wpnonce-orthoaccel-import-'.($for_docs ? "doctors" : "sales_rep")); ?>
				<input type="hidden" name="formtype" value="import" />
				<input type="hidden" name="filetype" value="<?= $doc_filetype ?>" />
				<strong>Import File:</strong>
				<input type="file" name="importfile" id="importfile" />
				<br /><br />
				<input type="submit" id="importbutton" value="Import to Database" />
			</form>
			<form name="exportform" id="exportform" action="admin.php?page=<?= ($for_docs ? "doctors" : "sales_rep") ?>" target="submitframe" method="POST" onsubmit="return checkexport(this);" style="margin-left:20px;padding:10px;background-color:#eeeeee;text-align:right;display:inline-block;vertical-align:top;">
				<?php wp_nonce_field('orthoaccel-export-'.($for_docs ? "doctors" : "sales_rep"), '_wpnonce-orthoaccel-export-'.($for_docs ? "doctors" : "sales_rep")); ?>
				<input type="hidden" name="formtype" value="export" />
				<input type="hidden" name="filetype" value="<?= $doc_filetype ?>" />
				<input type="submit" id="exportbutton" value="Export to File" />
			</form>
			<div id="results" style="padding:10px;"></div>
		</div>
		<script>
			function checkimport(fm) {
				if (! fm.importfile.value) {
					alert("Please select a file to import.");
					return false;
				} else if (confirm("Please confirm that you want to import this file and that it is in the proper format for this import process.\nExisting records in the database will be removed first before importing the data in the import file.")) {
					document.getElementById("importbutton").disabled = true;
					document.getElementById("exportbutton").disabled = true;
					document.getElementById("results").style.backgroundColor = "#eeeeee";
					document.getElementById("results").innerHTML = "<strong>Importing... Please be patient.";
				} else return false;
			}
			function checkexport(fm) {
				document.getElementById("importbutton").disabled = true;
				document.getElementById("exportbutton").disabled = true;
				document.getElementById("results").style.backgroundColor = "#eeeeee";
				document.getElementById("results").innerHTML = "<strong>Exporting... Please be patient.";
				setTimeout(function() { setresults(document.getElementById("results").innerHTML); }, 5000);
			}
			function setresults(text) {
				document.getElementById("importbutton").disabled = false;
				document.getElementById("exportbutton").disabled = false;
				if (text)
					document.getElementById("results").innerHTML = text;
			}
		</script>
		<iframe name="submitframe" id="submitframe" src="javascript:void(0);" style="<?=($is_dev?'width:800px;height:500px;border:1px solid black;':'width:0px;height:0px;border:0;')?>"></iframe>
	</div>
<?	
}

function doctors_import() {
	global $wpdb;
	exit();
}

function doctors_export() {
	global $wpdb, $doc_filetype, $doc_table;
	$wpdb->show_errors();
	$wpdb->suppress_errors(false);
	$query = $wpdb->get_results("SELECT first_name, last_name, degree, practice_name, phone, website, email, address, city, ".($doc_filetype == "usa" ? "state_province, zip_code" : "postal_code, country").", specialty, offers_acceledent FROM ".$doc_table." ORDER BY id");
	$output = "FIRST_NAME\tLAST_NAME\tDEGREE\tPRACTICE_NAME\tPHONE\tWEBSITE\tEMAIL\tADDRESS\tCITY\t".($doc_filetype == "usa" ? "STATE_PROVINCE\tZIP_CODE" : "POSTAL_CODE\tCOUNTRY")."\tSPECIALTY\tOFFERS_ACCELEDENT".$eol;
	$rowcount = 0;
	foreach($query as $row) {
		$first_name = exportstring($row->first_name);
		$last_name = exportstring($row->last_name);
		$degree = exportstring($row->degree);
		$practice_name = exportstring($row->practice_name);
		$phone = exportstring($row->phone);
		$website = exportstring($row->website);
		$email = exportstring($row->email);
		$address = exportstring($row->address);
		$city = exportstring($row->city);
		if ($doc_filetype == "usa") {
			$state_province = exportstring($row->state_province);
			$zip_code = exportstring($row->zip_code);
		} else {
			$postal_code = exportstring($row->postal_code);
			$country = exportstring($row->country);
		}
		$specialty = exportstring($row->specialty);
		$offers_acceledent = exportstring($row->offers_acceledent);
		$output .= $first_name."\t".$last_name."\t".$degree."\t".$practice_name."\t".$phone."\t".$website."\t".$email."\t".$address."\t".$city."\t".($doc_filetype == "usa" ? $state_province."\t".$zip_code : $postal_code."\t".$country)."\t".$specialty."\t".$offers_acceledent.$eol;
		$rowcount++;
	}
	$filename = $doc_filetype."_doc_list_".date("m_d_Y").".txt";
	header('Content-Description: File Transfer');
	header('Content-Type: text/plain'); 
    header('Content-Disposition: attachment; filename="' . $filename . '"');
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Expires: 0');
    header('Content-Length: ' . strlen($output));
	echo $output;
	exit();
}

function sales_rep_import() {
	global $wpdb;
	exit();
}

function sales_rep_export() {
	global $wpdb;
	exit();
}

function permissions_check() {
	if (!current_user_can('manage_options'))  {
		wp_die( __('You do not have sufficient permissions to access this page.') );
	}
}

function importstring($text, $max, $row, $colname) {
	if (strpos($text, '"') === 0)
		$text = substr($text, 1);
	if (strrpos($text, '"') == strlen($text) - 1 && strlen($text) > 1)
		$text = substr($text, 0, strlen($text) - 1);
	$text = trim(str_replace('""', '"', str_replace("'", "''", $text)));
	$ret = array("text" => $text, "error" => "");
	if (strlen($text) > $max) {
		$ret["text"] = substr($ret["text"], 0, $max);
		$ret["error"] = "<li>Row ".($row + 1).": [WARNING] Column '".$colname."' has been truncated to ".$max." characters.</li>";
	} else if (strlen($text) == 0) {
		$ret["error"] .= "<li>Row ".($row + 1).": [WARNING] Column '".$colname."' is empty.</li>";
	}
	return $ret;
}

function exportstring($text) {
	$text = '"'.str_replace('"', '""', $text).'"';
	return $text;
}

function zeroPad($str) {
	if ($str != "" && strlen($str) < 5)
		return str_repeat("0", 5 - strlen($str)).$str;
	else return $str;
}

function get($name, $array=null) {
	if(!$array)
		$array = $_GET;
	if(isset($array[$name]))
		return $array[$name];
	return "";
}

?>
