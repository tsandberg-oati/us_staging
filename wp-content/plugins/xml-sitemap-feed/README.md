# XML Sitemap &amp; Google News feeds

XML and Google News Sitemaps for WordPress to feed the hungry spiders. Multisite, WP Super Cache and Polylang compatible.
