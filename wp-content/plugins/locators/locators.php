<?php
/*
Plugin Name: OrthoAccel Locators Administration
Description: Import and export tab-delineated Doctor and Sales Rep spreadsheets for the site locator tools
Version: 1.1
Author: Portent, Inc.
Author URI: http://www.portent.com
------------------------------------------------------------------------
*/

$is_dev = false; // change to false
$doc_filetype = (strpos($_SERVER["HTTP_HOST"], ".co.uk") ? "uk" : "usa");
$doc_table = "docs_".$doc_filetype;

add_action('init', 'handle_export');
add_action('init', 'handle_import');
add_action('admin_menu', 'orthoaccel_plugin_menu');

function orthoaccel_plugin_menu() {
	global $doc_filetype, $orthoaccel_menu_hook_doctors, $orthoaccel_menu_hook_sales_reps, $submenu;
 	$orthoaccel_menu_hook_doctors = add_menu_page('Locators Admin', 'Locators Admin', 'manage_options', 'doctors', 'doctors_manage');
 	add_action('load-'.$orthoaccel_menu_hook_doctors, 'orthoaccel_plugin_help');
	if ($doc_filetype == "usa") {
		$orthoaccel_menu_hook_sales_reps = add_submenu_page('doctors', 'Sales Rep Import/Export', 'Sales Rep Import/Export', 'manage_options', 'sales-reps', 'sales_rep_manage');
		add_action('load-'.$orthoaccel_menu_hook_sales_reps, 'orthoaccel_plugin_help');
		if ( isset( $submenu['doctors'] ) )
			$submenu['doctors'][0][0] = 'Doctors Import/Export';
	}
}

function orthoaccel_plugin_help() {
	global $orthoaccel_menu_hook_doctors, $orthoaccel_menu_hook_sales_reps, $doc_filetype;
	$screen = get_current_screen();
	if ($screen->id != $orthoaccel_menu_hook_doctors && $screen->id != $orthoaccel_menu_hook_sales_reps) {
		return;
	} else {
		$screen->add_help_tab(array(
	        'id'	=> 'orthoaccel_doctors_help_tab',
	        'title'	=> 'Doctors Import/Export',
	        'content'	=> '<h3>Locators Admin > Doctors Import/Export</h3>' .
							'<p><strong>File Format:</strong></p>' .
							'<p>A valid doctors file is a tab-delimited .txt file that can be opened in a spreadsheet editor such as Excel (right-click, "Open with..."). When editing is complete, it should be saved to the same tab-delimited .txt format.</p>' .
							'<p>The doctors file consists of a header row defining 15 columns:</p>' .
							'<p style="margin-left:20px;">first_name, last_name, degree, practice_name, phone, website, email, address,<br />city, '.($doc_filetype == "usa" ? "state_province, zip_code" : "postal_code, country").', specialty, offers_acceledent, latitude, longitude</p>' .
							'<p>The "first_name", "last_name" and "degree" fields are all elements of the doctor&apos;s name, and are typically appended together for site display purposes. Therefore it is okay to leave one or more of them blank.</p>' .
							'<p>The "practice_name" field represents the doctor&apos;s business name. If it is left blank, the name fields mentioned above will be used instead for display purposes.</p>' .
							'<p>The "offers_acceledent" field should be either a "1" for doctors that currently offer Acceledent or a "0" for those who do not. Doctors with a "0" and a valid value in the "email" field will in the future feature a "Contact this Doctor" functionality.</p>' .
							'<p>The "provider_type" field should be ...</p>' .
							'<p>The "latitude" and "longitude" fields may be left blank, and the application will attempt to use geolocation from the address fields to fill in appropriate coordinates. It is recommended, however, to leave these values in place once they are set, unless the doctor&apos;s business address changes.</p>' .
							'<p><strong>Exporting:</strong></p>' .
							'<p>Click the "Export Doctors to File" button in order to download a file with doctors data from the database to your local computer. It is recommended that you do this each time prior to editing, in order to acquire a properly formatted doctors file with the latest data.</p>' .
							'<p><strong>Importing:</strong></p>' .
							'<p>Click the "Browse" button and find the saved tab-delimited doctors file on your local computer, and then click the "Import to Database" button.</p>' .
							'<p>As the import process runs, it will display output messages of any errors and warnings, along with a count of records imported or ignored. By default, warnings are generated for all blank fields encountered in the doctors file, even if those fields are not required. To turn off these informational warnings, click the "Hide blank field warnings" checkbox prior to importing.</p>' 
		));
		$screen->add_help_tab(array(
	        'id'	=> 'orthoaccel_sales_reps_help_tab',
	        'title'	=> 'Sales Rep Import/Export',
	        'content'	=> '<h3>Locators Admin > Sales Rep Import/Export</h3>' .
							'<p><strong>File Format:</strong></p>' .
							'<p>A valid sales rep file is a tab-delimited .txt file that can be opened in a spreadsheet editor such as Excel (right-click, "Open with..."). When editing is complete, it should be saved to the same tab-delimited .txt format.</p>' .
							'<p>The sales rep file consists of a header row defining 6 columns:</p>' .
							'<p style="margin-left:20px;">zip_prefix, first_name, last_name, title, email, phone</p>' .
							'<p>The "zip_prefix" field is a three-digit (USA) or three-letter-and-digit (Canada) prefix that encodes a zip/postal code region where the sales rep has coverage. For example, "981" would encode for all zip codes from 98101 to 98199.</p>' .
							'<p>All fields are required for each sales rep record, and any rows missing a field will not be imported.</p>' .
							'<p><strong>Exporting:</strong></p>' .
							'<p>Click the "Export Sales Reps to File" button in order to download a file with sales rep data from the database to your local computer. It is recommended that you do this each time prior to editing, in order to acquire a properly formatted sales rep file with the latest data.</p>' .
							'<p><strong>Importing:</strong></p>' .
							'<p>Click the "Browse" button and find the saved tab-delimited sales rep file on your local computer, and then click the "Import to Database" button.</p>' .
							'<p>As the import process runs, it will display output messages of any errors and warnings, along with a count of records imported or ignored.</p>'
		));
	}
}

function handle_export() {
	if (isset($_POST['_wpnonce-orthoaccel-export-doctors'])) {
		check_admin_referer('orthoaccel-export-doctors', '_wpnonce-orthoaccel-export-doctors');
		doctors_export();
	} else if (isset($_POST['_wpnonce-orthoaccel-export-sales_rep'])) {
		check_admin_referer('orthoaccel-export-sales_rep', '_wpnonce-orthoaccel-export-sales_rep');
		sales_rep_export();
	}
}

function handle_import() {
	if (isset($_POST['_wpnonce-orthoaccel-import-doctors'])) {
		check_admin_referer('orthoaccel-import-doctors', '_wpnonce-orthoaccel-import-doctors');
		doctors_import();
	} else if (isset($_POST['_wpnonce-orthoaccel-import-sales_rep'])) {
		check_admin_referer('orthoaccel-import-sales_rep', '_wpnonce-orthoaccel-import-sales_rep');
		sales_rep_import();
	}
}

function sales_rep_manage() {
	manager_output($doc=false);
}

function doctors_manage() {
	manager_output($doc=true);
}

function manager_output($for_docs) {
	permissions_check();
	global $is_dev, $doc_filetype, $doc_table;
?>
	<div class="wrap">
		<div id="icon-options-general" class="icon32"><br /></div>
		<h2><?= ($for_docs ? "Doctor" : "Sales Rep") ?> Locator data import/export tool</h2>
		<p>Instructions for this admin tool may be found under the "Help" tab in the upper-right of the screen.</p>
		<div>
			<form name="exportform" id="exportform" action="admin.php?page=<?= ($for_docs ? "doctors" : "sales_rep") ?>" target="submitframe" method="POST" onsubmit="return checkexport(this);" style="padding:10px;background-color:#eeeeee;text-align:right;display:inline-block;vertical-align:top;">
				<?php wp_nonce_field('orthoaccel-export-'.($for_docs ? "doctors" : "sales_rep"), '_wpnonce-orthoaccel-export-'.($for_docs ? "doctors" : "sales_rep")); ?>
				<strong>Export from Database:</strong>
				<input type="hidden" name="formtype" value="export" />
				<input type="hidden" name="filetype" value="<?= $doc_filetype ?>" />
				<input type="submit" id="exportbutton" value="Export <?=($for_docs ? "Doctors" : "Sales Reps")?> to File" />
			</form>
			<br /><br />
			<form name="importform" id="importform" action="admin.php?page=<?= ($for_docs ? "doctors" : "sales_rep") ?>" target="submitframe" method="POST" onsubmit="return checkimport(this);" enctype="multipart/form-data" style="padding:10px;background-color:#eeeeee;text-align:right;display:inline-block;vertical-align:top;">
				<?php wp_nonce_field('orthoaccel-import-'.($for_docs ? "doctors" : "sales_rep"), '_wpnonce-orthoaccel-import-'.($for_docs ? "doctors" : "sales_rep")); ?>
				<input type="hidden" name="formtype" value="import" />
				<input type="hidden" name="filetype" value="<?= $doc_filetype ?>" />
				<strong><?=($for_docs ? "Doctors" : "Sales Rep")?> Import File:</strong>
				<input type="file" name="importfile" id="importfile" style="margin-right:10px;" />
				<?php if ($for_docs) { ?>
					<strong>Hide blank field warnings</strong>
					<input type="checkbox" name="hidewarnings" value="1" style="margin-right:10px;" />
				<?php } ?>
				<input type="submit" id="importbutton" value="Import to Database" />
			</form>
			<div id="results" style="padding:10px;"></div>
		</div>
		<script>
			function checkimport(fm) {
				if (! fm.importfile.value) {
					alert("Please select a file to import.");
					return false;
				} else if (confirm("Please confirm that you want to import this file and that it is in the proper format for this import process.\nExisting records in the database will be removed first before importing the data in the import file.")) {
					document.getElementById("importbutton").disabled = true;
					document.getElementById("exportbutton").disabled = true;
					document.getElementById("results").style.backgroundColor = "#eeeeee";
					document.getElementById("results").innerHTML = "<strong>Importing... Please be patient.";
				} else return false;
			}
			function checkexport(fm) {
				document.getElementById("importbutton").disabled = true;
				document.getElementById("exportbutton").disabled = true;
				document.getElementById("results").style.backgroundColor = "#eeeeee";
				document.getElementById("results").innerHTML = "<strong>Exporting... Please be patient.";
				setTimeout(function() { setresults(""); }, 5000);
			}
			function setresults(text) {
				document.getElementById("importbutton").disabled = false;
				document.getElementById("exportbutton").disabled = false;
				if (text)
					document.getElementById("results").innerHTML = text;
			}
		</script>
		<iframe name="submitframe" id="submitframe" src="javascript:void(0);" style="<?=($is_dev?'width:800px;height:500px;border:1px solid black;':'width:0px;height:0px;border:0;')?>"></iframe>
	</div>
<?	
}

function get_import_file_rows() {
	if ($_FILES["importfile"]["error"] > 0) { // check for upload error
		echo "<script>top.alert('".$_FILES["file"]["error"]."');top.setresults('<strong>ERROR: ".$_FILES["file"]["error"]."</strong><br />0 records imported.');</script>";
		exit();
	}
	$parts = explode(".", $_FILES["importfile"]["name"]);
	if (end($parts) != "txt" || $_FILES["importfile"]["type"] != "text/plain") { // check for correct file type
		echo "<script>top.alert('Import File must be a tab-delimited .txt file.');top.setresults('<strong>ERROR: Import File must be a tab-delimited .txt file.</strong><br />0 records imported.');</script>";
		exit();
	}
	$file = fopen($_FILES["importfile"]["tmp_name"], "r");
	if (! $file) { // if error opening file
		echo "<script>top.alert('An unexpected filesystem error occurred.');top.setresults('<strong>ERROR: An unexpected filesystem error occurred.</strong><br />0 records imported.');</script>";
		exit();
	}
	$rows = explode("\r\n", fread($file, $_FILES["importfile"]["size"]));
	fclose($file);
	return $rows;
}

function doctors_import() {
	global $wpdb, $doc_filetype, $doc_table, $is_dev;
	//$wpdb->show_errors();
	$rows = get_import_file_rows();
	$hide_warnings = (isset($_POST["hidewarnings"]) && $_POST["hidewarnings"] == "1" ? true : false);
	$rowerrors = "";
	$insertcount = 0;
	$errorcount = 0;
	//	mysql_set_charset("latin1", $connect);
	$dirty = $wpdb->query("UPDATE ".$doc_table." SET is_dirty = 1");
	for ($i = 0; $i < count($rows); $i++) {
		$cells = explode("\t", $rows[$i]);
		if ($i == 0 && $cells[0] == "FIRST_NAME") { // ignore header row if exists
			continue;
		}
		if (count($cells) < 16) { // row needs at least 16 columns or we ignore and return an error
			if ($i == count($rows)) 
				continue;
			$rowerrors .= "<li>Row ".($i + 1).": [ERROR] Row has less than 16 columns (first_name, last_name, degree, practice_name, phone, website, email, address, city, ".($doc_filetype == "usa" ? "state_province, zip_code" : "postal_code, country").", specialty, offers_acceledent, provider_type, latitude, longitude). Row data not imported. Please fix and re-upload.</li>";
			$errorcount++;
			continue;
		}
		$rowerror = "";
		$first_name = importstring($cells[0], 200, $i, "first_name", $hide_warnings, true);
		$rowerror .= $first_name["error"];
		$last_name = importstring($cells[1], 200, $i, "last_name", $hide_warnings, false);
		$rowerror .= $last_name["error"];
		$degree = importstring($cells[2], 200, $i, "degree", $hide_warnings, false);
		$rowerror .= $degree["error"];
		$practice_name = importstring($cells[3], 100, $i, "practice_name", $hide_warnings, false);
		$rowerror .= $practice_name["error"];
		$phone = importstring($cells[4], 30, $i, "phone", $hide_warnings, false);
		$rowerror .= $phone["error"];
		$website = importstring($cells[5], 100, $i, "website", $hide_warnings, false);
		$rowerror .= $website["error"];
		$email = importstring($cells[6], 100, $i, "email", $hide_warnings, false);
		$rowerror .= $email["error"];
		$address = importstring($cells[7], 100, $i, "address", $hide_warnings, true);
		$rowerror .= $address["error"];
		$city = importstring($cells[8], 50, $i, "city", $hide_warnings, true);
		$rowerror .= $city["error"];
		if ($doc_filetype == "usa") {
			$state_province = importstring($cells[9], 3, $i, "state_province", $hide_warnings, true);
			$rowerror .= $state_province["error"];
			$zip_code = importstring($cells[10], 10, $i, "zip_code", $hide_warnings, true);
			$rowerror .= $zip_code["error"];
		} else {
			$postal_code = importstring($cells[9], 10, $i, "postal_code", $hide_warnings, false);
			$rowerror .= $postal_code["error"];
			$country = importstring($cells[10], 3, $i, "country", $hide_warnings, true);
			$rowerror .= $country["error"];
		}
		$specialty = importstring($cells[11], 30, $i, "specialty", $hide_warnings, false);
		$rowerror .= $specialty["error"];
		$offers_acceledent = importstring($cells[12], 1, $i, "offers_acceledent", $hide_warnings, true);
		$rowerror .= $offers_acceledent["error"];
		$provider_type = importstring($cells[13], 1, $i, "provider_type", $hide_warnings, true);
		$rowerror .= $provider_type["error"];
		$latitude = importstring($cells[14], 12, $i, "latitude", true, false);
		$rowerror .= $latitude["error"];
		$longitude = importstring($cells[15], 12, $i, "longitude", true, false);
		$rowerror .= $longitude["error"];
		$sapaccountID = importstring($cells[16], 12, $i, "sapaccountID", false, true);
		$coupon = importstring($cells[17], 12, $i, "coupon", false, true);
		$filename = importstring($cells[18],50, $i, "filename", false, true);
		
		if ($coupon["text"] != "") {
			if(validateDate($coupon["text"])) {
			} else {
			list($month,$day,$year) = explode("/",$coupon["text"]);
			$coupon["text"] = $year . "-" . $month . "-" . $day;
			}
		}
		
		$rowerrors .= $rowerror;
		if (strpos($rowerror, "[ERROR]")) {
			// missing a required field
			$errorcount++;
			continue;
		}
		if ($latitude["text"] == "" && $longitude["text"] == "") {
			$get_id = $wpdb->get_var("SELECT id FROM ".$doc_table." WHERE is_dirty = is_dirty AND address = '".$address["text"]."' AND city = '".$city["text"].($doc_filetype == "usa" ? "' AND state_province = '".$state_province["text"]."' AND zip_code = '".zeroPad($zip_code["text"]) : "' AND postal_code = '".$postal_code["text"]."' AND country = '".$country["text"])."' AND latitude IS NOT NULL AND longitude IS NOT NULL");
		} else {
			$get_id = $wpdb->get_var("SELECT id FROM ".$doc_table." WHERE is_dirty = is_dirty AND address = '".$address["text"]."' AND city = '".$city["text"].($doc_filetype == "usa" ? "' AND state_province = '".$state_province["text"]."' AND zip_code = '".zeroPad($zip_code["text"]) : "' AND postal_code = '".$postal_code["text"]."' AND country = '".$country["text"])."' AND latitude = '".$latitude["text"]."' AND longitude = '".$longitude["text"]."'");
		}
		if (is_numeric($get_id)) { // found at least one address match, update the record
			//echo "found ".$get_id."<br />";
			$upd_ins_str = "UPDATE ".$doc_table." SET first_name = '".$first_name["text"]."', last_name = '".$last_name["text"]."', degree = '".$degree["text"]."', practice_name = '".$practice_name["text"]."', phone = '".$phone["text"]."', website = '".$website["text"]."', email = '".$email["text"]."', specialty = '".$specialty["text"]."', offers_acceledent = ".$offers_acceledent["text"].", provider_type = ". $provider_type["text"] . ", sapaccountID = '". $sapaccountID["text"] . "', coupon = '" . $coupon["text"] . "', filename= '" . $filename["text"] . "',  is_dirty = 0 WHERE id = ".$get_id;
			$upd_ins = $wpdb->query($upd_ins_str);
		} else { // didn't find a match, insert
			if ($latitude["text"] == "" || $longitude["text"] == "") {
				if ($is_dev) echo "Row ".($i + 1).": didn't find, inserting new, searching ".urlencode(utf8_encode($address["text"].", ".$city["text"].", ".($doc_filetype == "usa" ? $state_province["text"].", ".zeroPad($zip_code["text"]) : $postal_code["text"].", ".$country["text"])))."<br />";
				usleep(200000); // rate limit to 5 google geolocation api calls per second
				$geo = simplexml_load_string(file_get_contents("http://maps.googleapis.com/maps/api/geocode/xml?address=".urlencode(utf8_encode($address["text"].", ".$city["text"].", ".($doc_filetype == "usa" ? $state_province["text"].", ".zeroPad($zip_code["text"]) : $postal_code["text"].", ".$country["text"])))."&sensor=false"));
				if ($geo->status == "OK") {
					$latitude["text"] = $geo->result[0]->geometry->location->lat;
					$longitude["text"] = $geo->result[0]->geometry->location->lng;
					if ($is_dev) echo $geo->status." ".$latitude["text"].",".$longitude["text"]."<br />";
				} else {
					$rowerrors .= "<li>Row ".($i + 1).": [ERROR] ".$geo->status."; Address data (address, city, ".($doc_filetype == "usa" ? "state_province, zip_code" : "postal_code, country").") does not encode to a latitude/longitude (according to Google). Row data not imported. Please fix and re-upload.</li>";
					$errorcount++;
					continue;
				}
			}
			$upd_ins_str = "INSERT INTO ".$doc_table." (first_name, last_name, degree, practice_name, phone, website, email, address, city, ".($doc_filetype == "usa" ? "state_province, zip_code" : "postal_code, country").", specialty, offers_acceledent, provider_type, latitude, longitude,sapaccountID,coupon,filename) VALUES ('".$first_name["text"]."', '".$last_name["text"]."', '".$degree["text"]."', '".$practice_name["text"]."', '".$phone["text"]."', '".$website["text"]."', '".$email["text"]."', '".$address["text"]."', '".$city["text"]."', '".($doc_filetype == "usa" ? $state_province["text"]."', '".zeroPad($zip_code["text"]) : $postal_code["text"]."', '".$country["text"])."', '".$specialty["text"]."', ".$offers_acceledent["text"].", ".$provider_type["text"].", ".$latitude["text"].", ".$longitude["text"].",'" . $sapaccountID["text"] . "','" . $coupon["text"] . "','" . $filename["text"] . "')";
			$upd_ins = $wpdb->query($upd_ins_str);
		}
		if ($upd_ins === false) { // if error inserting into database
			if ($is_dev) echo str_replace(")", "<br />)<br />", str_replace("[", "<br />[", print_r($rows[$i], true)));
			if ($is_dev) echo str_replace(")", "<br />)<br />", str_replace("[", "<br />[", print_r($cells, true)));
			$rowerrors .= "<li>Row ".($i + 1).": [ERROR] There was an error inserting the record. ".$wpdb->last_error.", \"".$upd_ins_str."\". Row data not imported. Please fix and re-upload.</li>";
			$errorcount++;
			continue;
		} else $insertcount++;
	}
	$delete_dirty = $wpdb->query("DELETE FROM ".$doc_table." WHERE is_dirty = 1"); // remove any remaining dirty records
	$createcouponlink = $wpdb->query("UPDATE ".$doc_table." SET couponlinkID = MD5(id) WHERE sapaccountID > 0"); //update coupon link
	if ($is_dev) echo $rowerrors;
	echo '<script>top.setresults("<strong>Import Results</strong><br />'.$insertcount.' records imported.<br />'.$errorcount.' records NOT imported.<br />'.str_replace('"','\"',$rowerrors).'");</script>';
	exit();
}

function doctors_export() {
	global $wpdb, $doc_filetype, $doc_table;
//	$wpdb->show_errors();
	$query = $wpdb->get_results("SELECT first_name, last_name, degree, practice_name, phone, website, email, address, city, ".($doc_filetype == "usa" ? "state_province, zip_code" : "postal_code, country").", specialty, offers_acceledent, provider_type, latitude,longitude,sapaccountID,coupon,filename FROM ".$doc_table." ORDER BY id");
	$output = "FIRST_NAME\tLAST_NAME\tDEGREE\tPRACTICE_NAME\tPHONE\tWEBSITE\tEMAIL\tADDRESS\tCITY\t".($doc_filetype == "usa" ? "STATE_PROVINCE\tZIP_CODE" : "POSTAL_CODE\tCOUNTRY")."\tSPECIALTY\tOFFERS_ACCELEDENT\tPROVIDER_TYPE\tLATITUDE\tLONGITUDE\tSAPACCOUNTID\tCOUPON\tFILENAME"."\r\n";
	$rowcount = 0;
	foreach($query as $row) {
		$first_name = exportstring($row->first_name);
		$last_name = exportstring($row->last_name);
		$degree = exportstring($row->degree);
		$practice_name = exportstring($row->practice_name);
		$phone = exportstring($row->phone);
		$website = exportstring($row->website);
		$email = exportstring($row->email);
		$address = exportstring($row->address);
		$city = exportstring($row->city);
		if ($doc_filetype == "usa") {
			$state_province = exportstring($row->state_province);
			$zip_code = exportstring($row->zip_code);
		} else {
			$postal_code = exportstring($row->postal_code);
			$country = exportstring($row->country);
		}
		$specialty = exportstring($row->specialty);
		$offers_acceledent = exportstring($row->offers_acceledent);
		$provider_type = exportstring($row->provider_type);
		$latitude = exportstring($row->latitude);
		$longitude = exportstring($row->longitude);
		$sapaccountid = exportstring($row->sapaccountID);
		$coupon = exportstring($row->coupon);
		$filename = exportstring($row->filename);
		$output .= $first_name."\t".$last_name."\t".$degree."\t".$practice_name."\t".$phone."\t".$website."\t".$email."\t".$address."\t".$city."\t".($doc_filetype == "usa" ? $state_province."\t".$zip_code : $postal_code."\t".$country)."\t".$specialty."\t".$offers_acceledent."\t".$provider_type."\t".$latitude."\t".$longitude."\t".$sapaccountid."\t".$coupon."\t".$filename."\r\n";
		$rowcount++;
	}
	$filename = $doc_filetype."_doc_list_".date("m_d_Y").".txt";
	do_export($filename, $output);
	exit();
}

function sales_rep_import() {
	global $wpdb, $doc_filetype, $doc_table, $is_dev;
//	$wpdb->show_errors();
	$rows = get_import_file_rows();
	$rowerrors = "";
	$insertcount = 0;
	$errorcount = 0;
	$delete = $wpdb->query("DELETE FROM sales_rep");
	for ($i = 0; $i < count($rows); $i++) {
		$cells = explode("\t", $rows[$i]);
		if ($i == 0 && $cells[0] == "ZIP_PREFIX") { // ignore header row if exists
			continue;
		}
		if (count($cells) < 6) { // row needs at least 6 columns or we ignore and return an error
			if ($i == count($rows) - 1) // last row ignore
				continue;
			$rowerrors .= "<li>Row ".($i + 1).": [ERROR] Row has less than 6 columns (zip_prefix, first_name, last_name, title, email, phone). Row data not imported. Please fix and re-upload.</li>";
			$errorcount++;
			continue;
		}
		$rowerror = "";
		$zip_prefix = importstring($cells[0], 10, $i, "zip_prefix", false, true);
		$rowerror .= $zip_prefix["error"];
		if (strlen($zip_prefix["text"]) == 1)
			$zip_prefix["text"] = "00".$zip_prefix["text"];
		if (strlen($zip_prefix["text"]) == 2)
			$zip_prefix["text"] = "0".$zip_prefix["text"];
		$first_name = importstring($cells[1], 100, $i, "first_name", false, true);
		$rowerror .= $first_name["error"];
		$last_name = importstring($cells[2], 100, $i, "last_name", false, true);
		$rowerror .= $last_name["error"];
		$title = importstring($cells[3], 100, $i, "title", false, true);
		$rowerror .= $title["error"];
		$email = importstring($cells[4], 100, $i, "email", false, true);
		$rowerror .= $email["error"];
		$phone = importstring($cells[5], 20, $i, "phone", false, true);
		$rowerror .= $phone["error"];
		$rowerrors .= $rowerror;
		if (strpos($rowerror, "[ERROR]")) {
			// missing a required field
			$errorcount++;
			continue;
		}
		$upd_ins_str = "INSERT INTO sales_rep (zip_prefix, first_name, last_name, title, email, phone) VALUES ('".$zip_prefix["text"]."', '".$first_name["text"]."', '".$last_name["text"]."', '".$title["text"]."', '".$email["text"]."', '".$phone["text"]."')";
		$upd_ins = $wpdb->query($upd_ins_str);
		if ($upd_ins === false) { // if error inserting into database
			if ($is_dev) echo str_replace(")", "<br />)<br />", str_replace("[", "<br />[", print_r($rows[$i], true)));
			if ($is_dev) echo str_replace(")", "<br />)<br />", str_replace("[", "<br />[", print_r($cells, true)));
			$rowerrors .= "<li>Row ".($i + 1).": [ERROR] There was an error inserting the record. ".$wpdb->last_error.", \"".$upd_ins_str."\". Row data not imported. Please fix and re-upload.</li>";
			$errorcount++;
			continue;									
		} else $insertcount++;
	}
	if ($is_dev) echo $rowerrors;
	echo '<script>top.setresults("<strong>Import Results</strong><br />'.$insertcount.' records imported.<br />'.$errorcount.' records NOT imported.<br />'.str_replace('"','\"',$rowerrors).'");</script>';
	exit();
}

function sales_rep_export() {
	global $wpdb, $doc_filetype, $doc_table;
	//	$wpdb->show_errors();
	$query = $wpdb->get_results("SELECT zip_prefix, first_name, last_name, title, email, phone FROM sales_rep ORDER BY zip_prefix");
	$output = "ZIP_PREFIX\tFIRST_NAME\tLAST_NAME\tTITLE\tEMAIL\tPHONE"."\r\n";
	$rowcount = 0;
	foreach($query as $row) {
		$zip_prefix = exportstring($row->zip_prefix);
		$first_name = exportstring($row->first_name);
		$last_name = exportstring($row->last_name);
		$title = exportstring($row->title);
		$email = exportstring($row->email);
		$phone = exportstring($row->phone);
		$output .= $zip_prefix."\t".$first_name."\t".$last_name."\t".$title."\t".$email."\t".$phone."\r\n";
		$rowcount++;
	}
	$filename = "sales_rep_list_".date("m_d_Y").".txt";
	do_export($filename, $output);
	exit();
}

function do_export($filename, $output) {
	header('Content-Description: File Transfer');
	header('Content-Type: text/plain');
	header('Content-Disposition: attachment; filename="'.$filename.'"');
	header('Content-Transfer-Encoding: binary');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Expires: 0');
	header('Content-Length: '.strlen($output));
	echo $output;
}

function permissions_check() {
	if (!current_user_can('manage_options'))  {
		wp_die( __('You do not have sufficient permissions to access this page.') );
	}
}

function importstring($text, $max, $row, $colname, $hide_blank_warnings, $blank_is_error) {
	if ($text == '""')
		$text = "";
	if (strpos($text, '"') === 0)
		$text = substr($text, 1);
	if (strrpos($text, '"') == strlen($text) - 1 && strlen($text) > 1)
		$text = substr($text, 0, strlen($text) - 1);
	$text = trim(str_replace('""', '"', str_replace("'", "''", $text)));
	$ret = array("text" => $text, "error" => "");
	if (strlen($text) > $max) {
		$ret["text"] = substr($ret["text"], 0, $max);
		$ret["error"] = "<li>Row ".($row + 1).": [WARNING] Column '".$colname."' has been truncated to ".$max." characters.</li>";
	} else if (strlen($text) == 0 && $blank_is_error) {
		$ret["error"] .= "<li>Row ".($row + 1).": [ERROR] Column '".$colname."' is empty. Row data not imported. Please fix and re-upload.</li>";
	} else if (strlen($text) == 0 && ! $hide_blank_warnings) {
		$ret["error"] .= "<li>Row ".($row + 1).": [WARNING] Column '".$colname."' is empty.</li>";
	}
	return $ret;
}

function exportstring($text) {
	$text = '"'.str_replace('"', '""', $text).'"';
	return $text;
}

function zeroPad($str) {
	if ($str != "" && strlen($str) < 5)
		return str_repeat("0", 5 - strlen($str)).$str;
	else return $str;
}

function validateDate($date)
{
    $d = DateTime::createFromFormat('Y-m-d', $date);
    return $d && $d->format('Y-m-d') === $date;
}

?>
