<?php
define("wsdplugin_SRC_ID", 1);
define("wsdplugin_SERVICE_URL", "https://dashboard.websitedefender.com/rpc.php");
define("wsdplugin_JSRPC_URL", "https://dashboard.websitedefender.com/jsrpc.php?callback=?");
define("wsdplugin_API_VERSION", 1);
define("wsdplugin_PLUGIN_PATH", WP_PLUGIN_URL . '/websitedefender-wordpress-security/');
define("wsdplugin_SRVCAP_URL", "http://stats.websitedefender.com:8888/");
