=== WebsiteDefender WordPress Security ===
Contributors: WebsiteDefender
Tags: admin, administration, wordpress security, websitedefender, security plugin, wordpress permissions, strong password, protection, security, security scan, security scanner, wordpress database security
Requires at least: 3.0
Tested up to: 3.5.1
Stable tag: trunk
License: GPLv2 or later


Helps you secure your WordPress installation and provides detailed reporting on discovered vulnerabilities and how to fix them.

== Description ==

The WebsiteDefender WordPress Security plugin is the ultimate must-have tool when it comes to WordPress security. The plugin is free and monitors your website for security weaknesses that hackers might exploit and tells you how to easily fix them.
You can also sign up for a full 15 day WebsiteDefender trial which includes includes extra security checks and daily malware scans. WebsiteDefender integrates with the plugin which allows you to see all your security alerts from your WordPress dashboard.


= Key security features: =

* Removal of error information on login page
* Automatically adds index.php to WordPress directories to avoid information disclosure
* Removal or WordPress version
* Removal of Really Simple Discovery meta tag
* Removal of Windows Live Writer meta tag
* Removal of WordPress update information to non admin wordpress users
* Removal of WordPress plugins update information to non admin wordpress users
* Removal of WordPress theme update information to non admin wordpress users
* Hides the WordPress version in the backend dashboard for non admin wordpress users
* Disables database error reporting
* Disables PHP error reporting


= Security tools: =

* Strong Password Generator which you can use to protect your WordPress from brute force attacks.
* Database tool which you can use to automatically rename the WordPress database table prefix to protect WordPress from zero day vulnerabilities.

= Integration with WebsiteDefender: =

If you are a WebsiteDefender user you can check out all WebsiteDefender WordPress Security alerts from your WordPress dashboard. WebsiteDefender scans your website daily for security issues and malware and creates automated backups, making sure you are fully covered. If you're new to WebsiteDefender, you can try it out for free for 15 days. Simply download the plugin and sign up for your 15 day trial, and join the vast community of WordPress users that are enjoying peace of mind when it comes to their WordPress security!

For more information on the WebsiteDefender WordPress Security plugin and other WordPress security news, visit the <a href="http://www.websitedefender.com/blog" target="_blank">WebsiteDefender Blog</a>. Post any questions or feedback on the <a href="http://www.websitedefender.com/forums/wp-security-scan-plugin/" target="_blank">WebsiteDefender WordPress Security plug-in forum</a>.



== Requirements ==
* WordPress version 3.0 and higher (tested up to 3.5.1)
* PHP5 (tested with PHP Interpreter >= 5.2.9)



== Installation ==
* Make a backup of your current installation
* Unpack the downloaded package
* Upload the extracted files to the /wp-content/plugins/ directory
* Activate the plugin through the 'Plugins' menu in WordPress

If you do encounter any bugs, or have comments or suggestions, please post them on the <a href="http://www.websitedefender.com/forums/websitedefender-wordpress-security-plugin/" target="_blank">WebsiteDefender WordPress Security plug-in forum</a>.

For more information on the WebsiteDefender WordPress Security plug-in and other WordPress security news, visit the <a href="http://www.websitedefender.com/blog" target="_blank">WebsiteDefender Blog</a> and join our <a href="http://www.facebook.com/websitedefender" target="_blank">Facebook</a> page. Post any questions or feedback on the <a href="http://www.websitedefender.com/forums/wp-security-scan-plugin/" target="_blank">WebsiteDefender WordPress Security plug-in forum</a>.


== Other Notes ==

For more information on the WebsiteDefender WordPress Security plug-in and other WordPress security news, visit the <a href="http://www.websitedefender.com/blog" target="_blank">WebsiteDefender Blog</a> and join our <a href="http://www.facebook.com/websitedefender" target="_blank">Facebook</a> page. Post any questions or feedback on the <a href="http://www.websitedefender.com/forums/wp-security-scan-plugin/" target="_blank">WebsiteDefender WordPress Security plug-in forum</a>.


== License ==
Good news, this plugin is free for everyone! Since it's released under the GPL, you can use it free of charge on your personal or commercial blog.

For more information on the WebsiteDefender WordPress Security plug-in and other WordPress security news, visit the <a href="http://www.websitedefender.com/blog" target="_blank">WebsiteDefender Blog</a> and join our <a href="http://www.facebook.com/websitedefender" target="_blank">Facebook</a> page. Post any questions or feedback on the <a href="http://www.websitedefender.com/forums/wp-security-scan-plugin/" target="_blank">WebsiteDefender WordPress Security plug-in forum</a>.



== Screenshots ==

1. The plugin automated checks and security alerts node WebsiteDefender alerts integrated in WordPress
2. The WebsiteDefender WordPress security alerts in the WordPress dashboard
3. WebsiteDefender WordPress security alert sample





== Changelog ==

= 1.0.4 =
* Fixed images paths


= 1.0.3 =
* Added the blog page
* Added the Why Go Pro page
* Changed the registration page
* Removed the password tool page
* Various internal code updates


= 1.0.2 =
* Added the Real Time Security Information section
* Added Backup tab
* Minor UI fixes


= 1.0.1 =
* False positive alert for WordPress readme fixed
* Fixed no alert pop up message shows when no WebsiteDefender alert is selected
* Limited db prefix to 15 characters
* Fixed issue where database tool generated an empy wp-config.php file


= 1.0 =
* Complete rewrite of the plugin
* New user friendly UI
* More robust security checks
* Integrated WebsiteDefender dashboard (gives you the ability to check WebsiteDefender WordPress security alerts from the WordPress dashboard)


= v0.6 =
* New feature: Added 'nonce' fields to forms where needed (thanks to julio from boiteaweb.fr)
* New feature: Added tooltips to the most important sections of the System Information box
* Update: Plugin settings are now accessible only by WordPress administrators
* Update: Text and hyperlink UI updates
* Update: Updated validation for plug-in form fields (email address, user name, target id, etc.)
* Update: Enhanced the input validation for the Change Database Prefix tool
* Update: Improved the validation of permissions for readme.html file
* Update: Improved user-rights retrieval in the WordPress database


= v0.5 =
* New feature: A System Information Report box provides a summary of your web server's components
* New setting: Option to open / close WebsiteDefender dashboard widget
* Update: Internal code updates
* Update: More help resource links

= v0.4 =
* BugFix: The bug reported about ALTER rights retrieval has been addressed
* Update: Code cleanup
* Update: Minor internal updates

= v0.3 =
* Update: Minor updates

= 0.2 =
* Update: Minor updates

= v0.1 =
* Feature: Removes error-information on login-page
* Feature: Adds index.php to the wp-content, wp-content/plugins, wp-content/themes and wp-content/uploads directories to prevent directory listings
* Feature: Removes the wp-version, except in admin-area
* Feature: Removes Really Simple Discovery meta tag
* Feature: Removes Windows Live Writer meta tag
* Feature: Removes core update information for non-admins
* Feature: Removes plugin-update information for non-admins
* Feature: Removes theme-update information for non-admins (only WP 2.8 and higher)
* Feature: Hides wp-version in backend-dashboard for non-admins
* Feature: Removes version on URLs from scripts and stylesheets only on frontend
* Feature: Provides various information after scanning your Wordpress blog
* Feature: Provides file permissions security checks
* Feature: Provides a strong password generator tool
* Feature: Provides database backup utility
* Feature: Provides a tool for changing the database prefix
* Feature: Turns off database error reporting (if enabled)
* Feature: Turns off PHP error reporting

For more information on the WebsiteDefender WordPress Security plug-in and other WordPress security news, visit the <a href="http://www.websitedefender.com/blog" target="_blank">WebsiteDefender Blog</a> and join our <a href="http://www.facebook.com/websitedefender" target="_blank">Facebook</a> page. Post any questions or feedback on the <a href="http://www.websitedefender.com/forums/wp-security-scan-plugin/" target="_blank">WebsiteDefender WordPress Security plug-in forum</a>.