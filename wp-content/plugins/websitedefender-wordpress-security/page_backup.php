<?php if (!defined('wsdplugin_WSD_PLUGIN_SESSION')) exit; wp_enqueue_style('wsdplugin_css_general',   wsdplugin_Utils::cssUrl('general.css'), array(), '1.0');
?>

<div class="wrap wsdplugin_content" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">

<div id="wrap wsdplugin_advert">
	<a href="<?php echo wsdplugin_Handler::site_url().'wp-admin/admin.php?page=wsdplugin_alerts';?>">
		<img src="<?php echo wsdplugin_Utils::imgUrl('wsd-backup-feature.jpg');?>" title="" alt=""/>
	</a>
</div>


<div>
	<p class="wsdplugin_special_text"><strong>Coming soon...</strong></p>

	<p class="wsdplugin_special_text">Thousands of WebsiteDefender users are already enjoying the added security of having their WordPress websites backed up everyday via the WebsiteDefender dashboard.</p>

	<p class="wsdplugin_special_text">WebsiteDefender Pro users of this plugin will soon be able to download and restore their WordPress website backups, including posts, pages, plugins and themes right from here, without having to sign into their WebsiteDefender dashboard!</p>

	<p class="wsdplugin_special_text">Join the thousands of WebsiteDefender users by upgrading to a <a href="<?php echo wsdplugin_Handler::site_url().'wp-admin/admin.php?page=wsdplugin_alerts';?>">Pro account today!</a></p>
</div>
