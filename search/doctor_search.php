<?php
	$is_dev = ($_SERVER["HTTP_HOST"] == "localhost" ? true : false);
	$override = isset($_GET["override"]);
	if ($is_dev) {
		error_reporting(E_ALL);
		ini_set('display_errors', '1');
	}
	include $_SERVER['DOCUMENT_ROOT']."/oati_includes/db.php";
	
	$db_table = "usa"; // dev table setting
	if (! $is_dev)
		$db_table = (strpos($_SERVER["HTTP_HOST"], ".co.uk") ? "uk" : "usa");

	if (! $override && ! $is_dev && (! isset($_SERVER["HTTP_REFERER"]) || ! strpos($_SERVER["HTTP_REFERER"], $_SERVER["HTTP_HOST"]))) { // make sure request is from our server
		echo "ERROR: Access denied.";
		exit();

	} else {
		// search form variables
		/* actually we don't need most of these
		$address = (isset($_POST["address"]) ? $_POST["address"] : (isset($_GET["address"]) ? $_GET["address"] : ""));
		$city = (isset($_POST["city"]) ? $_POST["city"] : (isset($_GET["city"]) ? $_GET["city"] : ""));
		$state_province = (isset($_POST["state_province"]) ? $_POST["state_province"] : (isset($_GET["state_province"]) ? $_GET["state_province"] : ""));
		$zip_code = (isset($_POST["zip_code"]) ? $_POST["zip_code"] : (isset($_GET["zip_code"]) ? $_GET["zip_code"] : ""));
		$practice_name = (isset($_POST["practice_name"]) ? $_POST["practice_name"] : (isset($_GET["practice_name"]) ? $_GET["practice_name"] : ""));
		$search_radius = (isset($_POST["search_radius"]) ? $_POST["search_radius"] : (isset($_GET["search_radius"]) ? $_GET["search_radius"] : ""));
		 */
		$search_bounds = (isset($_POST["search_bounds"]) ? $_POST["search_bounds"] : (isset($_GET["search_bounds"]) ? $_GET["search_bounds"] : ""));
		$search_bounds_array = explode(",", str_replace(array("(", ")", " "), "", $search_bounds));
		$latitude_min = $search_bounds_array[0];
		$latitude_max = $search_bounds_array[2];
		$latitude_mid = ($latitude_min + $latitude_max) / 2;
		$longitude_min = $search_bounds_array[1];
		$longitude_max = $search_bounds_array[3];
		$longitude_mid = ($longitude_min + $longitude_max) / 2;
		$extra_bounds = (isset($_POST["extra_bounds"]) ? $_POST["extra_bounds"] : (isset($_GET["extra_bounds"]) ? $_GET["extra_bounds"] : ""));
		$extra_bounds_array = explode(",", str_replace(array("(", ")", " "), "", $extra_bounds));
		$latitude2_min = $extra_bounds_array[0];
		$latitude2_max = $extra_bounds_array[2];
		$longitude2_min = $extra_bounds_array[1];
		$longitude2_max = $extra_bounds_array[3];

		$lat = 32;
		$long = -97;
		$rad = (isset($_POST["search_radius"]) ? $_POST["search_radius"] : (isset($_GET["search_radius"]) ? $_GET["search_radius"] : ""));


		if (! is_numeric($latitude_min) || ! is_numeric($latitude_max) || ! is_numeric($longitude_min) || ! is_numeric($longitude_max) || ! is_numeric($latitude2_min) || ! is_numeric($latitude2_max) || ! is_numeric($longitude2_min) || ! is_numeric($longitude2_max)) { // || (($city == "" || $state_province == "") && ($practice_name == "" || $state_province == "") && ($zip_code == ""))) {	// check that we have minimum search values
			echo "ERROR: Missing minimum search parameters.";
			exit();

		} else {
			$connect = mysql_connect($db_host, $db_user, $db_pw);
			if (! $connect) { // if error connecting to database
				echo "ERROR: There was a problem connecting to the database. ".mysql_error().".";
				exit();
			} else {
				mysql_set_charset("latin1", $connect);
				$query = mysql_query("SELECT acos(sin(radians(latitude))*sin(radians(".$latitude_mid."))+cos(radians(latitude))*cos(radians(".$latitude_mid."))*cos(radians(".$longitude_mid.")-radians(longitude)))*".($db_table=="uk"?"6371":"3959")." AS 'distance', CASE WHEN latitude BETWEEN ".$latitude_min." AND ".$latitude_max." AND longitude BETWEEN ".$longitude_min." AND ".$longitude_max." THEN 1 ELSE 0 END AS 'in_circle', first_name, last_name, degree, practice_name, phone, website, address, city, ".($db_table=="usa"?"state_province, zip_code":"postal_code, country").", specialty, provider_type, latitude, longitude, couponlinkID,coupon,filename,campaignID,linktext FROM ".($is_dev ? "portent" : "ortho".($db_table=="uk"?"_uk":"")."_wp").".docs_".$db_table." du LEFT JOIN campaigns c ON du.campaignID = c.campaignID WHERE latitude BETWEEN ".$latitude2_min." AND ".$latitude2_max." AND longitude BETWEEN ".$longitude2_min." AND ".$longitude2_max."
				 ORDER BY provider_type DESC, 1, 2 DESC");




                if (! $query) { // if error querying database
					echo "ERROR: There was a problem querying the database. ".mysql_error().".";
					mysql_close($connect);
					exit();
				} else {
					$results = array();
					while ($row = mysql_fetch_assoc($query)) {
						$results[count($results)] = $row;
					}
					echo json_encode($results, JSON_FORCE_OBJECT);
					mysql_close($connect);
				}
			}
		}
	}
?>
