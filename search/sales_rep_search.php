<?php
	$is_dev = false; // ($_SERVER["HTTP_HOST"] == "localhost" ? true : false);
	$is_staging = (strpos($_SERVER["HTTP_HOST"], "staging.") == 1 ? true : false);
	$override = isset($_GET["override"]);
	if ($is_dev) {
		error_reporting(E_ALL);
		ini_set('display_errors', '1');
	}

	include $_SERVER['DOCUMENT_ROOT']."/oati_includes/db.php";
	
	if (! $override && ! $is_dev && (! isset($_SERVER["HTTP_REFERER"]) || ! strpos($_SERVER["HTTP_REFERER"], $_SERVER["HTTP_HOST"]))) { // make sure request is from our server
		echo "ERROR: Access denied.";
		exit();
		
	} else {
		// search form variables
		$zip_code = (isset($_POST["use_zip_code"]) ? $_POST["use_zip_code"] : (isset($_GET["use_zip_code"]) ? $_GET["use_zip_code"] : ""));
		if ($zip_code == "" || strlen($zip_code) < 3) {
			echo "ERROR: Missing minimum search parameters.";
			exit();

		} else {
			$connect = mysql_connect($db_host, $db_user, $db_pw);
			if (! $connect) { // if error connecting to database
				echo "ERROR: There was a problem connecting to the database. ".mysql_error().".";
				exit();
			} else {
				$query = mysql_query("SELECT first_name, last_name, title, email, phone FROM ".$db_name.".sales_rep WHERE zip_prefix = '". $zip_code ."' ORDER BY (CASE WHEN title LIKE 'Territory%' THEN 0 ELSE 1 END) LIMIT 2");
				if (! $query) { // if error querying database
					echo "ERROR: There was a problem querying the database. ".mysql_error().".";
					mysql_close($connect);
					exit();
				} else {
					$results = array();
					while ($row = mysql_fetch_assoc($query)) {
						$results[count($results)] = $row;
					}
					echo json_encode($results, JSON_FORCE_OBJECT);
					mysql_close($connect);
					exit();
				}
			}
		}
	}
?>
