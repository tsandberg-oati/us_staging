<?php
/** 
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information by
 * visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ortho_wp_stage');

/** MySQL database username */
define('DB_USER', 'ortho_wp_stage');

/** MySQL database password */
define('DB_PASSWORD', 'acceler0m3t3r');

/** MySQL hostname */
define('DB_HOST', 'oati-mysql.c0kg4ptnumz8.us-west-2.rds.amazonaws.com:3306');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link http://api.wordpress.org/secret-key/1.1/ WordPress.org secret-key service}
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bYvdbsb!leO@KEFPzNV*lVKAPAx#P)r(ZLIlr00H^Je)UKUTl5tRY1)4Cae32E!u');
define('SECURE_AUTH_KEY',  '8P6H1sw3Fv08EfAWxk)sEeHJ2m3xpkOyRV@dPwBqHcK#ZgM7SVDitJ09y&VB8mg3');
define('LOGGED_IN_KEY',    'n0m4We$ILj%KLW05m5(#iaEeli#Xt82%1z#5Z^idtQ*jTDcj2fU90V0JEn(xi%Fc');
define('NONCE_KEY',        'm4X*x0VNK9rAqnh)XtSco@$!6gHYRW#0%^v@b81AZUY7)La^%umnb8(&OG!#i4cQ');
define('AUTH_SALT',        'Hf$9nEvwV&5w6omo1XMj0F5Ipl#@R^J02B60rHr8$4#hX0J0Lth%kXqbpy#sRk4I');
define('SECURE_AUTH_SALT', '!RQpKSnfItD5%vXntx5wW)9!k88hQwsaW)81P)snblq&^83F7UapKx7VS7l$u(N6');
define('LOGGED_IN_SALT',   'aX$(0lK%rdVFyq$$iR1c7F!&xhc(A2W@)P@cAr2Z)!dfxzkHRysU5CCYodDm!6Q8');
define('NONCE_SALT',       'x49Uw4avtauXajM4IDei*@aiAmk6wRE01)z(ZkN&nH8&eCBl0ozJSjZ7xVli%wPN');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define ('WPLANG', 'en_US');

define ('FS_METHOD', 'direct');

define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

?>
